{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Importance Sampling Demo\n",
    "This demo illustrates the basic concept of importance sampling described in Section 3.1.1 of [1].\n",
    "\n",
    "[1] Kobilarov, Marin. \"Cross-entropy motion planning.\" The International Journal of Robotics Research 31.7 (2012): 855-871."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib\n",
    "print(\"Matplotlib:\", matplotlib.__version__)\n",
    "print(\"Torch:\", torch.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cost Function\n",
    "Define a cost function $H(z)$ to evaluate samples with."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = lambda x: -0.1*x**3 + 0.5*x**2 + x + 3\n",
    "\n",
    "zs = np.linspace(-7, 7, 1000)\n",
    "fig = plt.figure(figsize=(10, 6))\n",
    "plt.plot(zs, H(zs), lw=5, label=\"cost\")\n",
    "plt.legend(prop={'size': 18})\n",
    "plt.xticks(fontsize=16)\n",
    "plt.yticks(fontsize=16)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## True and Importance Densities\n",
    "Define the true density $p(z)$ with which we wish to compute expected cost $\\mathbb{E}_p[H(Z)] = \\int H(z)p(z) dz$. Also define importance density $q(z)$ to use for importance sampling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "true_mu = 3\n",
    "true_sigma = 1\n",
    "importance_mu = 0\n",
    "importance_sigma = 2\n",
    "\n",
    "p = torch.distributions.normal.Normal(true_mu, true_sigma)\n",
    "q = torch.distributions.normal.Normal(importance_mu, importance_sigma)\n",
    "\n",
    "fig = plt.figure(figsize=(10, 6))\n",
    "true_values = torch.exp(p.log_prob(torch.Tensor(zs))).data.numpy()\n",
    "importance_values = torch.exp(q.log_prob(torch.Tensor(zs))).data.numpy()\n",
    "plt.plot(zs, true_values, lw=5, label=\"p(x) -- true\")\n",
    "plt.plot(zs, importance_values, lw=5, label=\"q(x) -- importance\")\n",
    "plt.legend(prop={'size': 18})\n",
    "plt.xticks(fontsize=16)\n",
    "plt.yticks(fontsize=16)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importance Sampling\n",
    "Estimate $\\mathbb{E}_p[H(Z)] = \\int H(z)p(z) dz = \\int H(Z) \\frac{p(z)}{q(z)}q(z) dz = \\mathbb{E}_q\\left[H(z)\\frac{p(z)}{q(z)}\\right] \\approx \\frac{1}{N} \\sum_{i=1}^N H(z_i) \\frac{p(z_i)}{q(z_i)}.$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Here you'll see as the number of samples used to estimate the expected cost increases,\n",
    "# the closer the estimate gets to the actual expectation when using the proxy distribution\n",
    "# instead of the actual distribution. You will also note a tighter variance the as the\n",
    "# number of samples increases.\n",
    "\n",
    "Ns = [10, 50, 100, 500, 1000]  # Number of samples for each run\n",
    "n_runs = 10  # Number of runs, more will give better variance estimate but will take longer to compute\n",
    "\n",
    "actual = [[] for _ in range(len(Ns))]\n",
    "estimate = [[] for _ in range(len(Ns))]\n",
    "\n",
    "print(\"Computing actual and estimated expected costs...\")\n",
    "for N_idx, N in enumerate(Ns):\n",
    "    for _ in range(n_runs):\n",
    "        # Compute ground truth expectation using actual distribution\n",
    "        costs = []\n",
    "        for i in range(N):\n",
    "            z_i = p.sample()\n",
    "            p_density_value = torch.exp(p.log_prob(z_i)).item()\n",
    "            cost = H(z_i)\n",
    "            costs.append(cost)\n",
    "        actual_expectation = sum(costs) / float(N)\n",
    "\n",
    "        # Compute expectation using importance sampling\n",
    "        costs = []\n",
    "        for i in range(N):\n",
    "            z_i = q.sample()\n",
    "            p_density_value = torch.exp(p.log_prob(z_i)).item()\n",
    "            q_density_value = torch.exp(q.log_prob(z_i)).item()\n",
    "            ratio = p_density_value / q_density_value\n",
    "            cost = H(z_i) * ratio\n",
    "            costs.append(cost)\n",
    "        estimated_expectation = sum(costs) / float(N)\n",
    "    \n",
    "        actual[N_idx].append(actual_expectation.item())\n",
    "        estimate[N_idx].append(estimated_expectation.item())\n",
    "print(\"Complete\")\n",
    "        \n",
    "estimate_means = [np.mean(e) for e in estimate]\n",
    "estimate_stds = [np.std(e) for e in estimate]\n",
    "    \n",
    "actual_means = [np.mean(a) for a in actual]\n",
    "actual_stds = [np.std(a) for a in actual]\n",
    "    \n",
    "fig, ax = plt.subplots(1, 1)\n",
    "fig.set_size_inches(10, 10)\n",
    "\n",
    "plt.errorbar(np.arange(len(Ns)), estimate_means, estimate_stds, linestyle='None', marker='s', ms=20, lw=5, capsize=20, capthick=5, label='estimated')\n",
    "plt.errorbar(np.arange(len(Ns)), actual_means, actual_stds, linestyle='None', marker='s', ms=20, lw=5, capsize=20, capthick=5, label='actual')\n",
    "plt.legend(loc=1, prop={'size': 16})\n",
    "labels = ax.get_xticklabels()\n",
    "ax.xaxis.set_major_locator(plt.MaxNLocator(len(Ns)))\n",
    "ax.xaxis.set_ticks(range(len(Ns)))\n",
    "ax.set_xticklabels(Ns)\n",
    "plt.xticks(fontsize=18)\n",
    "plt.yticks(fontsize=18)\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
