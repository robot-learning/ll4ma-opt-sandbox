# import numpy as np
import torch

from ll4ma_opt.solvers.log_barrier import LogBarrierSolver

from ll4ma_opt.problems import CircleInequality  # TwoLinkIK, PandaIK,

# from ll4ma_opt.problems import PointMassAccelerationFormulation, PointMassPositionFormulation
# from ll4ma_opt.solvers.penalty_method import PenaltyMethod
# from ll4ma_opt.solvers.penalty_method import AugmentedLagranMethod

torch.set_default_tensor_type(torch.DoubleTensor)


def test_circle_ineq():
    problem = CircleInequality()
    params = {"alpha": 1, "rho": 0.9, "min_alpha": 1e-30}
    x0 = problem.initial_solution.copy()
    solver = LogBarrierSolver(
        problem,
        "newton",
        params,
        init_barrier_multiplier=1.0,
        update_ratio=20.0,
        use_phase_I_sum=False,
    )

    x0[0] = 3
    print(f"x0 = {x0}")
    result = solver.optimize(x0, max_iterations=500)
    result.display()
    problem.visualize_optimization(result)

    # TODO plot slack iterates versus time


if __name__ == "__main__":
    test_circle_ineq()
