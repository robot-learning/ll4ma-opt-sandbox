# Third Party
import matplotlib.pyplot as plt
import torch

# ll4ma
from ll4ma_opt.problems import PointMassPositionFormulation
from ll4ma_opt.solvers.penalty_method import AugmentedLagranMethod, PenaltyMethod

torch.set_default_tensor_type(torch.DoubleTensor)


def compare_penalty_performance(num_prob_instances=2):
    """
    Generate and solve a bunch of problem instances between penalty and
    augmented Lagrangian
    """
    params = {"alpha": 1, "rho": 0.5, "min_alpha": 1e-30}
    FP_PRECISION = 1e-5

    p_results = []
    al_results = []

    for i in range(num_prob_instances):
        # Create problem instances
        problem = PointMassPositionFormulation(timesteps=15, obstacles=50, obstacle_radius=0.1)

        # Build associated ssolver
        augla_solver = AugmentedLagranMethod(problem, "newton", params, FP_PRECISION=FP_PRECISION)

        penalty_solver = PenaltyMethod(problem, "newton", params, FP_PRECISION=FP_PRECISION)
        # Solve and store results
        print("\nRunning aug la method on problem", i)
        al_results.append(augla_solver.optimize(problem.initial_solution, max_iterations=50))
        al_results[-1].display()

        print("\nRunning penalty method on problem", i)
        p_results.append(penalty_solver.optimize(problem.initial_solution, max_iterations=50))
        p_results[-1].display()

    # Analyze results
    # Plot histogram of solves times
    p_times = [p.time for p in p_results]
    al_times = [a.time for a in al_results]
    plot_comparison_histograms(p_times, al_times, "Times")

    # Plot histogram of solve costs
    p_costs = [p.cost for p in p_results]
    al_costs = [a.cost for a in al_results]
    plot_comparison_histograms(p_costs, al_costs, "Costs")

    # Plot iterates
    p_steps = [len(p.iterates) - 1 for p in p_results]
    al_steps = [len(a.iterates) - 1 for a in al_results]
    plot_comparison_histograms(p_steps, al_steps, "Steps")

    # Plot bargraph comparing success percentage
    p_success = sum([p.converged for p in p_results]) / len(p_results)
    al_success = sum([a.converged for a in al_results]) / len(al_results)
    plt.figure()
    plt.bar(["Penalty Method", "AugLa"], [p_success * 100, al_success * 100])
    plt.title("Success Rate")

    # Show plot
    # TODO: Make this an option and write to disk otherwise
    plt.show()


def plot_comparison_histograms(p_data, a_data, title_suffix, n_bins=10):
    max_val = max(max(p_data), max(a_data))
    hist_range = (0, max_val)

    fig, axes = plt.subplots(1, 2, sharey=True, tight_layout=True)
    axes[0].hist(p_data, bins=n_bins, range=hist_range)
    axes[0].set_title("Penalty Method " + title_suffix)
    axes[1].hist(a_data, bins=n_bins, range=hist_range)
    axes[1].set_title("AugLa " + title_suffix)


if __name__ == "__main__":
    compare_penalty_performance(100)
