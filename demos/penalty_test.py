#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 16:15:11 2021

@author: tabor
"""
# Standard Library
import time

# Third Party
import torch

# ll4ma
from ll4ma_opt.problems import PointMassPositionFormulation
from ll4ma_opt.solvers.penalty_method import AugmentedLagranMethod, PenaltyMethod

torch.set_default_tensor_type(torch.DoubleTensor)


problem = PointMassPositionFormulation(timesteps=15, obstacles=50, obstacle_radius=0.1)

params = {"alpha": 1, "rho": 0.2}
start = time.time()
solver = AugmentedLagranMethod(problem, "newton", params)

r = solver.optimize(problem.initial_solution, max_iterations=50)
r.display(False)

solver2 = PenaltyMethod(problem, "newton", params)
r2 = solver2.optimize(problem.initial_solution, max_iterations=50)
r2.display(False)
