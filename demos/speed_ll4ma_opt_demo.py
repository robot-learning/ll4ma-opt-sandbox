#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  9 11:15:32 2022

@author: tabor
"""

from ll4ma_opt.problems import Problem
from ll4ma_opt.solvers import GradientDescent
import torch
import numpy as np
import torch.multiprocessing as multiprocessing
import time


class BigProblem(Problem):
    def __init__(self, size=100):
        np.random.seed(1)
        self.Q = np.random.random((size, size))
        self._Q = torch.tensor(self.Q)
        self._size = size
        self.initial_solution = np.random.random((size, 1))
        self.make_tensor = torch.DoubleTensor
        super().__init__()

    def size(self):
        return self._size

    def tensor_cost(self, x):
        return x.T @ self._Q @ x


class BigProblem2(BigProblem):
    def cost(self, x):
        return x.T @ self.Q @ x

    def cost_gradient(self, x):
        return (self.Q + self.Q.T) @ x


class BigProblemTorch(BigProblem2):
    def __init__(self, size=100, device="cpu"):
        super().__init__(size)
        self.Q = torch.tensor(self.Q, device=device)
        self.max_bounds = torch.tensor(self.max_bounds, device=device)
        self.min_bounds = torch.tensor(self.min_bounds, device=device)
        self.initial_solution = torch.tensor(self.initial_solution, device=device)
        self.device = device

    def make_tensor(self, x):
        return torch.DoubleTensor(x).to(self.device)


def optimize_func_torch(prob):
    solver = GradientDescent(prob, library=torch)
    result = solver.optimize(prob.initial_solution, 10)
    return result


def optimize_func_np(prob):
    solver = GradientDescent(prob)
    result = solver.optimize(prob.initial_solution, 10)
    return result


if __name__ == "__main__":
    size = 500

    prob1 = BigProblem(size)
    solver1 = GradientDescent(prob1)
    result1 = solver1.optimize(prob1.initial_solution)
    print("traditional np + torch time ", result1.time)

    prob2 = BigProblem2(size)
    solver2 = GradientDescent(prob2)
    result2 = solver2.optimize(prob2.initial_solution)
    print("Analytic Gradient, pure np time ", result2.time)

    prob3 = BigProblemTorch(size)
    solver3 = GradientDescent(prob3, library=torch)
    result3 = solver3.optimize(prob3.initial_solution)
    print("Analytic Gradient, pure cpu torch time ", result3.time)

    prob4 = BigProblemTorch(size, "cuda")
    solver4 = GradientDescent(prob4, library=torch)
    result4 = solver4.optimize(prob4.initial_solution)

    print("Analytic Gradient, pure GPU torch time ", result4.time)

    ctx = multiprocessing.get_context("spawn")
    pool = ctx.Pool(processes=8)

    solver_prob_list = [prob3] * 10

    results = pool.map(optimize_func_torch, solver_prob_list)

    s = time.time()
    results = pool.map(optimize_func_torch, solver_prob_list)
    print(time.time() - s)
    print([result.time for result in results])
