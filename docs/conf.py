# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# Standard Library
import os
import pathlib
import sys

# Third Party
from sphinx.ext import apidoc

root = pathlib.Path(__file__).resolve().parent.parent
print(f"root={root}")
sys.path.insert(0, root / "src")


# -- Project information -----------------------------------------------------

project = "ll4ma_opt"
copyright = "2022, ll4ma lab"
author = "ll4ma lab"

# -- Run sphinx-apidoc -------------------------------------------------------
# This hack is necessary since RTD does not issue `sphinx-apidoc` before running
# `sphinx-build -b html docs _build/docs`.
# See Issue: https://github.com/[[rtfd/readthedocs.org/issues/1139

output_dir = os.path.join(root, "docs", "_api")
module_dir = os.path.join(root, "src", "ll4ma_opt")

apidoc_args = [
    "--implicit-namespaces",
    "--force",
    "--separate",
    "--module-first",
    "-o",
    f"{output_dir}",
    f"{module_dir}",
]

try:
    apidoc.main(apidoc_args)
    print("Running `sphinx-apidoc` complete!")
except Exception as e:
    print(f"ERROR: Running `sphinx-apidoc` failed!\n{e}")

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.todo",
    "sphinx_rtd_theme",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'alabaster'
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]
