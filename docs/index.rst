.. ll4ma_opt documentation master file, created by
   sphinx-quickstart on Sat Mar 19 14:42:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ll4ma_opt's documentation!
=====================================

.. toctree::
   api
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

