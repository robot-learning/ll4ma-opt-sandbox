#!/bin/bash

set -e  # exit on error for any line
set -u  # exit if variable undefined

# similar to $(dirname $(readlink -f $1)) but without resolving symbolic links
absdirname() {
  local relpath="$1"
  local orig_dir="${PWD}"
  cd "$(dirname "${relpath}")"
  pwd -L
  cd "${orig_dir}"
}

script_dir="$(absdirname "$0")"
env_name=ll4ma_opt
env_file="${script_dir}/conda/${env_name}_env.yml"

if [ $# -ge 2 ] && ( [ "$1" == "-h" ] || [ "$1" == "--help" ] ); then
  cat <<END_OF_USAGE

Usage: $0

Description:
  This script sets up a conda environment for use with this package.  If the
  environment is already there, it will be updated to the current
  specifications.

  After running this script, you can activate the environment with

    conda activate ${env_name}

  When you are done, you can deactivate with

    conda deactivate

  Note, this is expecting that you have already installed miniconda or anaconda
  and the conda command is available through the PATH environment variable.

END_OF_USAGE
  exit 0
fi

conda_env_exists() {
  local env_name_arg="$1"
  conda env list | grep "${env_name_arg}" 2>/dev/null >/dev/null
}


# Update defaults so extending environments have up-to-date packages
conda update -n base -c defaults conda

# create or update from environment yaml file
if conda_env_exists "${env_name}"; then
  echo "Environment ${env_name} already exists, updating"
  echo
  conda env update --name "${env_name}" --file "${env_file}" --prune
else
  echo "Creating new environment ${env_name}"
  echo
  conda env create --file "${env_file}"
fi

# install the package in develop mode
# See this for sed help: https://stackoverflow.com/a/31728689/3711266
source $(echo $CONDA_EXE | sed -e 's/\/bin.*//g')/etc/profile.d/conda.sh
conda activate "${env_name}"
cd "${script_dir}"
pip install -e ".[ci,dev]"  # lets you make changes without re-installing

unset env_name
unset script_dir
unset env_file
