"""
Module provides optimization problems including robotic
navigation and motion planning
"""

from .problem import (
    Constraint,
    Problem,
    LeastSquaresProblem,
    GeneralizedLeastSquaresProblem,
)  # isort:skip

# Local Folder
from .basic_tests import F0, F1, F2, Quadratic2D, Rosenbrock
from .constrained_circle_problem import CircleEquality, CircleInequality
from .ggn_test_problems import DiehlGGNProblem
from .iiwa_ik import IiwaIK
from .ik_problem import PandaIK, TwoLinkIK
from .point_mass_traj_problem import PointMassAccelerationFormulation, PointMassPositionFormulation
from .problem_conversion import InequalityToEqualityWrapper
from .stein_variational import SteinWrapper
from .gaussian_mixture_model import GMM2D
from .circle_distance_problem import CircleDistanceProblem

__all__ = [
    "Problem",
    "LeastSquaresProblem",
    "GeneralizedLeastSquaresProblem",
    "Constraint",
    "DiehlGGNProblem",
    "PandaIK",
    "TwoLinkIK",
    "IiwaIK",
    "PointMassAccelerationFormulation",
    "PointMassPositionFormulation",
    "CircleEquality",
    "CircleInequality",
    "F0",
    "F1",
    "F2",
    "Quadratic2D",
    "Rosenbrock",
    "InequalityToEqualityWrapper",
    "SteinWrapper",
    "GMM2D",
    "CircleDistanceProblem",
]
