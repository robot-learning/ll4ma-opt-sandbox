"""
Module to store simple problems for testing solvers
"""

# Third Party
# import matplotlib
# matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
import numpy as np
import torch

# ll4ma
from ll4ma_opt.problems import Problem, LeastSquaresProblem


class Func1D(Problem):
    """
    Super class for 1D problems
    """

    def __init__(self, x_min_disp=-15.0, x_max_disp=15.0, disp_res=100):
        super().__init__()
        self.x_min_disp = x_min_disp
        self.x_max_disp = x_max_disp
        self.disp_res = disp_res
        self.initial_solution = np.zeros((1, 1))

    def visualize_optimization(self, result=None, *args, **kwargs):
        """
        Draw a contour of the costs and draw the iterates over time
        TODO: Optionally animate the iterates
        """
        q_xx = np.linspace(self.x_min_disp, self.x_max_disp, self.disp_res)
        q_cc = self.cost(q_xx)
        if result and result.iterates is not None:
            c_iterates = self.cost(result.iterates)

        plt.figure()
        # Plot cost function
        plt.plot(q_xx, q_cc)
        # Plot iterates over time
        if result and result.iterates is not None:
            plt.plot(result.iterates.flatten(), c_iterates.flatten(), ".-r")
        plt.xlabel("$x$")
        plt.ylabel("$f(x)$")
        plt.show()

    def size(self):
        return 1

    def cost(self, x):
        return x.item() if isinstance(x, np.ndarray) and len(x) == 1 else x


class Func2D(Problem):
    """
    Super class for 2D problems
    """

    def __init__(self, x_min_disp, x_max_disp, y_min_disp, y_max_disp, disp_res):
        super().__init__()
        self.x_min_disp = x_min_disp
        self.x_max_disp = x_max_disp
        self.y_min_disp = y_min_disp
        self.y_max_disp = y_max_disp
        self.disp_res = disp_res
        self.levels = None
        self.initial_solution = np.zeros((2, 1))

    #
    # Plotting functions, should maybe put elsewhere...
    #
    def visualize_optimization(self, result=None, *args, **kwargs):
        """
        Draw a contour of the costs and draw the iterates over time
        """
        q_xx, q_yy = np.meshgrid(
            np.linspace(self.x_min_disp, self.x_max_disp, self.disp_res),
            np.linspace(self.y_min_disp, self.y_max_disp, self.disp_res),
        )
        q_cc = np.zeros(q_xx.shape)

        for r in range(q_xx.shape[0]):
            for c in range(q_xx.shape[1]):
                q_cc[r, c] = self.cost(np.array([[q_xx[r, c]], [q_yy[r, c]]]))
        q_cc = np.array(q_cc)
        plt.figure()
        if self.levels is not None:
            plt.contour(q_xx, q_yy, q_cc, self.levels)
        else:
            plt.contour(q_xx, q_yy, q_cc)
        if result and result.iterates is not None:
            plt.plot(result.iterates[:, 0, :], result.iterates[:, 1, :], ".-r")
        plt.xlabel("$x_0$")
        plt.ylabel("$x_1$")
        plt.show()

    def size(self):
        return 2

    def cost(self, x):
        return x.item() if isinstance(x, np.ndarray) and len(x) == 1 else x


class F0(Func1D):
    def __init__(self):
        super().__init__(-15.0, 15.0, 100)

    def cost(self, x):
        """
        Minimum is at -0.5, attains value -0.25
        """
        return super().cost(x**2 + x)

    def cost_gradient(self, x):
        return 2 * x + 1

    def cost_hessian(self, x):
        return np.array([[2.0]])


class F1(Func1D):
    def __init__(self, x_min_disp=-7, x_max_disp=7, disp_res=100):
        super().__init__(x_min_disp, x_max_disp, disp_res)

    def cost(self, x):
        return super().cost(-0.1 * x**3 + 0.5 * x**2 + x + 3)

    def cost_gradient(self, x):
        return -0.3 * x**2 + x + 1.0

    def cost_hessian(self, x):
        return -0.6 * x + 1.0


class F2(Func1D):
    def __init__(self, x_min_disp=-7, x_max_disp=7, disp_res=100):
        super().__init__(x_min_disp, x_max_disp, disp_res)

    def cost(self, x):
        return super().cost(0.1 * x**4 + -5.0 * x**2 + 4 * x + 3)

    def cost_gradient(self, x):
        return 0.4 * x**3 - 10 * x + 4.0

    def cost_hessian(self, x):
        return 0.12 * x**2 - 10.0


class Rosenbrock(Func2D):
    def __init__(self, a=1.0, b=100.0):
        super().__init__(-3.0, 3.0, -3.0, 5.0, 500)
        self.a = a
        self.b = b
        self.levels = 30
        self.initial_solution = np.array([[0.0], [0.0]])

    def cost(self, x):
        """
        Rosenbrock function for 2D
        Attains a minimum value of 0 at (1,1)
        """
        return super().cost((self.a - x[0, 0]) ** 2 + self.b * (x[1, 0] - x[0, 0] ** 2) ** 2)

    def cost_gradient(self, x):
        d_d_x0 = (
            -2.0 * self.a
            + 2.0 * x[0, 0]
            - 4.0 * self.b * x[1, 0] * x[0, 0]
            + 4.0 * self.b * x[0, 0] ** 3
        )
        d_d_x1 = 2.0 * self.b * (x[1, 0] - x[0, 0] ** 2)
        return np.array([[d_d_x0], [d_d_x1]])

    def cost_hessian(self, x):
        d_d_00 = 12.0 * self.b * x[0, 0] ** 2 - 4.0 * self.b * x[1, 0] + 2.0
        d_d_01 = -4.0 * self.b * x[0, 0]
        d_d_11 = 2.0 * self.b
        return np.array([[d_d_00, d_d_01], [d_d_01, d_d_11]])


# 2D quadratic inspired by Figure 3.7 from Nocedal & Wright
class Quadratic2D(Func2D):
    def __init__(self):
        super().__init__(-15.0, 15.0, -15.0, 15.0, 100)
        self.initial_solution = np.array([[-10.0], [10.0]])
        self.Q = np.array([[30.0, 0.0], [0.0, 3.0]])
        self.b = np.array([[1.0], [0.0]])
        self.x_min_disp = -15.0
        self.x_max_disp = 15.0
        self.y_min_disp = -15.0
        self.y_max_disp = 15.0
        self.disp_res = 100

    def cost(self, x):
        """
        2D quadratic cost funciton
        """
        return super().cost(0.5 * x.T @ self.Q @ x - self.b.T @ x)

    def cost_gradient(self, x):
        return self.Q @ x - self.b

    def cost_hessian(self, x):
        return self.Q


class Func2DLeastSquares(LeastSquaresProblem):
    """
    Slide 8.3: https://www.seas.ucla.edu/~vandenbe/133A/lectures/ls.pdf

    We are looking for vector x that minimizes:
    f(x) = 1/2 ||Jx - y||^2

              [2  0]
    where J = [-1 1]
              [0  2]

    and y = [1 0 -1].T

    It follows: f(x) = 1/2 [(2*x1 - 1)^2 + (-x1 + x2)^2 + (2*x2 +1)^2]

    And solution is [1/3, -1/3].T, for which f(x) = 1/3
    """

    def __init__(self):
        super().__init__()
        self.x_min_disp = -5.0
        self.x_max_disp = 5.0
        self.y_min_disp = -10.0
        self.y_max_disp = 5.0
        self.disp_res = 100
        self.levels = None
        self.initial_solution = np.zeros((2, 1))

        self.initial_solution = np.array([[0.0], [-10.0]])
        self.J = np.array([[2.0, 0], [-1, 1], [0, 2]])
        self.y = np.array([[1.0], [0], [-1]])

        self.torchJ = torch.tensor(self.J)
        self.torchy = torch.tensor(self.y)

    def jacobian(self, x, *args):
        if torch.is_tensor(x):
            return self.torchJ
        return self.J

    def residual(self, x):
        assert x.shape == (2, 1), x.shape
        if torch.is_tensor(x):
            return self.torchJ @ x - self.torchy
        return self.jacobian(x) @ x - self.y

    def get_cost(self, x):
        res = self.residual(x)
        norm = np.linalg.norm(res) if not torch.is_tensor(x) else torch.linalg.norm(res)
        return 0.5 * norm**2

    def get_solution(self):
        return np.array([[1 / 3], [-1 / 3]])

    def cost(self, x):
        return self.get_cost(x)

    def tensor_error(self, x):
        r = self.torchJ @ x - self.torchy
        return r

    def size(self):
        return 2

    #
    # Plotting functions, should maybe put elsewhere...
    #
    def visualize_optimization(self, result=None, *args, **kwargs):
        """
        Draw a contour of the costs and draw the iterates over time
        """
        q_xx, q_yy = np.meshgrid(
            np.linspace(self.x_min_disp, self.x_max_disp, self.disp_res),
            np.linspace(self.y_min_disp, self.y_max_disp, self.disp_res),
        )
        q_cc = np.zeros(q_xx.shape)

        for r in range(q_xx.shape[0]):
            for c in range(q_xx.shape[1]):
                q_cc[r, c] = self.cost(np.array([[q_xx[r, c]], [q_yy[r, c]]]))
        q_cc = np.array(q_cc)
        plt.figure()
        if self.levels is not None:
            plt.contour(q_xx, q_yy, q_cc, self.levels)
        else:
            plt.contour(q_xx, q_yy, q_cc)
        if result and result.iterates is not None:
            plt.plot(result.iterates[:, 0, :], result.iterates[:, 1, :], ".-r")
        plt.xlabel("$x_0$")
        plt.ylabel("$x_1$")
        plt.show()
