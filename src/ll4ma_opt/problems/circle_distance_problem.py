# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 18:10:53 2023

@author: tabor
"""

from ll4ma_opt.problems.gaussian_mixture_model import Animated2DProblem
from matplotlib.patches import Ellipse
import mpl_toolkits.mplot3d.art3d as art3d

import numpy as np
import torch


class CircleDistanceProblem(Animated2DProblem):
    def __init__(self, math_library=np, device="cpu", dtype=torch.float64):
        super().__init__(math_library)
        self.min_bounds[0] = -5
        self.min_bounds[1] = -5

        self.max_bounds[0] = 5
        self.max_bounds[1] = 5

        self.point = np.random.uniform(self.min_bounds / 2, self.max_bounds / 2)
        self.distance = np.array([3])

        self._point = torch.tensor(self.point, device=device, dtype=dtype)
        self._distance = torch.tensor(self.distance, device=device, dtype=dtype)
        self.initial_solution = np.zeros((2, 1))

        if math_library is torch:
            self.make_tensor = lambda x: x
            self.unmake_tensor = lambda x: x.detach()
            self.initial_solution = torch.tensor(self.initial_solution, device=device, dtype=dtype)
            self.min_bounds = torch.tensor(self.min_bounds, device=device, dtype=dtype)
            self.max_bounds = torch.tensor(self.max_bounds, device=device, dtype=dtype)

    def tensor_cost(self, x):
        error = x - self._point
        distance = torch.sqrt(error.T @ error)
        distance_error = self._distance - distance
        cost = distance_error * distance_error
        return cost

    def tensor_batch_cost(self, x):
        errors = x - self._point
        distances = torch.linalg.norm(errors, dim=1)
        distances_error = self._distance - distances
        costs = distances_error**2
        return costs

    def draw_extras(self, ax, show_costs, make_heatmap):
        if make_heatmap:
            return
        if not show_costs:
            ax.axis("equal")
        el = Ellipse(
            self.point.flatten(),
            width=self.distance * 2,
            height=self.distance * 2,
            facecolor=(0, 0, 0, 0),
            edgecolor="r",
        )
        ax.add_patch(el)
        if show_costs:
            art3d.pathpatch_2d_to_3d(el, z=0, zdir="z")
