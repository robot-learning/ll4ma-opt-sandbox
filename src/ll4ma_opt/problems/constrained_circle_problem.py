#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  1 16:28:36 2021

@author: tabor
"""
# Third Party
import matplotlib.pyplot as plt
import numpy as np
import torch

# ll4ma
from ll4ma_opt.problems import Constraint, Problem


class CircleEquality(Problem):
    def __init__(self):
        super().__init__()
        self.initial_solution = np.zeros((2, 1))
        self.equality_constraints.append(CircleConstraint())

    def tensor_cost(self, x):
        return torch.sum(x)

    def tensor_error(self, x):
        return x

    def visualize_optimization(self, result):
        angles = np.linspace(-2 * np.pi, 2 * np.pi, 1000)
        x = np.cos(angles) * np.sqrt(2)
        y = np.sin(angles) * np.sqrt(2)
        plt.figure()
        plt.plot(x, y)
        # Uncomment to plot path not just points
        # plt.plot(result.iterates[:,0,:], result.iterates[:,1,:])
        plt.scatter(
            result.iterates[:, 0, :],
            result.iterates[:, 1, :],
            c=range(result.iterates.shape[0]),
            cmap="cool",
        )

    def size(self):
        return 2


class CircleConstraint(Constraint):
    def tensor_error(self, x):
        return torch.sum(x**2) - 2.0

    def error(self, x):
        return np.sum(x**2) - 2.0

    def error_gradient(self, x):
        return 2 * x

    def error_hessian(self, x):
        return 2 * np.eye(x.shape[0])


class CircleInequality(CircleEquality):
    def __init__(self):
        super().__init__()
        self.inequality_constraints.append(Constraint(lambda x: torch.sum(x**2) - 2.0))
        self.equality_constraints = []
