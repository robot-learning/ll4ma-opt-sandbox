#!/usr/bin/env python
# Third Party
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import torch


class PolygonEnvironment:
    def __init__(self):
        self.obstacles = {}
        self.sdfs = {}

    def add_obstacle(self, name, config):
        self.obstacles[name] = config
        self.sdfs[name] = self._sdf_factory(config)

    def draw_obstacles(self, ax):
        for name, config in self.obstacles.items():
            if config["type"] == "rectangle":
                x_min, y_min, x_max, y_max = config["bounds"]
                patch = patches.Rectangle((x_min, y_min), x_max - x_min, y_max - y_min)
            elif config["type"] == "sphere":
                patch = patches.Circle(config["origin"], config["radius"])
            else:
                raise ValueError(f"Unknown obstacle type: {config['type']}")
            ax.add_patch(patch)

    def draw_scene(self):
        """
        This is currently more for debugging purposes, should add some better visualization
        options e.g. having env config
        """
        fig, ax = plt.subplots(1, 1)
        self.draw_obstacles(ax)
        fig.set_size_inches(8, 8)
        ax.axis("equal")
        plt.tight_layout()
        plt.show()

    def _sdf_factory(self, config):
        if config["type"] == "sphere":
            origin = torch.tensor(config["origin"]).unsqueeze(0)
            return lambda x: (x - origin).square().sum(dim=-1) - config["radius"] ** 2
        else:
            raise ValueError(f"Unsupported type for SDF: {config['type']}")


if __name__ == "__main__":
    # Third Party
    import numpy as np

    env = PolygonEnvironment()
    env.add_obstacle("circle", {"type": "sphere", "origin": [0, 0], "radius": 1})
    # env.draw_scene()

    test_input = torch.tensor([[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0]])
    print(env.sdfs["circle"](test_input))

    n_points = 500
    x = np.linspace(-10, 10, n_points)
    y = np.linspace(-10, 10, n_points)
    X, Y = np.meshgrid(x, y)
    pos = torch.tensor(np.array([X.flatten(), Y.flatten()]).T)
    Z = env.sdfs["circle"](pos).reshape(n_points, n_points)
    plt.imshow(Z, extent=[-10, 10, -10, 10])
    plt.show()
