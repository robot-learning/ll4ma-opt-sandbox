#!/usr/bin/env python
# Standard Library
import os

# Third Party
import matplotlib.pyplot as plt
import numpy as np
import numpy.matlib
import torch
from ll4ma_util import file_util
from matplotlib.animation import FuncAnimation
from torch.distributions import MultivariateNormal, Normal, Uniform

# ll4ma
from ll4ma_opt.environments import PolygonEnvironment
from ll4ma_opt.problems import Constraint, Problem

CONFIG_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config")
# DEFAULT_DUBINS_CONFIG = os.path.join(CONFIG_DIR, "dubins.yaml")
DEFAULT_DUBINS_CONFIG = os.path.join(CONFIG_DIR, "dubins_spheres.yaml")
# DEFAULT_DI_CONFIG = os.path.join(CONFIG_DIR, "double_integrator.yaml")
DEFAULT_DI_CONFIG = os.path.join(CONFIG_DIR, "double_integrator_spheres.yaml")

# Double integrator state
POS = slice(0, 2)
VEL = slice(2, 4)
ACC = slice(4, 6)
# Dubins state
X = 0
Y = 1
THETA = 2
# Dubins action
U = 0
V = 1
DURATION = 2


class SDConstraint(Constraint):
    def __init__(self, sdf, timestep, horizon, pos_idxs):
        self.sdf = sdf
        self.timestep = timestep
        self.horizon = horizon
        self.pos_idxs = pos_idxs

    def tensor_error(self, x):
        pos = x[self.pos_idxs].reshape(self.horizon, -1)
        sdf = self.sdf(torch.tensor(pos[self.timestep])).item()
        return sdf


class DynamicsConstraint(Constraint):
    def __init__(self, timestep, dt, horizon, pos_idxs, vel_idxs, acc_idxs):
        self.timestep = timestep
        self.dt = dt
        self.horizon = horizon
        self.pos_idxs = pos_idxs
        self.vel_idxs = vel_idxs
        self.acc_idxs = acc_idxs

    def tensor_error(self, x):
        pos = x[self.pos_idxs].reshape(self.horizon, -1)
        vel = x[self.vel_idxs].reshape(self.horizon, -1)
        acc = x[self.acc_idxs].reshape(self.horizon, -1)
        t = self.timestep

        vel_dyn = np.square(vel[t + 1] - (vel[t] + (acc[t] * self.dt))).sum()
        pos_dyn = np.square(pos[t + 1] - (pos[t] + (vel[t] * self.dt))).sum()

        return vel_dyn + pos_dyn


class StartConstraint(Constraint):
    def __init__(self, start_pos, horizon, pos_idxs, vel_idxs, acc_idxs):
        self.start_pos = start_pos
        self.horizon = horizon
        self.pos_idxs = pos_idxs
        self.vel_idxs = vel_idxs
        self.acc_idxs = acc_idxs

    def tensor_error(self, x):
        pos = x[self.pos_idxs].reshape(self.horizon, -1)
        vel = x[self.vel_idxs].reshape(self.horizon, -1)
        acc = x[self.acc_idxs].reshape(self.horizon, -1)
        pos_constraint = np.square(self.start_pos - pos[0]).sum()
        vel_constraint = np.square(vel).sum()
        acc_constraint = np.square(acc).sum()
        return pos_constraint + vel_constraint + acc_constraint


class PolygonNavigation2D(Problem):
    """
    Navigation among 2D polygonal obstacles.
    """

    def __init__(self, config, horizon=35):
        self.horizon = horizon
        super().__init__()

        self.state = np.zeros(self.state_size())
        self.initial_solution = np.random.rand(self.size()).reshape(-1, 1)

        self.env = PolygonEnvironment()
        self.obstacles = []

        if isinstance(config, dict):
            self.config = config
        elif isinstance(config, str):
            self.config = file_util.load_yaml(config)
        else:
            raise ValueError(f"Unknown type for config: {type(config)}")

        for name, cfg in self.config["obstacles"].items():
            self.env.add_obstacle(name, cfg)

        if "start" in self.config and "point" in self.config["start"]:
            self.start = self.config["start"]["point"]
        if "goal" in self.config and "point" in self.config["goal"]:
            self.goal = self.config["goal"]["point"]
        self.batch_goal = None

        # Assuming state has been specified in order
        self.state[: len(self.start)] = np.array(self.start)

        # # TODO Add SDF inequality constraints for obstacle collisions
        # self.inequality_constraints = [SDConstraint(self.env.sdfs['center_sphere'], self, i)
        #                               for i in range(horizon)]

    def cost(self, x):
        return self.batch_cost(torch.tensor(x).view(1, -1)).item()

    def tensor_cost(self, x):
        # return self.batch_cost(x.view(1, -1))
        return self.batch_cost(x.view(1, -1))

    def batch_cost(self, x):
        batch_size = x.size(0)
        x = x.view(batch_size, self.horizon, self.act_size())  # (B, H, 3)
        start = torch.tensor(self.state).unsqueeze(0).repeat(batch_size, 1)  # (B, 3)
        traj = self.get_trajectory(start, x)  # (B, H+1, 3)
        cost = self.goal_cost(traj[:, -1])

        # TODO trying caching
        self.traj = traj.squeeze(0)  # Remove batch dim

        # cost += self.collision_cost(traj)

        # TODO would be better to have smoothness cost instead, this just penalizes
        # magnitude to avoid the weird solns coming out of gradient optimizer
        cost += x.square().sum() * 0.1

        return cost

    def goal_cost(self, end_state):
        batch_size = end_state.size(0)
        if self.batch_goal is None or self.batch_goal.size(0) != batch_size:
            self.batch_goal = torch.tensor(self.goal).unsqueeze(0).repeat(batch_size, 1)  # (B, 3)
        goal_cost = (self.batch_goal - end_state).square().sum(dim=-1)
        return goal_cost

    def size(self):
        raise NotImplementedError()

    def act_size(self):
        raise NotImplementedError()

    def state_size(self):
        raise NotImplementedError()

    def apply_action(self, act):
        """
        Applies action to the environment.

        Args:
            act (Tensor): Action to be applied shape (batch, 3)
        """
        state = torch.tensor(self.state).unsqueeze(0)
        next_state = self.dynamics(state, act)
        self.state = next_state.squeeze().numpy()
        return next_state

    def dynamics(self, state, act, *args, **kwargs):
        """
        State dynamics function. Given initial state and action, generate successor state.

        B, A = batch_size, act_size

        Args:
            state (Tensor): Start state of shape (B, A)
            act (Tensor): Action to apply of shape (B, A)
        Returns:
            next_state (Tensor): Next state after applying action from current state (B, A)
        """
        raise NotImplementedError("Extending classes must implement dynamics function")

    def get_trajectory(self, start_state, actions, *args, **kwargs):
        """
        Applies dynamics from start state with action sequence to get trajectories.

        B, H, A = batch_size, horizon, act_size

        Args:
            start_state (Tensor): Start to execute action trajectories from (B, A)
            actions (Tensor): Action trajectories to apply (B, H, A)
            noise (float): Gain factor on Gaussian dynamics noise
        Returns:
            trajs (Tensor): Computed trajectories from action sequences (B, H+1, A).
                            Note this includes start state, hence length H+1.
        """
        B, H, _ = actions.size()
        trajs = torch.zeros(B, H + 1, self.state_size())
        trajs[:, 0] = start_state
        for t in range(H):
            trajs[:, t + 1] = self.dynamics(trajs[:, t], actions[:, t], *args, **kwargs)
        return trajs

    def visualize_optimization(self, result, repeat_delay=0):
        have_samples = result.sample_iterates is not None
        iterates = [x[self.pos_idxs].reshape(self.horizon, -1) for x in result.iterates]

        # if have_samples:
        #     samples = [torch.tensor(s.reshape(-1, self.horizon, self.act_size()))
        #                for s in result.sample_iterates]
        #     n_samples = samples[0].size(0)
        #     sample_trajs = [self.get_trajectory(start, s) for s in samples]

        fig, ax = plt.subplots(1, 1)
        self.draw_env(ax)

        path = ax.plot([], [], lw=8, color="goldenrod")[0]
        sample_paths = (
            [ax.plot([], [], lw=1, alpha=0.1, color="g")[0] for _ in range(n_samples)]
            if have_samples
            else []
        )

        def animate(i):
            ax.set_title(f"Iteration {i}")
            path.set_data(iterates[i][:, 0], iterates[i][:, 1])
            # if have_samples:
            #     for j in range(n_samples):
            #         sample_paths[j].set_data(sample_trajs[i][j][:,0], sample_trajs[i][j][:,1])
            return [path] + sample_paths

        fig.set_size_inches(8, 8)
        ax.axis("equal")

        anim = FuncAnimation(
            fig, animate, frames=len(iterates), blit=False, repeat_delay=repeat_delay
        )
        # anim.save(FILENAME, writer='ffmpeg', fps=5)  # TODO add a save option like this
        plt.show()
        return anim

    def draw_env(self, ax, obstacles=True, start=True, goal=True):
        if goal:
            self.draw_goal(ax)
        if start:
            self.draw_start(ax)
        if obstacles:
            self.draw_obstacles(ax)

    def draw_obstacles(self, ax):
        self.env.draw_obstacles(ax)

    def draw_agent(self, ax):
        cfg = self.config["agent"]
        return ax.scatter(*self.state[:2], c=cfg["color"], s=cfg["size"])

    def draw_start(self, ax):
        cfg = self.config["start"]
        return ax.scatter(*cfg["point"][:2], c=cfg["color"], s=cfg["size"])

    def draw_goal(self, ax):
        cfg = self.config["goal"]
        return ax.scatter(*cfg["point"][:2], c=cfg["color"], s=cfg["size"])


class DubinsCar(PolygonNavigation2D):
    """
    Navigation among 2D polygonal obstacles with Dubins car dynamics.

    https://en.wikipedia.org/wiki/Dubins_path
    """

    def __init__(self, config=DEFAULT_DUBINS_CONFIG, horizon=35):
        super().__init__(config, horizon)

        self.min_bounds = np.tile(
            [
                -np.tan(self.config["agent"]["phi_limit"]),
                self.config["agent"]["v_limits"][0],
                self.config["agent"]["dur_limits"][0],
            ],
            horizon,
        ).reshape(-1, 1)
        self.max_bounds = np.tile(
            [
                np.tan(self.config["agent"]["phi_limit"]),
                self.config["agent"]["v_limits"][1],
                self.config["agent"]["dur_limits"][1],
            ],
            horizon,
        ).reshape(-1, 1)

        # self.initial_solution = np.tile([0,
        #                                  self.config['agent']['v_limits'][1] / 2.,
        #                                  self.config['agent']['dur_limits'][1]],
        #                                 horizon).reshape(-1, 1)

    def dynamics(self, state, act, noise=0.0):
        """
        Dubins car dynamics (https://en.wikipedia.org/wiki/Dubins_path).

        B, A = batch_size, act_size

        Args:
            state (Tensor): Start state of shape (B, A)
            act (Tensor): Action to apply of shape (B, A)
            noise (float): Gain factor for additive Gaussian noise to dynamics
        Returns:
            next_state (Tensor): Next state after applying action on current state and feeding
                                 through nonlinear stochastic dynamics of shape (B, A)
        """
        u = act[:, U] + 1e-5  # Avoid divide by zero and boolean checks for DBZ
        v = act[:, V]
        dt = act[:, DURATION]
        dt_u = dt * u
        theta = state[:, 2].clone()
        v_over_u = v / u

        next_state = state.clone()
        next_state[:, X] += v_over_u * (torch.sin(theta + dt_u) - torch.sin(theta))
        next_state[:, Y] += v_over_u * (torch.cos(theta) - torch.cos(theta + dt_u))
        next_state[:, THETA] += dt_u

        if noise > 0:
            next_state += torch.randn_like(next_state) * noise  # Stochastic transition

        return next_state

    def get_trajectory(self, start_state, actions, noise=0.0):
        return super().get_trajectory(start_state, actions, noise)

    def size(self):
        return self.act_size() * self.horizon

    def act_size(self):
        return 3  # u, v, dt (turn rate, velocity, duration)

    def state_size(self):
        return 3  # x, y, theta (planar pose)


class DoubleIntegrator(PolygonNavigation2D):
    """
    Navigation among 2D polygonal obstacles with double integrator dynamics.
    """

    def __init__(self, config=DEFAULT_DI_CONFIG, horizon=35):
        super().__init__(config, horizon)

        min_pos = np.tile([self.config["agent"]["min_pos"]], horizon)
        max_pos = np.tile([self.config["agent"]["max_pos"]], horizon)
        min_vel = np.tile([self.config["agent"]["min_vel"]], horizon)
        max_vel = np.tile([self.config["agent"]["max_vel"]], horizon)
        min_acc = np.tile([self.config["agent"]["min_acc"]], horizon)
        max_acc = np.tile([self.config["agent"]["max_acc"]], horizon)

        self.min_bounds = np.concatenate([min_pos, min_vel, min_acc]).reshape(-1, 1)
        self.max_bounds = np.concatenate([max_pos, max_vel, max_acc]).reshape(-1, 1)

        self.pos_idxs = slice(0, 2 * horizon)
        self.vel_idxs = slice(2 * horizon, 4 * horizon)
        self.acc_idxs = slice(4 * horizon, 6 * horizon)

        self.equality_constraints = []

        self.equality_constraints += [
            DynamicsConstraint(i, 0.1, horizon, self.pos_idxs, self.vel_idxs, self.acc_idxs)
            for i in range(horizon - 1)
        ]

        self.equality_constraints += [
            StartConstraint(
                np.array(self.config["start"]["point"]),
                horizon,
                self.pos_idxs,
                self.vel_idxs,
                self.acc_idxs,
            )
        ]

        self.inequality_constraints = [
            SDConstraint(self.env.sdfs["center_sphere"], i, horizon, self.pos_idxs)
            for i in range(horizon)
        ]

        start = self.config["start"]["point"]
        goal = self.config["goal"]["point"]
        init_x = np.linspace(start[0], goal[0], horizon)
        init_y = np.linspace(start[1], goal[1], horizon)
        init_pos = np.stack([init_x, init_y], axis=1).flatten()
        # init_vel = np.zeros(2 * horizon)
        init_vel = np.zeros(2 * horizon)
        init_acc = np.zeros(2 * horizon)
        self.initial_solution = np.concatenate([init_pos, init_vel, init_acc])

    def batch_cost(self, x):
        B = x.size(0)
        pos = x[:, self.pos_idxs].view(B, self.horizon, -1)

        cost = self.goal_cost(pos[:, -1])

        # # TODO smoothness
        # smooth_cost = 0
        # for i in range(self.horizon - 2):
        #     smooth_cost += (pos[:,i] - 2 * pos[:,i+1] + pos[:,i+2]).square().sum()

        # cost += smooth_cost * 1e-2

        return cost

    def dynamics(self, state, act, noise=0.0, dt=0.2):
        """
        Double integrator dynamics.

        B, A = batch_size, act_size

        Args:
            state (Tensor): Start state of shape (B, A)
            act (Tensor): Action to apply of shape (B, A)
            noise (float): Gain factor for additive Gaussian noise to dynamics
        Returns:
            next_state (Tensor): Next state after applying action on current state and feeding
                                 through nonlinear stochastic dynamics of shape (B, A)
        """
        # Assuming state is pos/vel/acc (each x,y), and act is (possibly noisy) acc (x,y)
        if noise > 0:
            act += torch.randn_like(act) * noise  # Noise on accelerations
        next_state = state.clone()
        next_state[:, ACC] = act
        next_state[:, VEL] += next_state[:, ACC] * dt
        next_state[:, POS] += next_state[:, VEL] * dt

        return next_state

    def size(self):
        return self.horizon * self.act_size()  # 2D pos/vel/acc over time

    def act_size(self):
        return 6

    def state_size(self):
        return 6  # x, y pos/vel/acc

    def goal_cost(self, end_state):
        batch_size = end_state.size(0)
        if self.batch_goal is None or self.batch_goal.size(0) != batch_size:
            self.batch_goal = torch.tensor(self.goal).unsqueeze(0).repeat(batch_size, 1)  # (B, 2)
        # TODO probably can just use same base cost but currently the goal is just
        # going to be pos while state includes pos/vel/acc, so just need way to
        # make them operate on the same thing
        goal_cost = (self.batch_goal - end_state).square().sum(dim=-1)
        return goal_cost


if __name__ == "__main__":
    """
    Simple example showing how to apply actions and draw environment.
    """
    # Standard Library
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--problem", type=str, choices=["dubins", "di"], default="dubins")
    args = parser.parse_args()

    if args.problem == "dubins":
        problem = DubinsCar()
        traj = (
            [torch.tensor([0.0, 0.2, 0.5]).unsqueeze(0) for _ in range(30)]
            + [torch.tensor([0.1, 0.2, 0.5]).unsqueeze(0) for _ in range(50)]
            + [torch.tensor([0.0, 0.2, 0.5]).unsqueeze(0) for _ in range(50)]
        )
    elif args.problem == "di":
        problem = DoubleIntegrator()
        traj = (
            [torch.tensor([0.1, 0.2]).unsqueeze(0) for _ in range(50)]
            + [torch.tensor([-0.1, 0.0]).unsqueeze(0) for _ in range(60)]
            + [torch.tensor([-0.1, -0.1]).unsqueeze(0) for _ in range(60)]
        )
    else:
        raise ValueError(f"Unknown problem type: {args.problem}")

    fig, ax = plt.subplots(1, 1)
    problem.draw_env(ax)
    agent = problem.draw_agent(ax)

    points = []
    for act in traj:
        problem.apply_action(act)
        points.append(np.array(problem.state))

    def animate(i):
        agent.set_offsets(points[i][:2])
        return (agent,)

    fig.set_size_inches(8, 8)
    ax.axis("equal")
    plt.tight_layout()

    anim = FuncAnimation(fig, animate, frames=len(points), interval=10, blit=False)

    plt.show()
