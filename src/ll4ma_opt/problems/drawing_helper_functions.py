#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 17:58:29 2020

@author: tabor
"""
# Third Party
import numpy as np
from mpl_toolkits.mplot3d.art3d import Poly3DCollection


def build_rectangular_prism(center, length, width, height):
    cube_definition = []
    center = np.array(center)
    first = tuple(center - np.array([length, width, height]))
    cube_definition.append(first)
    for i in range(3):
        offset = np.array([-length, -width, -height])
        offset[i] = offset[i] * -1
        nextPoint = tuple(center + offset)
        cube_definition.append(nextPoint)
    return cube_definition


# this is stolen from stack overflow
def plot_rectangular_prism(cube_definition, colorinfo, ax):
    cube_definition_array = [np.array(list(item)) for item in cube_definition]

    points = []
    points += cube_definition_array
    vectors = [
        cube_definition_array[1] - cube_definition_array[0],
        cube_definition_array[2] - cube_definition_array[0],
        cube_definition_array[3] - cube_definition_array[0],
    ]
    points += [cube_definition_array[0] + vectors[0] + vectors[1]]
    points += [cube_definition_array[0] + vectors[0] + vectors[2]]
    points += [cube_definition_array[0] + vectors[1] + vectors[2]]
    points += [cube_definition_array[0] + vectors[0] + vectors[1] + vectors[2]]

    points = np.array(points)

    edges = [
        [points[0], points[3], points[5], points[1]],
        [points[1], points[5], points[7], points[4]],
        [points[4], points[2], points[6], points[7]],
        [points[2], points[6], points[3], points[0]],
        [points[0], points[2], points[4], points[1]],
        [points[3], points[6], points[7], points[5]],
    ]
    col = "k"
    faces = Poly3DCollection(edges, linewidths=1, edgecolors=col)
    faces.set_facecolor(colorinfo)

    ax.add_collection3d(faces)

    # Plot the points themselves to force the scaling of the axes
    ax.scatter(points[:, 0], points[:, 1], points[:, 2], s=0)
    # ax.set_aspect('equal')
