# Third Party
import matplotlib.pyplot as plt
import numpy as np
import torch

# ll4ma
from ll4ma_opt.problems import Problem


def rotate_pts(pts, rots):
    # Standard Library
    from math import cos, sin

    base = np.eye(3)
    xrot = base.copy()
    xrot[1, 1] = cos(rots[0])
    xrot[2, 2] = cos(rots[0])
    xrot[1, 2] = -sin(rots[0])
    xrot[2, 1] = sin(rots[0])
    yrot = base.copy()
    yrot[0, 0] = cos(rots[1])
    yrot[2, 2] = cos(rots[1])
    yrot[2, 0] = -sin(rots[1])
    yrot[0, 2] = sin(rots[1])
    zrot = base.copy()
    zrot[0, 0] = cos(rots[2])
    zrot[1, 1] = cos(rots[2])
    zrot[0, 1] = -sin(rots[2])
    zrot[1, 0] = sin(rots[2])
    base = np.matmul(zrot, np.matmul(yrot, xrot))
    return np.matmul(base, pts.T).T


class OrthonormalProblem(Problem):
    """docstring for OrthonormalProblem."""

    def __init__(self):
        super(OrthonormalProblem, self).__init__()

        theta = np.radians(np.arange(361, step=10))
        xyz = (
            np.atleast_2d(np.cos(theta)),
            np.atleast_2d(np.sin(theta)),
            np.atleast_2d(np.zeros(theta.shape)),
        )
        vecs = np.concatenate(xyz, 0).T
        move = True
        # if move:
        #     vecs += np.array([-3, 4, -2])
        if move:
            vecs = rotate_pts(vecs, np.radians([10, 10, 10]))
        # vecs = rotate_pts(vecs, np.radians([0, 0, 90]))
        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
        ax.plot(vecs[:, 0], vecs[:, 1], vecs[:, 2])
        # plt.savefig('points3d.png')
        self.points = vecs
        # plt.plot(vecs[:,0],vecs[:,1])
        # plt.show()
        # import pdb; pdb.set_trace()
        self.initial_solution = np.ones((3, 1)) / 3

    def cost(self, x):
        return np.sqrt(np.sum(np.power(np.matmul(self.points, x), 2)))

    def cost_gradient(self, x):
        x = torch.tensor(x, requires_grad=True)
        result = torch.sqrt(torch.sum(torch.pow(torch.matmul(torch.tensor(self.points), x), 2)))
        return torch.autograd.grad(result, x)[0].numpy()

    def constraints(self):
        def f(x):
            return np.sqrt(np.sum(np.power(x, 2))) - 1

        return [f]

    def constraint_grads(self):
        def f(x):
            return np.sum(x) / np.sqrt(np.sum(np.power(x, 2)))

        return [f]

    def size(self):
        return 3
