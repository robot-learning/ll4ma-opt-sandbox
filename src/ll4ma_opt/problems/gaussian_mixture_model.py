# -*- coding: utf-8 -*-
"""
Created on Fri Oct 28 14:34:04 2022

@author: tabor
"""
# ll4ma
from ll4ma_opt.problems import Problem

# Third Party
import numpy as np
import torch
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib.animation import FuncAnimation
import mpl_toolkits.mplot3d.art3d as art3d


class Animated2DProblem(Problem):
    def __init__(self, math_library=np):
        self.math_library = math_library
        super().__init__()

    # overwrite if you have additional visualizations you would like to draw
    def draw_extras(self, ax, show_costs, make_heatmap=False):
        pass

    def visualize_optimization(
        self, iterates, samples=50, func=None, show_costs=False, make_heatmap=False
    ):
        if not func:
            func = self.math_library.abs
        fig = plt.figure()
        if show_costs:
            ax = fig.add_subplot(projection="3d")
        else:
            ax = fig.add_subplot()

        if self.math_library is np:
            ax.set_xlim(self.min_bounds[0], self.max_bounds[0])
            ax.set_ylim(self.min_bounds[1], self.max_bounds[1])
        else:
            ax.set_xlim(self.min_bounds[0].cpu(), self.max_bounds[0].cpu())
            ax.set_ylim(self.min_bounds[1].cpu(), self.max_bounds[1].cpu())
        self.draw_extras(ax, show_costs, make_heatmap)

        if iterates is not None:
            points = iterates[0].reshape((-1, 2))

        if show_costs or make_heatmap:

            if self.math_library is np:
                possible_xs = self.math_library.linspace(
                    self.min_bounds[0], self.max_bounds[0], samples
                )
                possible_ys = self.math_library.linspace(
                    self.min_bounds[1], self.max_bounds[1], samples
                )
            elif self.math_library is torch:
                possible_xs = self.math_library.linspace(
                    self.min_bounds[0].cpu().numpy()[0],
                    self.max_bounds[0].cpu().numpy()[0],
                    samples,
                    device=self.min_bounds[1].device,
                )
                possible_ys = self.math_library.linspace(
                    self.min_bounds[1].cpu().numpy()[0],
                    self.max_bounds[1].cpu().numpy()[0],
                    samples,
                    device=self.min_bounds[1].device,
                )

            xs, ys = self.math_library.meshgrid(possible_xs, possible_ys, indexing="ij")
            xs = xs.flatten()
            ys = ys.flatten()

            sample_points = self.math_library.stack([xs, ys]).T.reshape((-1, self.size(), 1))

            sample_costs = func(self.batch_cost(sample_points))

        if show_costs:

            best_index = self.math_library.argmin(sample_costs)
            print(
                "best brute force cost ",
                sample_costs[best_index],
                " at ",
                xs[best_index],
                ys[best_index],
            )
            costs = self.batch_cost(points.reshape((-1, 2, 1))).reshape((-1, 1))

            if self.math_library is torch:
                # iterate points to cpu
                points = points.cpu().numpy()
                costs = costs.cpu().numpy()

                # sample points to cpu
                sample_points = sample_points.cpu().numpy()
                sample_costs = sample_costs.cpu().numpy()

            points_3d = np.concatenate((points, costs), 1)
            # silly hack to get filled circle
            ax.scatter(
                sample_points[:, 0, 0], sample_points[:, 1, 0], sample_costs, color=(0, 0, 1, 0.5)
            )

            scat = ax.scatter(
                points_3d[:, 0], points_3d[:, 1], points_3d[:, 2], linewidths=10, c="orange"
            )
            scat2 = ax.scatter(
                points_3d[:, 0], points_3d[:, 1], points_3d[:, 2], linewidths=5, c="orange"
            )

            scat._offsets3d = (points_3d[:, 0], points_3d[:, 1], points_3d[:, 2])
        else:

            if make_heatmap:
                c = ax.contourf(
                    xs.reshape((samples, samples)),
                    ys.reshape((samples, samples)),
                    sample_costs.reshape((samples, samples)),
                    150,
                    cmap="RdBu",
                )
                fig.colorbar(c, ax=ax)

            if self.math_library is torch:
                points = points.cpu()
            if iterates is not None:
                scat = plt.scatter(points[:, 0], points[:, 1], linewidths=5, c="orange")
                scat.set_offsets(points)
                scat2 = None

        def update(num, scat, scat2, iterates, show_costs):
            points = iterates[num].reshape((-1, 2))
            if show_costs:
                costs = self.batch_cost(points.reshape((-1, 2, 1))).reshape((-1, 1))

                if self.math_library is torch:
                    # iterate points to cpu
                    points = points.cpu().numpy()
                    costs = costs.cpu().numpy()

                points_3d = np.concatenate((points, costs), 1)
                scat._offsets3d = (points_3d[:, 0], points_3d[:, 1], points_3d[:, 2])
                scat2._offsets3d = (points_3d[:, 0], points_3d[:, 1], points_3d[:, 2])

            else:
                if self.math_library is torch:
                    points = points.cpu()
                scat.set_offsets(points)

        if iterates is not None:
            anim = FuncAnimation(
                fig,
                update,
                fargs=(scat, scat2, iterates, show_costs),
                frames=len(iterates),
                interval=100,
                repeat=True,
            )
            return anim
        else:
            return fig

    def size(self):
        return 2


class GMM2D(Animated2DProblem):
    def __init__(self, num=3, math_library=np, device="cpu", dtype=torch.float64):
        super().__init__(math_library)
        self.num = num
        self.means = np.random.random((num, 2, 1)) * 3
        self.covariance_inverses = np.array(
            [1.0 * np.diag(np.random.randint(1, 5, size=(2))) for i in range(num)]
        )
        self.weights = np.array([1 / num] * num)
        self.constants = [
            np.power(2 * np.pi, -num / 2) * np.sqrt(np.linalg.det(covariance_inverse))
            for covariance_inverse in self.covariance_inverses
        ]

        self._means = torch.tensor(self.means, dtype=dtype, device=device)
        self._covariance_inverses = torch.tensor(
            self.covariance_inverses, dtype=dtype, device=device
        )
        self._weights = torch.tensor(self.weights, dtype=dtype, device=device)
        self._constants = torch.tensor(self.constants, dtype=dtype, device=device)

        self.initial_solution = np.zeros((2, 1))
        self.initial_solution[0] = 1

        self.min_bounds[0] = 0
        self.min_bounds[1] = -1

        self.max_bounds[0] = 5
        self.max_bounds[1] = 4

        if math_library is torch:
            self.make_tensor = lambda x: x
            self.unmake_tensor = lambda x: x.detach()
            self.initial_solution = torch.tensor(self.initial_solution, device=device, dtype=dtype)
            self.min_bounds = torch.tensor(self.min_bounds, device=device, dtype=dtype)
            self.max_bounds = torch.tensor(self.max_bounds, device=device, dtype=dtype)

    def tensor_cost(self, x):
        cost = 0
        for mean, weight, cov_inverse, constant in zip(
            self._means, self._weights, self._covariance_inverses, self._constants
        ):
            error = x - mean
            dist = error.T @ cov_inverse @ error
            cost = cost + weight * constant * torch.exp(-0.5 * dist)
        return -torch.log(cost)

    def draw_extras(self, ax, show_costs, make_heatmap=False):
        # if not show_costs:
        #    ax.axis("equal")
        if make_heatmap:
            return
        for mean, weight, cov_inverse in zip(self.means, self.weights, self.covariance_inverses):
            for i in range(3):
                el = Ellipse(
                    (mean.flatten()),
                    width=i / cov_inverse[0, 0],
                    height=i / cov_inverse[1, 1],
                    facecolor=(0, 0, 0, 0),
                    edgecolor="r",
                )
                ax.add_patch(el)
                if show_costs:
                    art3d.pathpatch_2d_to_3d(el, z=0, zdir="z")

        if not show_costs:
            bounds_points = np.array(
                [
                    [self.min_bounds[0], self.min_bounds[1]],
                    [self.min_bounds[0], self.max_bounds[1]],
                    [self.max_bounds[0], self.max_bounds[1]],
                    [self.max_bounds[0], self.min_bounds[1]],
                    [self.min_bounds[0], self.min_bounds[1]],
                ]
            )
            ax.plot(bounds_points[:, 0], bounds_points[:, 1], linestyle="--")
