#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example problems for use with the generalized Gauss-Newton solver
"""
# Third Party
import numpy as np

# ll4ma
from ll4ma_opt.problems import GeneralizedLeastSquaresProblem
from ll4ma_opt.problems.resources.util import PseudoHuberLoss


class DiehlGGNProblem(GeneralizedLeastSquaresProblem):
    """
    Simple problem demonstrating the use of an outer convex loss
    for use with the generalized Gauss-Newton solver
    """

    def __init__(self):
        super().__init__()
        self.initial_solution = np.array([[1.0]])
        self.phl = PseudoHuberLoss()
        self.outer_convex_loss = self.phl.loss
        self.outer_convex_gradient = self.phl.gradient
        self.outer_convex_hessian = self.phl.hessian

    def residual(self, x):
        error = 0.75 * x + np.sin(x)
        return error

    def jacobian(self, x):
        return 0.75 + np.cos(x)

    def size(self):
        return 1
