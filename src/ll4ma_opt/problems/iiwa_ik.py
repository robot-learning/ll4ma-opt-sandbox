# Standard Library
import time

# Third Party
import numpy as np
import pybullet
import torch
from ll4ma_util import math_util
from pybullet_utils import bullet_client

# ll4ma
from ll4ma_opt.problems import Problem
from ll4ma_opt.problems.resources import pybullet_util

ERROR_TYPES = ["pose", "position", "phuber_position", "phuber_pose"]


class ChainIK(Problem):
    def __init__(
        self,
        chain,
        desired_pose,
        error_type="pose",
        orientation_error_type="log",
        use_collision=False,
        collision_env=[],
        phuber_delta=1e-2,
        soft_limits=False,
    ):
        self._bot = chain
        self.desired_pose = desired_pose  # TF matrix
        super().__init__()

        if error_type not in ERROR_TYPES:
            raise ValueError("Error type '{}' not one of {}".format(error_type, ERROR_TYPES))
        self.error_type = error_type
        self.orientation_error_type = orientation_error_type

        self.initial_solution = np.zeros((self._bot.num_joints, 1))

        self.use_collision = use_collision
        self._env = None
        self.inequality_constraints = []
        if use_collision:
            num_obstacles, obstacle_radius, object_range = collision_env
            # from ll4ma_opt.problems.resources.sphere_env import SphereEnvironment
            # Third Party
            from ll4ma_sdf_collisions.envs.sphere_sdf import RandomSphereSDF

            self._env = RandomSphereSDF(
                num_obstacles,
                object_range=object_range,
                radii_range=(obstacle_radius, obstacle_radius),
            )
            # ll4ma
            from ll4ma_opt.problems.resources.sdf_constraints import sdfEnv2Constraints

            self.inequality_constraints = sdfEnv2Constraints(
                self._env,
                self._bot.get_points,
                self._bot.num_points,
                self._bot.modify_sdf,
                delta=0.01,  # (obstacle_radius/2.)
            )

        if soft_limits:
            try:
                soft_lower = self._bot.soft_limits[: self._bot.num_joints].reshape(-1, 1).numpy()
            except Exception:
                jl = self._bot.soft_limits
                soft_lower = np.array([jl[i][0] for i in range(self.size())]).reshape(-1, 1)
            try:
                soft_upper = self._bot.soft_limits[: self._bot.num_joints].reshape(-1, 1).numpy()
            except Exception:
                jl = self._bot.soft_limits
                soft_upper = np.array([jl[i][1] for i in range(self.size())]).reshape(-1, 1)
            # ll4ma
            from ll4ma_opt.problems.resources.limit_constraint import SoftLimitConstraint

            self.inequality_constraints.append(
                SoftLimitConstraint(torch.tensor(soft_lower), torch.tensor(soft_upper))
            )

        try:
            self.min_bounds = self._bot.limits_lower[: self._bot.num_joints].reshape(-1, 1).numpy()
        except Exception:
            jl = self._bot.joint_limits
            self.min_bounds = np.array([jl[i][0] for i in range(self.size())]).reshape(-1, 1)

        try:
            self.max_bounds = self._bot.limits_upper[: self._bot.num_joints].reshape(-1, 1).numpy()
        except Exception:
            jl = self._bot.joint_limits
            self.max_bounds = np.array([jl[i][1] for i in range(self.size())]).reshape(-1, 1)

    def set_desired_pose(self, desired_pose):
        self.desired_pose = desired_pose

    def size(self):
        return self._bot.num_joints

    def set_weights(self, weights):
        self._bot.weights = torch.DoubleTensor(weights)

    def tensor_cost(self, x):
        cost = self._bot.pose_cost(x.squeeze(), self.desired_pose)
        return cost

    def tensor_error(self, x):
        err = self._bot.pose_error(x.squeeze(), self.desired_pose)
        return err

    def visualize_optimization(self, result, *args, client="pybullet", **kwargs):
        if client == "pybullet":
            self._visualize_optimization_pybullet(result, *args, **kwargs)
        elif client == "matplotlib":
            return self._visualize_optimization_matplotlib(result, *args, **kwargs)
        else:
            raise ValueError(f"Unknown visualization client: {client}")

    def _visualize_optimization_pybullet(self, result, step=0.1):
        client = bullet_client.BulletClient(connection_mode=pybullet.GUI)
        close_btn_id = client.addUserDebugParameter("Close Window", 1, 0, 1)
        client.resetDebugVisualizerCamera(
            cameraDistance=4,
            cameraYaw=52,
            cameraPitch=-35,
            cameraTargetPosition=[0, 0, 0],
        )
        # Turn off pybullet cameras
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_RGB_BUFFER_PREVIEW, 0)
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_DEPTH_BUFFER_PREVIEW, 0)
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_SEGMENTATION_MARK_PREVIEW, 0)

        # Load robot URDF
        # robot_id = pybullet_util.load_urdf(self._bot.urdf_path, client=client)
        self._bot.draw_pybullet(client)

        # Turn off pybullet cameras
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_RGB_BUFFER_PREVIEW, 0)
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_DEPTH_BUFFER_PREVIEW, 0)
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_SEGMENTATION_MARK_PREVIEW, 0)

        # Show target frame to reach to
        desired_position, desired_orientation = math_util.homogeneous_to_pose(self.desired_pose)
        # Need some object to attach frame to easily, using ducky
        pybullet_util.add_mesh(
            desired_position,
            desired_orientation,
            "duck.obj",
            [0, -0.05, 0],
            [0.1, 0.1, 0.1],
            client=client,
        )
        joints = result.iterates
        print("target:\n", self.desired_pose)
        fk = self._bot.fk(torch.tensor(joints[-1]).squeeze())
        print("actual:\n", fk)

        # Add collision objects
        if self.use_collision:
            self._env.draw_pybullet(client)

        # joints = joints * np.array([[[-1.], [1.], [1.], [1.], [1.], [1.], [1.]]])

        # self.world_trans = torch.eye(4, dtype=torch.double)
        # self.world_trans[0,0] *= -1
        # self.world_trans[1,1] *= -1
        draw_spheres = True  # self.use_collision
        # Put robot in initial joint configuration
        # for i in range(self._bot.num_joints):
        #     # multiply joints by -1 to get correct position
        #     client.resetJointState(robot_id, i, joints[0][i])
        self._bot.draw_pybullet(client, joints[0], reset=True, draw_points=draw_spheres)

        time.sleep(0.5)

        while client.readUserDebugParameter(close_btn_id) == 1:
            for thetas in joints:
                self._bot.draw_pybullet(client, thetas, reset=True, draw_points=draw_spheres)
                # for i in range(self._bot.num_joints):
                #     client.resetJointState(robot_id, i, thetas[i])
                #     # TODO tune this, can maybe set based on desired duration and length of thetas
                time.sleep(step)
                if pybullet.readUserDebugParameter(close_btn_id) > 1:
                    break

            # Only show EE frame at the end, slows down viz too much if in list loop
            pybullet_util.show_frame(self._bot.bot_id, self._bot.ee_index, client=client)
            print("final pose")
            time.sleep(5.0)

        client.disconnect()

    def _visualize_optimization_matplotlib(self, *args, **kwargs):
        raise NotImplementedError()


class IiwaIK(ChainIK):
    def __init__(
        self,
        desired_pose=None,
        error_type="pose",
        orientation_error_type="log",
        use_collision=False,
        collision_env=[],
        phuber_delta=1e-2,
        soft_limits=False,
    ):
        # ll4ma
        from ll4ma_opt.robots import IiwaKDL

        arm = IiwaKDL()

        if desired_pose is None:
            desired_pose = torch.DoubleTensor(
                [[1, 0, 0, 0.6], [0, -1, 0, 0.0], [0, 0, -1, 1.0], [0, 0, 0, 1]]
            )
        else:
            if not torch.is_tensor(desired_pose):
                desired_pose = torch.DoubleTensor(desired_pose)
            elif desired_pose.dtype != torch.double:
                desired_pose = desired_pose.to(torch.double)

        super().__init__(
            arm,
            desired_pose,
            error_type,
            orientation_error_type,
            use_collision,
            collision_env,
            phuber_delta=phuber_delta,
            soft_limits=False,
        )

    def _visualize_optimization_matplotlib(self, result, interval=50):
        return self._bot.visualize(result.iterates, result.sample_iterates, interval)

    def cost_gradient(self, x):
        """
        Gradient of the cost function.

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            gradient (array-like): Gradient of cost.
        """
        start = time.time()
        if not torch.is_tensor(x):
            x = torch.DoubleTensor(x)

        grad = self._bot.pose_grad(x.squeeze(), self.desired_pose)

        self.gradientTime += time.time() - start
        return grad.reshape(-1, 1).numpy()

    def residual(self, x):
        return self.tensor_error(self.make_tensor(x)).unsqueeze(1).numpy()

    def jacobian(self, x):
        xten = self.make_tensor(x)
        jac = self._bot.pose_error_grad(xten.squeeze(), self.desired_pose)
        return jac.numpy()
