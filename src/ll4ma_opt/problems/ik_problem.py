# Standard Library
import time

# Third Party
import numpy as np
import torch
from ll4ma_util import func_util, ui_util

# ll4ma
from ll4ma_opt.problems import Constraint, LeastSquaresProblem, GeneralizedLeastSquaresProblem
from ll4ma_opt.problems.resources import CollisionChecker, Panda, TwoLinkArm, pybullet_util, util

pybullet = func_util.silent_import("pybullet")
bullet_client = func_util.silent_import("pybullet_utils.bullet_client")

ERROR_TYPES = ["pose", "position", "phuber_position", "phuber_pose"]


class ChainIK(LeastSquaresProblem):
    def __init__(
        self,
        chain,
        desired_pose,
        error_type="pose",
        use_collision=False,
        collision_env=[],
    ):
        self.chain = chain
        self.desired_pose = desired_pose  # TF matrix
        super().__init__()

        if error_type not in ERROR_TYPES:
            raise ValueError("Error type '{}' not one of {}".format(error_type, ERROR_TYPES))
        self.initial_solution = np.array([[0.0]] * self.chain.num_joints)
        self.error_type = error_type

        self.use_collision = use_collision
        if use_collision:
            if pybullet is None:
                ui_util.print_error_exit(
                    "\n  Tried to use pybullet visualization, but pybullet is not installed."
                )
            self.collision_env = collision_env
            if not self.collision_env:
                self.collision_env = [(pybullet.GEOM_SPHERE, 0.1, [0.25, 0, 0.5])]
            self.collision_checker = CollisionChecker(self.chain, self.collision_env)

        jl = self.chain.joint_limits
        self._min_bounds = np.array([jl[i][0] for i in range(self.size())]).reshape(-1, 1)
        self._max_bounds = np.array([jl[i][1] for i in range(self.size())]).reshape(-1, 1)

        # self.phl = util.PseudoHuberLoss(phuber_delta)
        # if "phuber" in self.error_type:
        #     self.outer_convex_loss = self.phl.loss
        #     self.outer_convex_hessian = self.phl.hessian
        #     self.outer_convex_gradient = self.phl.gradient
        # HACK: Need to override this call to get the batch versions to work
        self.cost = self.tensor_cost

    def size(self):
        return self.chain.num_joints

    def set_weights(self, weights):
        self.chain.weights = torch.DoubleTensor(weights)

    def tensor_cost(self, x):
        error = self.tensor_error(x)
        cost = 0.5 * error.T @ error
        return cost

    def tensor_error(self, x):
        actual_pose = self.chain.fk(x)
        if "pose" in self.error_type:
            error = self.chain.pose_error(self.desired_pose, actual_pose)
        elif "position" in self.error_type:
            error = self.chain.ee_position_error(self.desired_pose, actual_pose, sum_squares=False)
        else:
            print("Unknown error type", self.error_type)
            return None

        return error

    def set_goal(self, goal_pose):
        """Update the goal to the given goal pose."""
        # TODO: Add assert on goal_pose type
        self.desired_pose = goal_pose  # TF matrix

    def visualize_optimization(self, result, *args, client="pybullet", **kwargs):
        if client == "pybullet":
            self._visualize_optimization_pybullet(result, *args, **kwargs)
        elif client == "matplotlib":
            return self._visualize_optimization_matplotlib(result, *args, **kwargs)
        else:
            raise ValueError(f"Unknown visualization client: {client}")

    def _visualize_optimization_pybullet(self, result, step=0.01):
        if pybullet is None:
            ui_util.print_error_exit(
                "\n  Tried to use pybullet visualization, but pybullet is not installed."
            )
        client = bullet_client.BulletClient(connection_mode=pybullet.GUI)
        close_btn_id = client.addUserDebugParameter("Close Window", 1, 0, 1)
        client.resetDebugVisualizerCamera(
            cameraDistance=4,
            cameraYaw=52,
            cameraPitch=-35,
            cameraTargetPosition=[0, 0, 0],
        )
        # Turn off pybullet cameras
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_RGB_BUFFER_PREVIEW, 0)
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_DEPTH_BUFFER_PREVIEW, 0)
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_SEGMENTATION_MARK_PREVIEW, 0)

        # Load robot URDF
        robot_id = pybullet_util.load_urdf(self.chain.urdf_path, client=client)

        # Turn off pybullet cameras
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_RGB_BUFFER_PREVIEW, 0)
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_DEPTH_BUFFER_PREVIEW, 0)
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_SEGMENTATION_MARK_PREVIEW, 0)

        # Show target frame to reach to
        desired_position = util.pos_from_homogeneous(self.desired_pose)
        desired_orientation = util.quat_from_homogeneous(self.desired_pose)
        # Need some object to attach frame to easily, using ducky
        pybullet_util.add_mesh(
            desired_position,
            desired_orientation,
            "duck.obj",
            [0, -0.05, 0],
            [0.1, 0.1, 0.1],
            client=client,
        )

        # Add collision objects
        if self.use_collision:
            self.collision_checker.add_collision_objects(self.collision_env, client)

        # Put robot in initial joint configuration
        for i in range(self.chain.num_joints):
            client.resetJointState(robot_id, i, result.iterates[0][i])

        time.sleep(0.5)

        while client.readUserDebugParameter(close_btn_id) == 1:
            for thetas in result.iterates:
                for i in range(self.chain.num_joints):
                    client.resetJointState(robot_id, i, thetas[i])
                    # TODO tune this, can maybe set based on desired duration and length of thetas
                    time.sleep(step)
                if pybullet.readUserDebugParameter(close_btn_id) > 1:
                    break

            # Only show EE frame at the end, slows down viz too much if in list loop
            pybullet_util.show_frame(robot_id, self.chain.ee_index, client=client)
            time.sleep(1.0)

        client.disconnect()

    def _visualize_optimization_matplotlib(self, *args, **kwargs):
        raise NotImplementedError()


class ChainIKPHuber(GeneralizedLeastSquaresProblem):
    # TODO: Implement the generalized version as its own class for easier use
    pass


class PandaIK(ChainIK):
    def __init__(
        self,
        desired_pose=None,
        error_type="pose",
        use_collision=False,
    ):
        arm = Panda()
        if desired_pose is None:
            desired_pose = torch.DoubleTensor(
                [[1, 0, 0, 0.6], [0, -1, 0, 0.0], [0, 0, -1, 0.5], [0, 0, 0, 1]]
            )

        super().__init__(
            arm,
            desired_pose,
            error_type,
            use_collision,
        )


class TwoLinkIK(ChainIK):
    def __init__(
        self,
        desired_pose=None,
        error_type="pose",
        use_collision=False,
    ):
        arm = TwoLinkArm()
        if desired_pose is None:
            desired_pose = torch.DoubleTensor(
                [[0, 0, 1, 1.5], [1, 0, 0, 0.0], [0, 1, 0, 0.5], [0, 0, 0, 1]]
            )

        super().__init__(
            arm,
            desired_pose,
            error_type,
            use_collision,
        )
        self.inequality_constraints.append(Constraint(lambda x: -(torch.sum(x) - 3)))
        self.inequality_constraints.append(Constraint(lambda x: -(6 - torch.sum(x))))

    def _visualize_optimization_matplotlib(self, result, interval=50):
        return self.chain.visualize(
            result.iterates, result.sample_iterates, interval, desired_pose=self.desired_pose
        )
