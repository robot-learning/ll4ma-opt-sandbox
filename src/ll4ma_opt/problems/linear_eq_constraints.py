#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: thermans
"""
# Third Party
# import matplotlib.pyplot as plt
import numpy as np

# ll4ma
from ll4ma_opt.problems import Problem


class TuckersQP(Problem):
    def __init__(self):
        super().__init__()

        # Constraint of the form Ax = b
        # x0 + x1 = 2
        # x2 - 2 * x3 = 3
        self.linear_equality_matrix = np.array([[1.0, 1.0, 0, 0], [0, 0.0, 1.0, -2.0]])
        self.linear_equality_vector = np.array([[2.0], [3.0]])
        self.Q = np.eye(4) * 2
        self.q = np.array([[1.0, 3.0, 3.0, 2.0]]).T
        self.initial_solution = np.array([[1.0, 1.0, 1.0, -1]]).T

    def cost(self, x):
        return x.T @ self.Q @ x + self.q.T @ x

    def cost_gradient(self, x):
        return 2.0 * self.Q @ x + self.q

    def cost_hessian(self, x):
        return 2.0 * self.Q

    def size(self):
        return 4


class EasyQP(Problem):
    def __init__(self):
        super().__init__()

        # Constraint of the form Ax = b
        # x0 + x1 = 2
        # x2 - 2 * x3 = 3
        self.linear_equality_matrix = np.array([[1.0, 1.0]])

        self.linear_equality_vector = np.array([[2.0]])

        self.Q = np.eye(2) * 2
        self.q = np.array([[1.0, -3.0]]).T
        self.initial_solution = np.array([[1.0, 1.0]]).T

    def cost(self, x):
        return x.T @ self.Q @ x - self.q.T @ x

    def cost_gradient(self, x):
        return 0.5 * self.Q @ x - self.q

    def cost_hessian(self, x):
        return 0.5 * self.Q

    def size(self):
        return 2
