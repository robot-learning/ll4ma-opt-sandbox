#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 14:08:01 2021

@author: tabor
"""


# Third Party
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import numpy as np
import torch
from matplotlib import animation

# ll4ma
from ll4ma_opt.problems import Constraint, LeastSquaresProblem


def simpleAnalyticSim(state, controls):
    states = []
    if isinstance(state, torch.Tensor):
        states.append(state.clone())
        state = state.clone()
    else:
        states.append(state.copy())
        state = state.copy()
    for i in range(controls.shape[1]):
        state[:3] = state[:3] + state[3:6] + controls[:, i] / 2
        state[3:6] = state[3:6] + controls[:, i]
        if isinstance(state, torch.Tensor):
            states.append(state.clone())
        else:
            states.append(state.copy())
    return states


# this signed distance constraint assumes problem has a get_positions
# function, if this could be cleverly implemented that would be nice


class SDConstraint(Constraint):
    def __init__(self, obstacle, timestep, problem, radius=0.1, make_tensor=torch.DoubleTensor):
        super().__init__(make_tensor=make_tensor)
        self.timestep = timestep
        self.obstacle = torch.tensor(obstacle)
        self.start = torch.tensor(problem.start)
        self.radius = radius
        self.problem = problem

    def tensor_error(self, x):
        error = (
            self.problem.get_positions(self.start, x)[self.timestep][:3] - self.obstacle
        ).reshape((3, 1))
        dist = torch.sqrt(error.T @ error).squeeze()
        return -(dist - self.radius)


class PointMassTraj(LeastSquaresProblem):
    def __init__(self, timesteps, obstacles, obstacle_radius=0.1):
        super().__init__()

        self.obstacles = np.array([[0.5], [0], [0]])
        self.obstacles = np.random.uniform(0.25, 0.75, (3, obstacles))
        self.start = np.zeros((6))
        self.goal = np.zeros((6))
        self.goal[:3] = 1
        self.inequality_constraints = [
            SDConstraint(self.obstacles[:, j], i, self, obstacle_radius)
            for i in range(timesteps)
            for j in range(self.obstacles.shape[1])
        ]
        self.obstacle_radius = obstacle_radius
        self.tensor_start = torch.tensor(self.start)
        self.tensor_goal = torch.tensor(self.goal)

        self.cached_hessian = None
        self.cached_jacobian = None

    def jacobian(self, x):
        if self.cached_jacobian is None:
            self.cached_jacobian = super().jacobian(x)
        return self.cached_jacobian

    def cost_hessian(self, x):
        if self.cached_hessian is None:
            self.cached_hessian = super().cost_hessian(x)
        return self.cached_hessian

    def get_accelerations(self, start, x):
        raise NotImplementedError()

    def size(self):
        return self.initial_solution.shape[0]

    def get_positions(self, start, x):
        raise NotImplementedError()

    def tensor_error(self, x):
        positions = self.get_positions(self.tensor_start, x)
        accelerations = self.get_accelerations(self.tensor_start, x)
        error = positions[-1][:3] - self.tensor_goal[:3]

        residual = torch.cat((error * 1e5, accelerations.flatten())).reshape((-1, 1))
        return residual

    def tensor_cost(self, x):
        positions = self.get_positions(self.tensor_start, x)
        accelerations = self.get_accelerations(self.tensor_start, x)
        error = (positions[-1][:3] - self.tensor_goal[:3]).reshape((3, 1))

        acceleration_cost = 1 / 2 * torch.sum(accelerations**2)
        final_position_cost = torch.sum(error.T @ error)

        cost = final_position_cost * 1e10 + acceleration_cost
        self.accelerations = accelerations
        return cost

    def visualize_optimization(self, result):
        fig = plt.figure()
        ax = p3.Axes3D(fig)
        ax.set_xlim(-0.25, 1)
        ax.set_ylim(-0.25, 1)
        ax.set_zlim(-0.25, 1)
        positions = np.array(self.get_positions(self.start, result.iterates[0]))
        traj_plot = ax.plot(
            positions[:, 0],
            positions[:, 1],
            positions[:, 2],
            linewidth=2,
            color="k",
            marker="o",
        )[0]

        for i in range(self.obstacles.shape[1]):
            self.draw_sphere(self.obstacles[:, i], self.obstacle_radius, ax)

        def updateTraj(num, traj_plot, iterates):
            positions = np.array(self.get_positions(self.start, iterates[num]))
            traj_plot.set_data(np.transpose(positions[:, 0:2]))
            traj_plot.set_3d_properties(positions[:, 2])

        anim = animation.FuncAnimation(
            fig,
            updateTraj,
            result.iterates.shape[0],
            fargs=(traj_plot, result.iterates),
            interval=1,
            blit=False,
        )
        ax.view_init(elev=6, azim=-60)
        plt.show()
        return anim

    def draw_sphere(self, center, radius, ax):
        # draw sphere
        u, v = np.mgrid[0 : 2 * np.pi : 20j, 0 : np.pi : 10j]
        x = np.cos(u) * np.sin(v) * radius + center[0]
        y = np.sin(u) * np.sin(v) * radius + center[1]
        z = np.cos(v) * radius + center[2]
        ax.plot_surface(x, y, z, linewidth=0.0, cstride=1, rstride=1, color=(0, 1, 0, 0.5))


class PointMassAccelerationFormulation(PointMassTraj):
    def __init__(self, timesteps=15, obstacles=50, obstacle_radius=0.1):
        self.initial_solution = np.ones((3 * timesteps, 1)) * 0.01
        super().__init__(timesteps, obstacles, obstacle_radius)

    # the constraints need to call this a lot, would be good if this was smart and cached somehow
    def get_positions(self, start, x):
        positions = simpleAnalyticSim(start, self.get_accelerations(start, x))
        return positions

    def get_accelerations(self, start, x):
        return x.reshape((3, -1))


class PointMassPositionFormulation(PointMassTraj):
    def __init__(self, timesteps=15, obstacles=50, obstacle_radius=0.1):
        self.initial_solution = np.ones((3 * (timesteps + 1), 1)) * 0.01
        super().__init__(timesteps, obstacles, obstacle_radius)

    def get_positions(self, start, x):
        return x.reshape((-1, 3))

    def get_accelerations(self, start, x):
        positions = self.get_positions(start, x)

        accel_list = []
        for i in range(len(positions) - 1):
            if i == 0:
                accel = (positions[0] - start[:3]) * 2  # start had 0 velocity
            else:
                accel = positions[i + 1] - 2 * positions[i] + positions[i - 1]
            accel_list.append(accel)
        accels = torch.stack(accel_list, 1)
        return accels
