#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 20:31:48 2020

@author: tabor
"""
# Standard Library
import time

# Third Party
import numpy as np
import torch
from torch.autograd import Variable

# ll4ma
from ll4ma_opt.problems.resources.finite_differences import approximate_hessian, fd


def unmake_tensor(x):
    return x.detach().cpu().numpy()


class Problem:
    def __init__(self):
        self.inequality_constraints = []
        self.equality_constraints = []

        # For linear equality constraints of the form Ax = b
        self.linear_equality_matrix = None  # A matrix
        self.linear_equality_vector = None  # b vector

        self._min_bounds = np.full((self.size(), 1), -np.inf)
        self._max_bounds = np.full((self.size(), 1), np.inf)

        self.costTime = 0
        self.gradientTime = 0
        self.hessianTime = 0

        self.make_tensor = torch.DoubleTensor
        self.unmake_tensor = unmake_tensor

    def min_bounds():
        doc = "The min_bounds property."

        def fget(self):
            return self._min_bounds

        def fset(self, value):
            self._min_bounds = value

        def fdel(self):
            del self._min_bounds

        return locals()

    min_bounds = property(**min_bounds())

    def max_bounds():
        doc = "The max_bounds property."

        def fget(self):
            return self._max_bounds

        def fset(self, value):
            self._max_bounds = value

        def fdel(self):
            del self._max_bounds

        return locals()

    max_bounds = property(**max_bounds())

    def cost(self, x):
        """
        Cost function for the problem.

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            cost (flt): Computed cost.
        """
        start = time.time()
        try:
            x_tensor = Variable(self.make_tensor(x), requires_grad=True)
            cost = self.tensor_cost(x_tensor)
            self.costTime += time.time() - start
            return self.unmake_tensor(cost).flatten()[0]
        except NotImplementedError:
            raise NotImplementedError(
                "Subclasses must override 'cost' function, or define "
                "a pytorch 'tensor_cost' function that base class can use."
            )

    def batch_cost(self, x):
        """
        Default implementation of batch cost that calls tensor_batch_cost.
        Override this if you have a better way to compute cost in batch.
        """
        return self.unmake_tensor(self.tensor_batch_cost(self.make_tensor(x)))

    def cost_gradient(self, x):
        """
        Gradient of the cost function.

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            gradient (array-like): Gradient of cost.
        """
        start = time.time()
        try:
            x_tensor = Variable(self.make_tensor(x), requires_grad=True)
            grad = self.unmake_tensor(
                torch.autograd.grad([self.tensor_cost(x_tensor)], x_tensor, allow_unused=True)[0]
            )
        except NotImplementedError:
            grad = fd(self.cost, x)
        self.gradientTime += time.time() - start
        return grad

    def batch_gradient(self, x):
        """
        Default implementation of batch gradient that calls tensor_batch_cost.
        Override this if you have a better way to compute gradient in batch.
        """
        x_tensor = Variable(self.make_tensor(x), requires_grad=True)
        grad = self.unmake_tensor(
            torch.autograd.grad(
                [torch.sum(self.tensor_batch_cost(x_tensor))], x_tensor, allow_unused=True
            )[0]
        )
        return grad

    def cost_hessian(self, x):
        """
        Hessian of the cost function.

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            gradient (array-like): Gradient of cost.
        """
        start = time.time()
        try:
            x_tensor = Variable(self.make_tensor(x), requires_grad=True)
            H = self.unmake_tensor(
                torch.autograd.functional.hessian(self.tensor_cost, x_tensor, vectorize=True)[
                    :, 0, :, 0
                ]
            )
        except NotImplementedError:
            H = approximate_hessian(self.cost, x)
        self.hessianTime += time.time() - start
        return H

    def tensor_cost(self, x):
        """
        Cost function operating on Tensors. If this is defined, you get the
        gradient and and hessian for free using autograd.
        """
        raise NotImplementedError("Tensor cost function is not implemented.")

    def tensor_batch_cost(self, x):
        """
        Cost function operating on array of tensors. To be used in batch_cost
        and batch_gradient. By default it calls tensor_cost sequentially, override
        if you have faster way to implement the cost function.
        """
        return torch.stack([self.tensor_cost(point) for point in x])

    def visualize_optimization(self, iterates, *args, **kwargs):
        """
        Creates visualization of the optimization.

        Args:
            iterates (list-like): Solutions from each iteration of the optimization.

            Additional positional and keyword args accepted from *args and **kwargs respectively.
        """
        raise NotImplementedError("Subclasses must override to create optimization visualization.")

    def size(self):
        """
        Returns the size of the problem decision variable.
        """
        raise NotImplementedError()

    def clamp(self, x):
        """
        Clamps decision variable to be within limits of bound constraints.

        Args:
            x (Tensor): Decision var of shape (batch, decision_var_size)

        TODO this currently only works for batched torch tensors, if you want to use
        this for numpy column vectors you need to check types/shapes and modify accordingly
        """
        minb = (
            self.min_bounds if torch.is_tensor(self.min_bounds) else torch.tensor(self.min_bounds)
        )
        maxb = (
            self.max_bounds if torch.is_tensor(self.max_bounds) else torch.tensor(self.max_bounds)
        )
        return torch.clamp(x, minb.T, maxb.T)


# optionally define by passing lambda or overwriting tensor_error directly
class Constraint:
    def __init__(self, constraintFunc=None, make_tensor=torch.DoubleTensor):
        if constraintFunc:
            self.tensor_error = constraintFunc

        self.make_tensor = make_tensor
        self.unmake_tensor = unmake_tensor

    def tensor_error(self, x):
        """
        Constraint function operating on Tensors.
        """
        # assumes <0 is fine, >0 is violating constraint
        raise NotImplementedError("Tensor error function is not implemented.")

    def error(self, x):
        """
        Error function for the contraint.

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            cost (flt): Computed cost.
        """
        try:
            is_tensor = torch.is_tensor(x)
            if is_tensor:
                x_tensor = x.clone()
            else:
                x_tensor = self.make_tensor(x)
            cost = self.tensor_error(x_tensor)
            if not is_tensor:
                cost = self.unmake_tensor(cost)
            return cost
        except NotImplementedError:
            raise NotImplementedError(
                "Subclasses must override 'cost' function, or define "
                "a pytorch 'tensor_cost' function that base class can use."
            )

    def error_gradient(self, x):
        """
        Gradient of the error function.

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            gradient (array-like): Gradient of cost.
        """
        try:
            is_tensor = torch.is_tensor(x)
            if is_tensor:
                x_tensor = x.clone()
            else:
                x_tensor = self.make_tensor(x)
            x_tensor.requires_grad = True
            grad = torch.autograd.grad([self.tensor_error(x_tensor)], x_tensor, allow_unused=True)[
                0
            ].detach()
            if not is_tensor:
                grad = self.unmake_tensor(grad)
        except NotImplementedError:
            grad = fd(self.cost, x)
        return grad

    def error_hessian(self, x):
        """
        Hessian of the cost function.

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            gradient (array-like): Gradient of cost.
        """
        try:
            x_tensor = Variable(self.make_tensor(x), requires_grad=True)
            H = self.unmake_tensor(
                torch.autograd.functional.hessian(self.tensor_error, x_tensor, vectorize=True)[
                    :, 0, :, 0
                ]
            )
        except NotImplementedError:
            H = approximate_hessian(self.error, x)
        return H


class TensorConstraint(Constraint):
    """
    Version of the Constraint class that batches a bunch of constraints together
    for speedy calculations
    """

    def __call__(self, x):
        return self.error(x)

    def error(self, x):
        raise NotImplementedError("TensorConstraint error not implemented")

    def error_gradient(self, x):
        raise NotImplementedError("TensorConstraint error_gradient not implemented")

    def error_hessian(self, x):
        raise NotImplementedError("TensorConstraint error_hessian not implemented")


class LeastSquaresProblem(Problem):
    """
    Problem class specialized for least squares problems.
    Problems take the form of f(x) = 1/2 || r(x) ||^2 where r(x) defines the residual vector.
    """

    def __init__(self):
        super().__init__()
        # Override parent method by assignment to allow for directly calling superclass functions
        self.cost = self.gn_cost
        self.cost_gradient = self.gn_gradient

    def gn_cost(self, x):
        """
        Cost function for the problem.

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            cost (flt): Computed cost.
        """
        y = self.residual(x)
        return 0.5 * y.T @ y

    def residual(self, x):
        """
        Compute the residual associated with the nonlinear function
        """
        try:
            x_tensor = Variable(self.make_tensor(x), requires_grad=True)
            error = self.tensor_error(x_tensor)
            residual = self.unmake_tensor(error)
            return residual
        except NotImplementedError:
            raise NotImplementedError(
                "Must override 'residual' function or implement 'tensor_error' function."
            )

    def jacobian(self, x):
        """
        Compute the Jacobian of the inner nonlinear function
        """
        try:
            x_tensor = Variable(self.make_tensor(x), requires_grad=True)
            J = self.unmake_tensor(
                torch.autograd.functional.jacobian(self.tensor_error, x_tensor, vectorize=True)[
                    :, 0, :, 0
                ]
            )
            return J
        except NotImplementedError:
            raise NotImplementedError(
                "Must override 'jacobian' function or implement "
                "'tensor_error' to benefit from autograd."
            )

    def gn_gradient(self, x):
        """
        Gradient of the cost function del f(x) = J^T @ R(x)

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            gradient (array-like): Gradient of cost.
        """
        r = self.residual(x)
        J = self.jacobian(x)
        return J.T @ r

    def tensor_error(self, x):
        """
        Error function operating on Tensors. If this is defined, you can run this
        problem with residual methods and compute Jacobian for free.
        """
        raise NotImplementedError("Tensor error function is not implemented.")


class GeneralizedLeastSquaresProblem(LeastSquaresProblem):
    """
    Class to store a convex-over nonlinear optimization.
    This can be thought of as a generalization of a nonlinear least squares problem.
    """

    def __init__(self):
        super().__init__()
        # Override parent method by assignment to allow for directly calling superclass functions
        self.cost = self.ggn_cost
        self.cost_gradient = self.ggn_gradient

    def ggn_cost(self, x):
        """
        Cost function for the problem.

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            cost (flt): Computed cost.
        """
        return self.outer_convex_loss(self.residual(x))

    def ggn_gradient(self, x):
        """
        Gradient of the cost function del f(x) = J^T @ R(x)

        Args:
            x (column vector): Decision variable of the problem.
        Returns:
            gradient (array-like): Gradient of cost.
        """
        r = self.residual(x)
        v = self.outer_convex_gradient(r)
        J = self.jacobian(x)
        return J.T @ v

    def outer_convex_loss(self, y):
        """
        The outer convex cost being computed over an arbitrary smooth nonlinear function
        """
        raise NotImplementedError("Must provide an outer convex loss function for use in GGN")

    def outer_convex_gradient(self, y):
        """
        The gradient of the outer convex being computed over an arbitrary smooth nonlinear function
        """
        raise NotImplementedError("Must provide an outer convex gradient function for use in GGN")

    def outer_convex_hessian(self, y):
        """
        Hessian associated with the outer convex cost
        """
        raise NotImplementedError("Must provide an outer convex hessian function for use in GGN")
