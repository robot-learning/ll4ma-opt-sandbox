# Third Party
import numpy as np

# ll4ma
from ll4ma_opt.problems import Constraint, Problem
from ll4ma_opt.solvers import SolverReturn


class InequalityToEqualityWrapper(Problem):
    """
    Construct a problem that's bound and equality constrained from a problem that originally also
    includes inequality constraints.

    Converts each inequality constraint, g(x) <= 0 to an equality constraint
    g(x)  = s with s <= 0 for a new slack variable s.

    Currently only setup for tensor-based costs, need to setup associated gradient calls
    to make things work more generally
    """

    def __init__(self, problem):
        self.original_size = int(problem.size())
        self.size = lambda: self.original_size + len(problem.inequality_constraints)

        # Initialize with provided init solution and epsilon for slack variables
        self.initial_solution = np.ones((self.size(), 1)) * 0.001
        self.initial_solution[: self.original_size] = problem.initial_solution[:]

        self.original_problem = problem
        # copy data over before init
        super().__init__()

        # Copy over original problem bounds
        self.min_bounds[: self.original_size] = problem.min_bounds[:]
        self.max_bounds[: self.original_size] = problem.max_bounds[:]
        # Append upper 0 bound for slack variables
        self.max_bounds[self.original_size :] = 0.0

        # Copy previous equality constraints
        self.equality_constraints = [
            Constraint(lambda x: eq.tensor_error(x[: self.original_size]))
            for eq in problem.equality_constraints
        ]
        # Convert inequality constraints to equality_constraints
        for i, ineq in enumerate(problem.inequality_constraints):
            slack_idx = self.original_size + i
            self.equality_constraints.append(
                Constraint(lambda x: ineq.tensor_error(x[: self.original_size] - x[slack_idx]))
            )

    def tensor_cost(self, x):
        """
        Compute the cost of the original problem

        x - decision vector with slack variables
        """
        return self.original_problem.tensor_cost(x[: self.original_size])

    def convert_solver_result(self, result):
        """
        Convert the solver's return to not include the slack variables
        so that you can use the original problem's viz, cost, etc
        """
        non_slack_result = SolverReturn()
        non_slack_result.solution = result.solution[: self.original_size]
        non_slack_result.cost = self.original_problem.cost(non_slack_result.solution)
        non_slack_result.iterates = result.iterates[:, : self.original_size]
        non_slack_result.time = result.time
        non_slack_result.converged = result.converged
        non_slack_result.convergence_criteria = result.convergence_criteria
        return non_slack_result

    def visualize_optimization(self, result):
        """
        Not sure this adds much convenience, but I'll leave it
        """
        # Only pass non slack variable info
        new_result = self.convert_solver_result(result)
        self.original_problem.visualize_optimization(new_result)
