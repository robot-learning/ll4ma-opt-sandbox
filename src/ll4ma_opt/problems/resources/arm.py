# Standard Library
import os

# Third Party
import matplotlib.pyplot as plt
import numpy as np
import torch
from ll4ma_util import func_util
from matplotlib import animation

# ll4ma
from ll4ma_opt.problems.resources import util

pybullet_data = func_util.silent_import("pybullet_data")
BULLET_DATA_PATH = pybullet_data.getDataPath() if pybullet_data is not None else ""


class Chain:
    """
    Base class implementing functionality of a kinematic chain (e.g. robot arm or finger)
    with differentiable forward kinematics.
    """

    def __init__(self, alphas, Ds, As, urdf_path, ee_index=-1, modified_DH=False):
        """
        Args:
            alphas: List of 'alpha' DH parameters.
            Ds: List of 'd' DH parameters.
            As: List of 'a' DH parameters.

        Note: Assuming all revolute joints, so Ds are always fixed and thetas always variable.
        """
        self.alphas = torch.DoubleTensor(alphas).view(-1, 1)
        self.Ds = torch.DoubleTensor(Ds).view(-1, 1)
        self.As = torch.DoubleTensor(As).view(-1, 1)
        self.num_joints = len(alphas)
        self.urdf_path = urdf_path
        self.ee_index = ee_index
        self.modified_DH = modified_DH
        self.weights = torch.eye(3, dtype=torch.double)

    def fk(self, thetas):
        """
        Computes forward kinematics.

        Args:
            thetas (Tensor): Joint angles
        Returns:
            T: Homogeneous TF matrix representing end-effector 3D pose.
        """
        if not isinstance(thetas, torch.Tensor):
            thetas = torch.tensor(thetas)

        return self.fk_links(thetas)[-1]

    def fk_links(self, thetas):
        """
        Computes forward kinematics for all links.

        Args:
            thetas (Tensor): Joint angles
        Returns:
            Ts (list): List of homogeneous TF matrices, one for each link in kinematic chain.
        """
        Ts = []
        T = torch.eye(4, dtype=torch.double)
        for i in range(len(thetas)):
            T_i = self._compute_T(self.alphas[i], thetas[i], self.Ds[i], self.As[i])
            T = torch.mm(T, T_i)
            Ts.append(T)
        return Ts

    def get_link_positions(self, thetas):
        """
        Computes the position of each link in Cartesian space w.r.t. to robot base.
        """
        if not isinstance(thetas, torch.Tensor):
            thetas = torch.tensor(thetas)
        return [util.pos_from_homogeneous(T) for T in self.fk_links(thetas)]

    def pose_error(self, T_desired, T_actual):
        position_error = self.ee_position_error(T_desired, T_actual, False)
        orientation_error = self.ee_orientation_error(T_desired, T_actual)
        error = torch.cat((position_error, orientation_error))
        return error

    def ee_position_error(self, T_desired, T_actual, sum_squares=True):
        """
        Computes sum of squares error between position components of two homogeneous TF matrices.
        """
        p_desired = util.pos_from_homogeneous(T_desired)
        p_actual = util.pos_from_homogeneous(T_actual)
        error = p_desired - p_actual
        error = torch.mm(self.weights, error.view(-1, 1))
        if sum_squares:
            error = torch.sum(error**2)
        return error

    def ee_orientation_error(self, T_desired, T_actual):
        """
        Computes orientation error as using matrix log between the desired
        and actual rotation matrices.
        Could easily extend to handle quaternions if desired.
        """
        R_desired = util.rot_from_homogeneous(T_desired)
        R_actual = util.rot_from_homogeneous(T_actual)
        R_diff = R_actual.T @ R_desired
        _, theta = util.rotation_matrix_log(R_diff)
        error = theta**2

        return error.reshape((1, 1))

    def _compute_T(self, alpha, theta, d, a):
        """
        Computes homogeneous transformation matrix.

        If self.modified_DH=True, uses modified DH convention:
        https://en.wikipedia.org/wiki/Denavit%E2%80%93Hartenberg_parameters#Modified_DH_parameters
        """
        ct = torch.cos(theta).view(1)
        st = torch.sin(theta).view(1)
        ca = torch.cos(alpha).view(1)
        sa = torch.sin(alpha).view(1)
        # These are a little silly, but necessary for autograd not to yell at you
        one = torch.DoubleTensor([1.0])
        zero = torch.DoubleTensor([0.0])

        if self.modified_DH:
            T = torch.stack(
                [
                    torch.stack([ct, -st, zero, a]),
                    torch.stack([st * ca, ct * ca, -sa, -d * sa]),
                    torch.stack([st * sa, ct * sa, ca, d * ca]),
                    torch.stack([zero, zero, zero, one]),
                ]
            )
        else:
            T = torch.stack(
                [
                    torch.stack([ct, -st * ca, st * sa, a * ct]),
                    torch.stack([st, ct * ca, -ct * sa, a * st]),
                    torch.stack([zero, sa, ca, d]),
                    torch.stack([zero, zero, zero, one]),
                ]
            )

        return T.squeeze()


class Panda(Chain):
    def __init__(self, urdf_path=os.path.join(BULLET_DATA_PATH, "franka_panda/panda.urdf")):
        """
        DH parameters from:
          https://frankaemika.github.io/docs/control_parameters.html#denavithartenberg-parameters

        Note: These are in the Modified DH convention, the FK function accounts for this.
        """
        alphas = [0, -np.pi / 2, np.pi / 2, np.pi / 2, -np.pi / 2, np.pi / 2, np.pi / 2]
        Ds = [0.333, 0, 0.316, 0, 0.384, 0, 0]
        As = [0, 0, 0, 0.0825, -0.0825, 0, 0.088]
        ee_index = 6  # TODO right now this is flange, need to make hand once transform is computed
        self.joint_limits = {
            0: (-2.8973, 2.8973),
            1: (-1.7628, 1.7628),
            2: (-2.8973, 2.8973),
            3: (-3.0718, -0.0698),
            4: (-2.8973, 2.8973),
            5: (-0.0175, 3.7525),
            6: (-2.8973, 2.8973),
        }
        super().__init__(alphas, Ds, As, urdf_path, ee_index, True)

    def fk(self, thetas):
        T = super().fk(thetas)
        # Adding one more fixed transform for the flange
        alpha = torch.DoubleTensor([0.0])
        theta = torch.DoubleTensor([0.0])
        d = torch.DoubleTensor([0.107])
        a = torch.DoubleTensor([0.0])
        T = torch.mm(T, self._compute_T(alpha, theta, d, a))
        # TODO there's one more transform to add from flange to hand, need to manually find it
        return T


class TwoLinkArm(Chain):
    def __init__(
        self,
        alphas=[0, 0],
        Ds=[0, 0],
        As=[1, 1],
        urdf_path=os.path.join(os.path.dirname(__file__), "two_link_arm.urdf"),
    ):
        # TODO need to add ee_index for display once you add a "hand" link
        self.joint_limits = {0: (-np.pi, np.pi), 1: (-np.pi, np.pi)}
        super().__init__(alphas, Ds, As, urdf_path)

    def visualize(self, thetas, samples=None, interval=50, ws_buffer=1, desired_pose=None):
        fig, ax = plt.subplots(1, 1)
        fig.set_size_inches(8, 8)
        arm_length = torch.sum(self.As).item()
        ws = arm_length + ws_buffer
        ax.set_xlim(-ws, ws)
        ax.set_ylim(-ws, ws)

        arm = ax.plot([], [], "o-", lw=3, color="green")[0]

        arm_samples = []
        if samples is not None:
            # Assuming shape (iters, samples, n_dims)
            for j in range(samples.shape[1]):
                (sample_line,) = ax.plot([], [], lw=1, color="blue", alpha=0.2)
                arm_samples.append(sample_line)

        if desired_pose is not None:
            x = desired_pose[0, -1]
            y = desired_pose[2, -1]  # I think it uses z so it looks okay in pybullet also?
            plt.scatter([x], [y], s=150, color="r")

        def animate(i):
            xs, ys = self._get_draw_coordinates(thetas[i])
            arm.set_data(xs, ys)

            if samples is not None:
                for j in range(samples.shape[1]):
                    xs, ys = self._get_draw_coordinates(samples[i, j, :])
                    arm_samples[j].set_data(xs, ys)

            fig.canvas.draw()
            return arm_samples + [arm]

        anim = animation.FuncAnimation(fig, animate, frames=thetas.shape[0], interval=interval)
        plt.tight_layout()
        plt.show()
        return anim

    def _get_draw_coordinates(self, thetas):
        """
        Helper function for creating the (x,y) points to be drawn with lines.
        """
        points = self.get_link_positions(thetas)
        xys = [p.cpu().numpy().tolist()[:2] for p in points]
        xs, ys = zip(*xys)
        return [0] + list(xs), [0] + list(ys)

    def fk(self, thetas):
        # Adding a fixed transform at beginning to align axis with pybullet
        alpha = torch.DoubleTensor([np.pi / 2])
        theta = torch.DoubleTensor([0.0])
        d = torch.DoubleTensor([0.0])
        a = torch.DoubleTensor([0.0])
        T = self._compute_T(alpha, theta, d, a)

        T = torch.mm(T, super().fk(thetas))
        return T


if __name__ == "__main__":
    arm = TwoLinkArm()
    joints = np.stack([np.linspace(0, np.pi / 2, 100) for _ in range(2)]).T
    arm.visualize(joints)
