# Third Party
import numpy as np


def fd(f, x, delta=1.0e-5, *args, **kwargs):
    """
    Compute derivative via central differencing
    """
    x_len = x.shape[0]
    x_vector = np.zeros((x_len,))  # change x to vector
    f_gradient = np.array(x_vector).T  # init the gradient
    for i in range(x_len):
        x_vector[i] = x[i, 0]
    for i in range(x_len):
        e_i = np.zeros((x_len,))
        e_i[i] = 1  # get the unit vector
        q_plus = np.array(x_vector + e_i * delta / 2).T
        q_minus = np.array(x_vector - e_i * delta / 2).T

        # HACK: Should be fixed more cleanly on construction
        if len(x.shape) > 1:
            q_plus = np.reshape(q_plus, x.shape)
            q_minus = np.reshape(q_minus, x.shape)

        c_plus = f(q_plus, *args, **kwargs)
        c_minus = f(q_minus, *args, **kwargs)
        f_gradient[i] = (c_plus - c_minus) / delta

    return f_gradient


def approximate_hessian(f, x, delta=1.0e-5, *args, **kwargs):
    """
    Compute hessian
    """
    x_len = x.shape[0]
    f_hessian = np.zeros((x_len, x_len))  # init the hessian
    x_vector = np.zeros((x_len,))
    for i in range(x_len):
        x_vector[i] = x[i, 0]
    for i in range(x_len):
        for j in range(x_len):
            e_i = np.zeros((x_len,))
            e_j = np.zeros((x_len,))
            e_i[i] = 1  # get the unit vector
            e_j[j] = 1  # get the unit vector

            x_i_j = np.array(x_vector + e_i * delta + e_j * delta).T
            x_i = np.array(x_vector + e_i * delta).T
            x_j = np.array(x_vector + e_j * delta).T

            # HACK: Should be fixed more cleanly on construction
            if len(x.shape) > 1:
                x_i_j = np.reshape(x_i_j, x.shape)
                x_i = np.reshape(x_i, x.shape)
                x_j = np.reshape(x_j, x.shape)

            f_i_j_shift = f(x_i_j, *args, **kwargs)
            f_i_shift = f(x_i, *args, **kwargs)
            f_j_shift = f(x_j, *args, **kwargs)
            f_x = f(x, *args, **kwargs)

            f_hessian[i][j] = (f_i_j_shift - f_i_shift - f_j_shift + f_x) / (delta * delta)

    return f_hessian


def complex_fd(f, x, delta):
    raise NotImplementedError("Not yet implemented")
