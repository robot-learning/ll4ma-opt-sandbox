# Third Party
import torch

# ll4ma
from ll4ma_opt.problems import Constraint

# def my_chain_fk(x, chain):
#     chain.fk(x[-1])
#     #... conversion
#   x -> 6x1 vector
#
# @gradient
# def my_chain_fk_gradient(x, chain):
#     return chain.jacobian(x[-1])
#     #... conversion
#   x -> 6x1 vector


def pose_error(x, desired_pose, chain, error_type, orientation_error_type, phl=None):
    x = x.reshape(-1, chain.num_joints)
    if error_type == "pose":
        error = chain.pose_error(desired_pose, chain.fk(x[-1]), orientation_error_type)
    elif error_type == "position":
        error = chain.ee_position_error(desired_pose, x[-1], sum_squares=False)
    error = (error.T @ error).squeeze()
    return error


def pose_grad(x, desired_pose, chain, error_type, orientation_error_type, phl=None):
    x = x.reshape(-1, chain.num_joints)
    grad = torch.zeros_like(x)
    if error_type == "pose":
        # pose_error outputs a (4,) vector
        perr = chain.pose_error(desired_pose, chain.fk(x[-1]), orientation_error_type).squeeze()
        # pose_grad outputs a (4,6) matrix
        pgrad = chain.pose_grad(desired_pose, chain.fk(x[-1]), orientation_error_type)
        # jacobian outputs a (6,7) matrix
        jac = chain.jacobian(x[-1])
        # combining outputs a (4,7) matrix
        pgrad = pgrad @ jac
        # combining outputs a (7,) vector
        grad[-1] = 2 * perr @ pgrad
    elif error_type == "position":
        grad = chain.ee_position_grad(desired_pose, x[-1], sum_squares=False)
    # error = (error.T @ error).squeeze()
    return grad


class PoseConstraint(Constraint):
    """docstring for PoseConstraint."""

    def __init__(
        self,
        chain,
        desired_pose,
        error_type="pose",
        orientation_error_type="log",
    ):
        super(PoseConstraint, self).__init__()
        self.chain = chain
        self.desired_pose = desired_pose
        self.error_type = error_type
        self.orientation_error_type = orientation_error_type

    def error(self, x):
        return self.tensor_error(x if torch.is_tensor(x) else torch.tensor(x)).item()

    def tensor_error(self, x):
        x = x.reshape(-1, self.chain.num_joints)
        fk = self._bot.fk(x[-1])

        pos = fk[:3, 3]
        des_pos = self.desired_pose[:3, 3]
        perr = self._bot.ee_position_error(des_pos, pos, sum_squares=False)
        cost = (perr.T @ perr).squeeze()

        ori = fk[:3, :3]
        des_r = self.desired_pose[:3, :3]
        oerr = 0.5 * self._bot.ee_orientation_error(des_r, ori).squeeze() ** 2
        cost += oerr

        return cost * 1e3


class CollisionConstraint(Constraint):
    """docstring for CollisionConstraint."""

    def __init__(
        self,
        collision_checker,
        chain,
    ):
        super(CollisionConstraint, self).__init__()
        self.chain = chain
        self.collision_checker = collision_checker

    def error(self, x):
        return self.tensor_error(x if torch.is_tensor(x) else torch.tensor(x)).item()

    def tensor_error(self, x):
        """
        Differentiable collision function
        """
        x = x.reshape(-1, self.chain.num_joints)
        # loop over each step in the trajectory
        error = 0.0
        # for ti in range(x.shape[0]):
        #     # loop over each link
        #     lpos = get_link_positions(x[ti])
        #     for li in range(self.chain.num_joints):
        #         # loop over each object
        #         for oi in range(self.objects):
        #             # get the SD between the link and the object
        #             error += max(d_safe - sd(lpos[li], self.objects[oi]), 0)
        return error

    # def tensor_error(self, x):
    #     x = x.reshape(-1,self.chain.num_joints)
    #     for i in range(x.shape[0]):
    #         self.collision_checker.set_chain(x[i])
    #         dists += sum(self.collision_checker.dist_to_obstacles())
    #         import pdb; pdb.set_trace()
    #     return pose_error( x, self.desired_pose, self.chain,
    #       self.error_type, self.orientation_error_type
    #     )


class SelfCollisionConstraint(Constraint):
    """docstring for SelfCollisionConstraint."""

    def __init__(
        self,
        chain,
        desired_pose,
        error_type="pose",
        orientation_error_type="log",
    ):
        super(SelfCollisionConstraint, self).__init__()
        self.chain = chain
        self.desired_pose = desired_pose
        self.error_type = error_type
        self.orientation_error_type = orientation_error_type

    def error(self, x):
        return self.tensor_error(x if torch.is_tensor(x) else torch.tensor(x)).item()

    def tensor_error(self, x):
        return pose_error(
            x, self.desired_pose, self.chain, self.error_type, self.orientation_error_type
        )
