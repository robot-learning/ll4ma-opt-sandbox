# Third Party
import numpy as np
import torch
from pyquaternion import Quaternion


def pos_from_homogeneous(T):
    return T[:3, 3]


def rot_from_homogeneous(T):
    return T[:3, :3]


def quat_from_homogeneous(T):
    """
    Converts rotation matrix from homogenous TF matrix to quaternion.
    """
    if isinstance(T, torch.Tensor):
        T = T.numpy()
    q = Quaternion(matrix=T)  # w, x, y, z
    q = np.array([q.x, q.y, q.z, q.w])  # Need to switch to x, y, z, w
    return q


def rotation_matrix_log(R):
    """
    Lynch And Park Equation 3.53 / 3.54
    with minor check to ensure the matrix is sufficienty close to valid rotation matrix
    if not, row normalize
    """
    if torch.abs(torch.trace(R) - 1) > 2.0:
        R = torch.nn.functional.normalize(R, p=2, d=0)
    # make the value just tiny bit smaller to avoid acos limits
    phi = torch.arccos(0.5 * (torch.trace(R) - 1) * (1 - 1e-10))

    # Store the angular velocity in its skew symmetric "hat" form
    omega_hat = (1 / (2 * torch.sin(phi))) * (R - R.T)

    return omega_hat, phi


def vee(M):
    """The vee operator takes a ske-symetric matrix and creates the equivalent vector form."""
    M_v = torch.zeros((3, 1))
    M_v[0, 0] = M[2, 1]
    M_v[1, 0] = M[0, 2]
    M_v[2, 0] = M[1, 0]
    return M_v


def hat(v):
    """The hat operator takes a 3-vector and creates the equivalent skew-symmetric matrix."""
    v_hat = torch.zeros((3, 3))

    v_hat[0, 1] = -v[2, 0]  # -v_z
    v_hat[1, 0] = v[2, 0]  # v_z
    v_hat[0, 2] = v[1, 0]  # v_y
    v_hat[2, 0] = -v[1, 0]  # -v_y
    v_hat[1, 2] = -v[0, 0]  # -v_x
    v_hat[2, 1] = v[0, 0]  # v_x

    return v_hat


def transformation_matrix_log(T):
    R = T[:3, :3]
    omega_hat, phi = rotation_matrix_log(R)

    # TODO: is omega_hat unit length or does it have theta in it?
    V = (
        np.eye(3)
        + (1 - torch.cos(phi)) / (phi**2) * omega_hat
        + (phi - torch.sin(phi)) / phi**3 * omega_hat**2
    )

    omega_vee = vee(omega_hat)

    twist = torch.zeros((6, 1))
    twist[:3] = omega_vee
    twist[3:] = V.inv() @ T[3, :3]
    return twist


class PseudoHuberLoss:
    def __init__(self, delta=1e-2):
        """ """
        self.delta = delta

    def loss(self, v):
        if torch.is_tensor(v):
            lib = torch
        else:
            lib = np

        return lib.sum((self.delta**2 + v**2) ** (0.5) - self.delta, 1)

    def gradient(self, v):
        return v * (self.delta**2 + v**2) ** (-0.5)

    def hessian(self, v):
        if torch.is_tensor(v):
            lib = torch
        else:
            lib = np

        v_squared = v**2
        x = v_squared * (self.delta**2 + v_squared) ** (-1.5)
        return lib.diagflat(x)
