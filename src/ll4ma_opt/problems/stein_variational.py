# -*- coding: utf-8 -*-
"""
Created on Mon Nov 14 16:57:46 2022

@author: tabor
"""
# Third Party
import numpy as np
import torch

# ll4ma
from ll4ma_opt.problems import Problem


class Kernel:
    def __init__(self, math_library):
        self.math_library = math_library

    """
    Evaluates the kernel between each point and returns square matrix of kernels
    """

    def __call__(self, points):
        raise NotImplementedError("Kernel call is not implemented")

    def grad(self, points):
        raise NotImplementedError("Kernel grad is not implemented")

    def eval_and_gradient(self, points):
        return self(points), self.grad(points)


class RBFKernel(Kernel):
    def __init__(self, math_library):
        super().__init__(math_library)
        self.number_particles = None

    def pre_compute(self, particles):
        num_particles = len(particles)
        particle_size = particles[0].shape[0]
        if not num_particles == self.number_particles:
            if self.math_library is np:
                (
                    self.left_subtraction_indices,
                    self.right_subtraction_indices,
                ) = self.math_library.tril_indices(num_particles, -1)
            if self.math_library is torch:
                (
                    self.left_subtraction_indices,
                    self.right_subtraction_indices,
                ) = self.math_library.tril_indices(num_particles, num_particles, -1)

        errors = (
            particles[self.left_subtraction_indices, :]
            - particles[self.right_subtraction_indices, :]
        )
        pair_distances_sq = errors.swapaxes(2, 1) @ errors
        median = self.math_library.median(pair_distances_sq)  # + 1e-25

        h = median / np.log(num_particles)

        kernel_list = self.math_library.exp(-pair_distances_sq / h)

        return errors, pair_distances_sq, h, kernel_list, num_particles, particle_size

    def __call__(self, particles):
        errors, pair_distances_sq, h, kernel_list, num_particles, particle_size = self.pre_compute(
            particles
        )

        # pre initialize main diagonal of k as 1s, this corresponds to kernel evaluating
        # the similarity between a point and itself
        if self.math_library is np:
            k = self.math_library.eye(num_particles, dtype=particles.dtype).reshape(
                (num_particles, num_particles, 1)
            )
        elif self.math_library is torch:
            k = self.math_library.eye(
                num_particles, dtype=particles.dtype, device=particles.device
            ).reshape((num_particles, num_particles, 1))
        # lower triangular
        k[self.left_subtraction_indices, self.right_subtraction_indices, 0] = kernel_list.flatten()
        # upper triangular
        k[self.right_subtraction_indices, self.left_subtraction_indices, 0] = kernel_list.flatten()
        return k

    def grad(self, particles):
        errors, pair_distances_sq, h, kernel_list, num_particles, particle_size = self.pre_compute(
            particles
        )

        # pre initialize main diagonal of kernel_gradient as zeros, kernels are at max
        if self.math_library is np:
            kernel_gradient = self.math_library.zeros(
                (num_particles, num_particles, particle_size, 1), dtype=particles.dtype
            )
        elif self.math_library is torch:
            kernel_gradient = self.math_library.zeros(
                (num_particles, num_particles, particle_size, 1),
                dtype=particles.dtype,
                device=particles.device,
            )
        kernel_gradient_list = -(2 / h) * errors * kernel_list
        # lower triangular
        kernel_gradient[self.left_subtraction_indices, self.right_subtraction_indices, :, :] = (
            kernel_gradient_list
        )
        # upper triangular, where the gradient is pushing in opposite direction
        kernel_gradient[self.right_subtraction_indices, self.left_subtraction_indices, :, :] = (
            -kernel_gradient_list
        )

        return kernel_gradient

    def eval_and_gradient(self, particles):
        errors, pair_distances_sq, h, kernel_list, num_particles, particle_size = self.pre_compute(
            particles
        )

        # pre initialize main diagonal of k as 1s, this corresponds to kernel evaluating
        # the similarity between a point and itself
        # pre initialize main diagonal of kernel_gradient as zeros, kernels are at max

        if self.math_library is np:
            k = self.math_library.eye(num_particles, dtype=particles.dtype).reshape(
                (num_particles, num_particles, 1)
            )
            kernel_gradient = self.math_library.zeros(
                (num_particles, num_particles, particle_size, 1), dtype=particles.dtype
            )
        elif self.math_library is torch:
            k = self.math_library.eye(
                num_particles, dtype=particles.dtype, device=particles.device
            ).reshape((num_particles, num_particles, 1))
            kernel_gradient = self.math_library.zeros(
                (num_particles, num_particles, particle_size, 1),
                dtype=particles.dtype,
                device=particles.device,
            )
        # lower triangular
        k[self.left_subtraction_indices, self.right_subtraction_indices, 0] = kernel_list.flatten()
        # upper triangular
        k[self.right_subtraction_indices, self.left_subtraction_indices, 0] = kernel_list.flatten()

        kernel_gradient_list = -(2 / h) * errors * kernel_list
        # lower triangular
        kernel_gradient[self.left_subtraction_indices, self.right_subtraction_indices, :, :] = (
            kernel_gradient_list
        )
        # upper triangular, where the gradient is pushing in opposite direction
        kernel_gradient[self.right_subtraction_indices, self.left_subtraction_indices, :, :] = (
            -kernel_gradient_list
        )

        return k, kernel_gradient


class SteinWrapper(Problem):
    """https://arxiv.org/pdf/1608.04471.pdf
    assuming p(x) = b * exp(-a * f(x)) find the series of particles
    that implicitly represent the distribution p(x)"""

    def __init__(
        self, problem, particles=10, repulsive_weight=1, kernel=RBFKernel, math_library=np
    ):
        self.problem = problem
        self.original_size = problem.size()
        self.num_particles = particles
        self._size = self.original_size * particles
        self.repulsive_weight = repulsive_weight
        super().__init__()

        if math_library is np:
            if not (np.isinf(problem.min_bounds).any() or np.isinf(problem.max_bounds).any()):
                self.initial_solution = math_library.random.uniform(
                    problem.min_bounds, problem.max_bounds, (self.original_size, self.num_particles)
                ).T.reshape((-1, 1))

        if math_library is torch:
            if not (torch.isinf(problem.min_bounds).any() or torch.isinf(problem.max_bounds).any()):
                self.initial_solution = torch.tensor(
                    np.random.uniform(
                        problem.min_bounds.detach().cpu().numpy(),
                        problem.max_bounds.detach().cpu().numpy(),
                        (self.original_size, self.num_particles),
                    ).T.reshape((-1, 1))
                ).to(problem.min_bounds.device, dtype=problem.min_bounds.dtype)

        self.min_bounds = math_library.tile(self.problem.min_bounds, (particles, 1)).reshape(
            (-1, 1)
        )
        self.max_bounds = math_library.tile(self.problem.max_bounds, (particles, 1)).reshape(
            (-1, 1)
        )

        self.math_library = math_library
        self.kernel = kernel(math_library)

        # TODO duplicate equality and inequality constraints, with correct index remapping

    def cost_gradient(self, x):
        particles = x.reshape(self.num_particles, self.original_size, 1)

        prob_gradients = self.problem.batch_gradient(particles)

        k, kernel_gradient = self.kernel.eval_and_gradient(particles)

        # desired shape is (number_particles,number_particles,original_size,1)

        if self.math_library is np:
            kernel_scalars = self.math_library.tile(k, (self.original_size, 1, 1, 1)).transpose(
                (1, 2, 0, 3)
            )
        elif self.math_library is torch:
            kernel_scalars = self.math_library.tile(k, (self.original_size, 1, 1, 1)).permute(
                (1, 2, 0, 3)
            )

        # duplicate problem gradient so there is one for each particle
        probability_gradient = self.math_library.tile(prob_gradients, (self.num_particles, 1, 1, 1))
        step_contributions = (
            kernel_scalars * probability_gradient + self.repulsive_weight * kernel_gradient
        )

        # compute gradient in shape (number_particles,original_size,1)
        gradient = step_contributions.sum(1) / self.num_particles

        # reorder to match column vector notation
        return gradient.reshape((-1, 1))

    def cost(self, x, summed=True, minimum=True):
        particles = x.reshape((self.num_particles, self.original_size, 1))
        costs = self.problem.batch_cost(particles)
        if summed:
            return self.math_library.sum(costs)
        if minimum:
            return self.math_library.min(costs)
        return costs, particles

    def size(self):
        return self._size

    def visualize_optimization(self, *args, **kwargs):
        return self.problem.visualize_optimization(*args, **kwargs)
