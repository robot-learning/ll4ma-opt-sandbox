#!/usr/bin/env python3
"""
Implementation of Cross-Entropy Method (CEM)

Author: Adam Conkey
"""
# Standard Library
import time

# Third Party
import numpy as np
import torch
from tqdm import tqdm

# ll4ma
from ll4ma_opt.solvers import OptimizationSolver, SolverReturn
from ll4ma_opt.solvers.opt_conv_criteria import (
    _MEAN_COST_CRIT,
    _TOP_COST_CRIT,
    _KL_CRIT,
    _COV_DET_CRIT,
)


class CEM(OptimizationSolver):
    """
    Cross-Entropy Method (CEM) solver initially developed for rare-event simulation [1] and
    widely used in robotics, e.g. motion planning [2].

    Implements the basic steps of:
        1. Generate N samples.
        2. Evaluate the cost of each sample.
        3. Take the subset of top-K samples with the lowest cost (a.k.a. the elite set).
        4. Re-fit the distribution with the top-K elite samples.
        5. Repeat until distribution converges on low-cost region.

    [1] Rubinstein, Reuven Y., and Dirk P. Kroese. The cross-entropy method: a unified
        approach to combinatorial optimization, Monte-Carlo simulation and machine learning.
        Springer Science & Business Media, 2013.

    [2] Kobilarov, Marin. "Cross-entropy motion planning." The International Journal of
        Robotics Research 31.7 (2012): 855-871.
    """

    def __init__(
        self,
        problem,
        sampler,
        n_samples=100,
        n_elite=10,
        verbose=False,
        cost_threshold=None,
        mean_solve_only=False,
        kl_tolerance=0.01,
        cov_tolerance=1e-8,
        uniform_initial_sampling=False,
        FP_PRECISION=1e-7,
        library=np,
    ):
        """
        problem (Problem): Problem to be optimized
        sampler (Sampler): Sampler to generate samples of decision variable from
        n_samples (int): Number of samples to generate at each iteration
        n_elite (int): Number of elite samples to re-fit distribution with at each iteration
        verbose (bool): Have verbose output
        cost_threshold (float): Converge early if cost drops below cost_threshold (default None)
        mean_solve_only (bool): Only return mean as solution not top sample (default False)
        kl_tolerance (float): Convergance tolerance for KL between prev and cur dist
        uniform_initial_sampling (bool): take the initial sample from a uniform
                                         distribution rather than the common distribution
        """
        if n_samples < 2 or n_samples <= n_elite:
            raise ValueError("Number of samples must be at least 2 and greater than n_elite.")
        if n_elite < 2:
            raise ValueError("Number of elite samples must be at least 2.")
        super().__init__(
            problem, FP_PRECISION=FP_PRECISION, library=library, cost_threshold=cost_threshold
        )
        self.sampler = sampler
        self.n_samples = n_samples
        self.n_elite = n_elite
        self.verbose = verbose
        self.mean_solve_only = mean_solve_only
        self.test_top = self.cost_threshold is not None and not self.mean_solve_only
        self.kl_tolerance = kl_tolerance
        self.cov_tolerance = cov_tolerance
        self.uniform_initial_sampling = uniform_initial_sampling
        self.elites = None

    def optimize(self, x0=None, max_iterations=100):
        """
        Runs CEM optimization up to convergence or terminates at max_iterations.

        Args:
            x0 (ndarray): Initial distribution mean, shape (n_dims,), defaults to all zeros.
            max_iterations (int): Maximum number of iterations to run CEM
        """
        self.sampler.set_params(x0)

        if self.verbose:
            print(f"\nRunning CEM optimization for up to {max_iterations} iterations...")
            pbar = tqdm(total=max_iterations, desc="CEM")

        start = time.time()
        with torch.no_grad():
            # TODO: We could probably make these optional for efficiency
            iterates = []
            sample_iterates = []

            # NOTE: if testing for cost threshold, then we sort the elite below
            # NOTE: updating value here in case cost_threshold was set after init
            self.test_top = self.cost_threshold is not None and not self.mean_solve_only

            for i in range(max_iterations):
                # Generate samples
                if i == 0 and self.uniform_initial_sampling:
                    samples = self.sampler.uniform_sampling(self.n_samples)
                else:
                    samples = self.sampler.sample(self.n_samples)
                iterates.append(self.sampler.get_mean().numpy())
                sample_iterates.append(samples.numpy())
                # Store previous distributoin data for KL comparison

                # Find top-k low-cost samples (the elite set)
                costs = self.problem.tensor_batch_cost(samples).flatten()
                top_costs, top_idxs = costs.topk(
                    self.n_elite, dim=-1, largest=False, sorted=self.test_top
                )
                elite = samples[top_idxs]

                # Re-fit distribution over elite set
                self.sampler.fit(elite)
                mean = self.sampler.get_mean()
                mean_cost = self.problem.tensor_batch_cost(mean.unsqueeze(0)).item()
                converged, conv_criteria = self.test_convergance(mean_cost, top_costs)

                if converged:
                    if conv_criteria == _TOP_COST_CRIT:
                        sol = elite[0].unsqueeze(-1).numpy()
                        cost = top_costs[0].numpy()
                    elif conv_criteria == _MEAN_COST_CRIT or self.mean_solve_only:
                        sol = mean.unsqueeze(-1).numpy()
                        cost = mean_cost
                    else:
                        # See if the top cost is better than the mean cost
                        top_idx = torch.argmin(top_costs)
                        top_cost = top_costs[top_idx]
                        if top_cost < mean_cost:
                            sol = samples[top_idx].unsqueeze(-1).numpy()
                            cost = top_cost
                            if self.verbose:
                                "Returning top sample not mean"
                        else:
                            sol = mean.unsqueeze(-1).numpy()
                            cost = mean_cost

                    break
                if self.verbose:
                    pbar.update(1)

        duration = time.time() - start
        iterates = np.expand_dims(np.array(iterates), -1)
        sample_iterates = np.array(sample_iterates)
        self.elites = elite

        # Return mean and mean cost if converged at max iters
        if converged is False:
            converged = True
            conv_criteria = "max iters"
            cost = mean_cost
            sol = mean.unsqueeze(-1).numpy()
        return SolverReturn(
            solution=sol,
            cost=float(cost),
            converged=bool(converged),
            convergence_criteria=conv_criteria,
            time=duration,
            iterates=iterates,
            sample_iterates=sample_iterates,
        )

    def test_convergance(self, cur_cost, elite_costs):
        """
        Different tests for convergance

        cur_cost - current mean cost
        top_costs - lits of elite sample costs
        """
        # Check KL between previous and current sampler dist
        kl = self.sampler.kl_to_prev()
        if kl < self.kl_tolerance:
            return True, {_KL_CRIT: kl}

        top_cost = elite_costs[0]
        # Check cost below a desired tolerance
        if self.test_top and top_cost < self.cost_threshold:
            return True, {_TOP_COST_CRIT: (self.cost_threshold, top_cost)}
        if self.cost_threshold is not None and cur_cost < self.cost_threshold:
            return True, {_MEAN_COST_CRIT: (self.cost_threshold, cur_cost)}

        # If the covariance gets too small, then we have converged
        cov_det = self.sampler.covariance_determinant()
        if cov_det < self.cov_tolerance:
            return True, {_COV_DET_CRIT: cov_det}

        return False, (None)
