# Third Party
import numpy as np


def armijo_condition_satisfied(f, project_func, x, alpha, gradient, p, c1=1e-4):
    """
    Nocedal and Wright equation 3.4
    """
    step_vector = alpha * p
    step_vector = project_func(step_vector, x)

    return f(x + step_vector) <= (f(x) + c1 * alpha * gradient.T @ p).item()


def armijo_condition_batch(f, project_func, x, alphas, gradient, p, c1=1e-4, library=np):
    """
    Nocedal and Wright equation 3.4
    """
    step_vectors = (alphas * p).T
    fx_step = library.stack(
        [f(x + project_func(step_vector.unsqueeze(1), x)) for step_vector in step_vectors]
    )
    fx_max = f(x) + alphas * (c1 * gradient.T @ p).squeeze()
    satisfied = fx_step - fx_max < 1e-7
    satisfied[-1] = True
    return satisfied


def curvature_condition_satisfied(f_grad, x, alpha, gradient, p, c2=0.9):
    """
    Nocedal and Wright equation 3.5
    """
    return np.dot(f_grad(x + alpha * p).T, p) >= c2 * np.dot(gradient.T, p)


def wolfe_conditions_satisfied(f, f_grad, x, alpha, gradient, p, c1=1e-4, c2=0.9):
    assert c1 > 0
    assert c2 > c1
    assert c2 < 1
    return armijo_condition_satisfied(
        f, x, alpha, gradient, p, c1=c1
    ) and curvature_condition_satisfied(f_grad, x, alpha, gradient, p, c2=c2)


# end
