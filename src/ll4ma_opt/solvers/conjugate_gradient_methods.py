# Standard Library
import time

# Third Party
import numpy as np

# ll4ma
from ll4ma_opt.solvers.line_search import backtracking_step_length, LineSearchSolver
from ll4ma_opt.solvers.optimization_solver import SolverReturn
from ll4ma_opt.solvers.opt_conv_criteria import _GRAD_THRESH


class ConjugateGradient(LineSearchSolver):
    def __init__(
        self,
        problem,
        alpha=1.0,
        rho=0.9,
        c1=1e-4,
        min_alpha=1e-10,
        FP_PRECISION=1e-7,
        library=np,
        cost_threshold=None,
        PR=False,
    ):
        assert alpha > 0, "Alpha must be positive"
        assert 0 < rho < 1, "Constant ro must be in range (0,1) "
        assert 0 < c1 < 1, "Constant c1 must be in range (0,1) "
        assert 0 < min_alpha <= alpha, "Constant min alpha must be in range (0,alpha]"
        super().__init__(
            problem, FP_PRECISION=FP_PRECISION, library=library, cost_threshold=cost_threshold
        )
        self.current_step = self.math_library.zeros_like(self.problem.initial_solution)
        self.alpha = alpha
        self.rho = rho
        self.c1 = c1
        self.min_alpha = min_alpha

        self.previous_gradient = None
        self.PR = PR

    def compute_search_direction(self, current_solution, binding_set, gradient):
        # Fletcher-Reeves method for non-linear conjugate gradient method.
        # TODO: update wolfe_line_search to zoom with cubic interpolation, optimization goes to
        # infinite loop sometimes.
        # Exact doesnt work for multidemnsional x values

        if self.previous_gradient is None:
            self.previous_gradient = gradient

        if self.PR:
            beta = (
                np.inner(gradient, (gradient.T - self.previous_gradient.T))
                / np.linalg.norm(self.previous_gradient) ** 2
            )
            beta = max(beta, 0)
        else:
            beta = (gradient.T @ gradient) / (self.previous_gradient.T @ self.previous_gradient)
        self.current_step = -gradient + float(beta) * self.current_step
        self.previous_gradient = gradient

        return self.current_step

    def compute_step_length(self, current_solution, search_direction, gradient):
        return backtracking_step_length(
            self.problem.cost,
            self.project_onto_box_constraints,
            current_solution,
            self.alpha,
            self.rho,
            search_direction,
            gradient,
            self.c1,
            self.min_alpha,
        )

    def optimizeLinear(self, Q, b, initial_solution=None, bounds=None, n=None, max_iterations=100):
        # Linear Conjugate Gradient Method, Objective must be Quadratic and matrix
        # form Ax - b as residue
        start = time.time()
        converged = False
        iterates = []
        criteria = {}

        if initial_solution is None:
            initial_solution = np.zeros(n)
        else:
            n = len(initial_solution)
        x = initial_solution
        iterates.append(np.copy(x))
        r_k = initial_solution.T @ Q.T - b.T
        p_k = -r_k
        for k in range(max_iterations):
            grad_mag = np.linalg.norm(r_k)
            grad_thresh = grad_mag < self.FP_PRECISION
            if grad_thresh:
                converged = True
                criteria = {_GRAD_THRESH: grad_mag}
                break

            alpha = float((r_k @ r_k.T) / (p_k @ Q @ p_k.T))
            x = x + alpha * p_k.T
            r_kp = r_k + alpha * p_k @ Q.T
            beta = (r_kp @ r_kp.T) / (r_k @ r_k.T)
            p_k = -r_kp + beta * p_k
            r_k = r_kp
            iterates.append(np.copy(x))
        quadcost = 0.5 * x.T @ Q @ x
        linear_cost = -b.T @ x
        cost = quadcost + linear_cost
        duration = time.time() - start
        return SolverReturn(
            solution=x,
            cost=cost.item(),
            converged=bool(converged),
            convergence_criteria=criteria,
            time=duration,
            iterates=np.array(iterates),
        )
