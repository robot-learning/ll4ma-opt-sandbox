"""
Core line search methods for unconstrained, smooth optimization

Check the book Numerical Optimization by Nocedal & Wright, Chapter 3.
Names of the variables in this file are according to the book as well.
"""

# Standard Library
import time

# Third Party
import numpy as np
import torch
import scipy.optimize

# ll4ma
from ll4ma_opt.solvers.conditions import armijo_condition_satisfied, armijo_condition_batch
from ll4ma_opt.solvers.matrix_utils import (
    linear_solve_cholesky_adaptive,
    linear_solve_lu_adaptive,
    linear_solve_cholesky_adaptive_fast,
)
from ll4ma_opt.solvers.optimization_solver import OptimizationSolver, SolverReturn
from ll4ma_opt.solvers.opt_conv_criteria import _GRAD_THRESH, _STEP_THRESH, _COST_THRESH


class LineSearchSolver(OptimizationSolver):
    """
    Line search class that handles general iterations of the solver, bound constraint projection,
    and convergence test.

    This class is abstract as it doesn't implement the search direction computation or
    step length choice. Subclasses implement different strategies for these.
    """

    def __init__(self, problem, FP_PRECISION=1e-7, library=np, use_lu=False, cost_threshold=None):
        super().__init__(
            problem, FP_PRECISION=FP_PRECISION, library=library, cost_threshold=cost_threshold
        )

        if use_lu:
            self.linear_solve = linear_solve_lu_adaptive
        else:
            self.linear_solve = linear_solve_cholesky_adaptive
        self.solve_linear_eq_constraints = False
        self.step_length_convergence_scale = 1e3

    def hessian(self, x):
        return self.problem.cost_hessian(x)

    def optimize(self, x0, max_iterations=100):
        self.reset_time_vars()
        assert max_iterations >= 1, "max_iterates must be ≥ 1"
        assert isinstance(max_iterations, int), "max_iterates must be an int"

        tot_start = time.time()

        if self.solve_linear_eq_constraints:
            # Append 0 slack variables to the initial_solution
            m = len(self.problem.linear_equality_vector)
            slack_k = np.zeros((m, 1))
            slacks = self.math_library.stack(
                [self.math_library.zeros_like(slack_k)] * max_iterations
            )

        else:
            slack_k = None

        # make a stack of zeros like iterate vectors
        iterates = self.math_library.stack(
            [self.math_library.zeros_like(x0)] * (max_iterations + 1)
        )
        converged = False
        criteria = {}  # Store convergence criteria as dictionary

        p_k = self.math_library.zeros_like(x0)
        alpha_k = 0.0
        x_k = x0 + self.project_onto_box_constraints(alpha_k * p_k, x0)
        iterates[0] = x_k

        for k in range(1, max_iterations + 1):
            start = time.time()
            if hasattr(self.problem, "pre_compute"):
                self.problem.pre_compute(x_k)
            gradient = self.problem.cost_gradient(x_k)
            near_upper_bound = self.math_library.logical_and(
                (x_k < self.problem.min_bounds + self.FP_PRECISION), (gradient > 0)
            )
            near_lower_bound = self.math_library.logical_and(
                (x_k > self.problem.max_bounds - self.FP_PRECISION), (gradient < 0)
            )
            binding_set = self.math_library.argwhere(
                self.math_library.logical_or(near_upper_bound, near_lower_bound)
            ).flatten()
            self._gradTime += time.time() - start

            start = time.time()
            if self.solve_linear_eq_constraints:
                p_k, delta_slack_k = self.compute_search_direction(
                    x_k, binding_set, gradient, slack_k
                )
            else:
                p_k = self.compute_search_direction(x_k, binding_set, gradient)

            self._searchTime += time.time() - start

            start = time.time()
            if self.solve_linear_eq_constraints:
                alpha_k = self.compute_eq_step_length(x_k, p_k, slack_k, delta_slack_k)
            else:
                alpha_k = self.compute_step_length(x_k, p_k, gradient)
            self._lengthTime += time.time() - start

            delta_x_k = self.project_onto_box_constraints(alpha_k * p_k, x_k)

            # Update slack variabvles using same line search result
            if self.solve_linear_eq_constraints:
                slack_k = slack_k + alpha_k * delta_slack_k
                slacks[k - 1] = slack_k

            #
            # Test for convergence
            #

            # Set converged if either the maximum (l infinity) search direction is close to zero
            search_direction_max = self.math_library.abs(
                self.project_onto_box_constraints(p_k, x_k)
            ).max()

            # or the maximum step (l infinity) is too small (with tighter tolerance)
            step_max = self.math_library.abs(delta_x_k).max() * self.step_length_convergence_scale

            converged = search_direction_max < self.FP_PRECISION or step_max < self.FP_PRECISION

            # Update next iterate based on line search result
            x_k = x_k + delta_x_k

            # Store iterates for visualization and debugging purposes
            iterates[k] = x_k

            # Test for eq constrained convergence
            if self.solve_linear_eq_constraints:
                converged = self._r_norm(x_k, slack_k) < self.FP_PRECISION

            # Test using the cost_threshold
            cur_cost = float(self.problem.cost(x_k))
            if self.cost_threshold is not None and cur_cost < self.cost_threshold:
                converged = True
                criteria[_COST_THRESH] = (self.cost_threshold, cur_cost)
                break

            if converged:
                criteria[_GRAD_THRESH] = search_direction_max
                criteria[_STEP_THRESH] = step_max
                break

        if not converged:
            if self.cost_threshold is not None:
                criteria[_COST_THRESH] = (self.cost_threshold, cur_cost)
            criteria[_GRAD_THRESH] = search_direction_max
            criteria[_STEP_THRESH] = step_max

        duration = time.time() - tot_start
        return SolverReturn(
            solution=x_k,
            cost=float(self.problem.cost(x_k)),
            converged=bool(converged),
            convergence_criteria=criteria,
            time=duration,
            iterates=iterates[: (k + 1)],
        )

    def project_onto_box_constraints(self, step_vector, x):
        # project onto box constraints
        # if x+p*a>max_bounds, p=(max_bounds-x) such that  x+p=max_bounds
        step_vector = self.math_library.minimum(step_vector, self.problem.max_bounds - x)
        step_vector = self.math_library.maximum(step_vector, self.problem.min_bounds - x)
        return step_vector

    def hessian_constraint_modification(self, H, binding_set):
        if len(binding_set) > 0:
            H[binding_set, :] = 0
            H[:, binding_set] = 0
            H[binding_set, binding_set] = 1
        return H

    def compute_search_direction(
        self, current_solution, binding_set, gradient, current_eq_slack=None
    ):
        raise NotImplementedError()

    def compute_step_length(self, current_solution, search_direction, gradient):
        raise NotImplementedError()


class NewtonMethod(LineSearchSolver):
    """
    Algorithm 3.2.
    """

    def __init__(
        self,
        problem,
        alpha=1.0,
        rho=0.9,
        c1=1e-4,
        min_alpha=1e-10,
        FP_PRECISION=1e-7,
        library=np,
        alpha_dec_eq=0.3,
        use_lu=False,
        cost_threshold=None,
    ):
        assert alpha > 0, "Alpha must be positive"
        assert 0 < rho < 1, "Constant rho must be in range (0,1)"
        assert 0 < c1 < 1, "Constant c1 must be in range (0,1)"
        assert 0 < min_alpha <= alpha, "Constant min alpha must be in range (0,alpha]"
        assert (
            0 < alpha_dec_eq <= 0.5
        ), "Constant alpha_dec_eq must be in range (0, 0.5), suggestd (0.01, 0.3)"
        super().__init__(
            problem,
            FP_PRECISION=FP_PRECISION,
            library=library,
            use_lu=use_lu,
            cost_threshold=cost_threshold,
        )
        self.alpha = alpha
        self.rho = rho
        self.c1 = c1
        self.min_alpha = min_alpha
        self.alpha_dec_eq = (
            alpha_dec_eq  # book says typical is 0.01 - 0.3 and must be in range 0-0.5
        )

        # Check if we have linear equality constraints in the problem
        if problem.linear_equality_matrix is not None:
            A = problem.linear_equality_matrix
            b = problem.linear_equality_vector
            m = A.shape[0]
            p = A.shape[1]
            assert m < p, "Linear eq matrix must be fat (fewer rows than columns)!"
            assert m == b.shape[0], "Linear eq matrix and vector don't match sizes"
            assert m == np.linalg.matrix_rank(A), "Linear eq matrix must have full row rank!"

            self.solve_linear_eq_constraints = True

            # TODO: Compare using LDL to LU for this solver
            self.linear_solve = linear_solve_lu_adaptive

    def compute_search_direction(
        self, current_solution, binding_set, gradient, current_eq_slack=None
    ):
        if self.solve_linear_eq_constraints:
            return self.compute_eq_search_direction(
                current_solution, binding_set, gradient, current_eq_slack
            )

        x = current_solution
        B = self.hessian(x)
        B = self.hessian_constraint_modification(B, binding_set)

        return self.linear_solve(B, -gradient, library=self.math_library)

    def compute_eq_search_direction(
        self, current_solution, binding_set, gradient, current_eq_slack
    ):
        x = current_solution
        # v = current_eq_slack

        H = self.hessian(x)
        H = self.hessian_constraint_modification(H, binding_set)
        A = self.problem.linear_equality_matrix
        b = self.problem.linear_equality_vector
        m = len(b)

        # Create augmented LHS of the form
        # [[H A^T ]
        #  [A 0_m ]]
        aug_H = np.vstack([np.hstack([H, A.T]), np.hstack([A, np.zeros((m, m))])])

        # Create augmented RHS of the form
        # [[\del f]
        #  [Ax-b]]
        residual = A @ x - b

        aug_gradient = np.vstack([gradient, residual])
        aug_delta = self.linear_solve(aug_H, -aug_gradient, library=self.math_library)
        return aug_delta[: len(current_solution)], aug_delta[len(current_solution) :]

    def compute_eq_step_length(self, x, delta_x, v, delta_v):
        # Run backtring line search with the residual cost
        # This uses the residual eval Eq in Alg 10.2

        alpha = self.alpha
        while self._r_norm(x + alpha * delta_x, v + alpha * delta_v) > (
            1 - self.alpha_dec_eq * alpha
        ) * self._r_norm(x, v):
            alpha = self.rho * alpha
            if alpha < self.min_alpha:
                return alpha

        return alpha

    def compute_step_length(self, current_solution, search_direction, gradient):
        return backtracking_step_length(
            self.problem.cost,
            self.project_onto_box_constraints,
            current_solution,
            self.alpha,
            self.rho,
            search_direction,
            gradient,
            self.c1,
            self.min_alpha,
        )

    def _r_norm(self, z, u):
        """
        Computes the norm of the primal and dual residuals for use in the linear equality
        constrained newton method for testing convergence of the solver and line search

        z - decision variable
        u - slack variables on linear constraints
        """
        A = self.problem.linear_equality_matrix
        b = self.problem.linear_equality_vector

        del_f = self.problem.cost_gradient(z)
        r_dual = del_f + A.T @ u
        r_pri = A @ z - b

        if self.math_library is np:
            r = self.math_library.concatenate((r_dual, r_pri))
        else:
            # NOTE: Assumes pytorch
            r = self.math_library.cat((r_dual, r_pri), 0)

        return self.math_library.linalg.norm(r)


class GradientDescent(NewtonMethod):
    """
    Newton Method with identity matrix as Hessian
    """

    def __init__(
        self,
        problem,
        alpha=1.0,
        rho=0.9,
        c1=1e-4,
        min_alpha=1e-10,
        FP_PRECISION=1e-7,
        library=np,
        cost_threshold=None,
    ):
        super().__init__(
            problem,
            alpha,
            rho,
            c1=c1,
            min_alpha=min_alpha,
            FP_PRECISION=FP_PRECISION,
            library=library,
            cost_threshold=cost_threshold,
        )
        if self.math_library is np:
            self._hessian = self.math_library.eye(problem.size(), dtype=problem.min_bounds.dtype)
        else:
            self._hessian = self.math_library.eye(
                problem.size(), dtype=problem.min_bounds.dtype, device=problem.min_bounds.device
            )

    def hessian(self, x):
        return self._hessian


class NewtonMethodWolfeLine(NewtonMethod):
    """
    Algorithm 3.2. with wolfe line search instead of backtracking
    """

    def __init__(
        self,
        problem,
        alpha=1.0,
        rho=0.9,
        c1=1e-4,
        c2=0.9,
        FP_PRECISION=1e-7,
        cost_threshold=None,
    ):
        super().__init__(
            problem, alpha, rho, c1=c1, FP_PRECISION=FP_PRECISION, cost_threshold=cost_threshold
        )
        self.c2 = c2

    def compute_step_length(self, current_solution, search_direction, gradient):
        x = current_solution
        p = search_direction

        def phi(alpha):
            return self.problem.cost(x + alpha * p)

        def phi_d(alpha):
            return np.matmul(self.problem.cost_gradient(x + alpha * p).T, p).item()

        return wolfe_line_search(phi, phi_d, alpha_max=2, c1=self.c1, c2=self.c2)


class Momentum(LineSearchSolver):
    """
    Equation 18 DOOMED. https://arxiv.org/abs/1608.00309
    equivalent equation to more standard equation
    http://proceedings.mlr.press/v28/sutskever13.pdf
    """

    def __init__(
        self,
        problem,
        alpha=1.0,
        rho=0.9,
        momentum=0.9,
        c1=1e-4,
        min_alpha=1e-10,
        FP_PRECISION=1e-7,
        library=np,
        cost_threshold=None,
    ):
        assert 0 < momentum < 1, "Constant momentum must be in range (0,1) "
        assert alpha > 0, "Alpha must be positive"
        assert 0 < rho < 1, "Constant ro must be in range (0,1) "
        assert 0 < c1 < 1, "Constant c1 must be in range (0,1) "
        assert 0 < min_alpha <= alpha, "Constant min alpha must be in range (0,alpha]"
        super().__init__(
            problem, FP_PRECISION=FP_PRECISION, library=library, cost_threshold=cost_threshold
        )
        self.momentum = momentum
        self.current_step = self.math_library.zeros_like(self.problem.initial_solution)
        self.alpha = alpha
        self.rho = rho
        self.c1 = c1
        self.min_alpha = min_alpha

    def compute_search_direction(self, current_solution, binding_set, gradient):
        self.current_step = self.momentum * self.current_step + (1 - self.momentum) * (-gradient)
        return self.current_step

    def compute_step_length(self, current_solution, search_direction, gradient):
        return backtracking_step_length(
            self.problem.cost,
            self.project_onto_box_constraints,
            current_solution,
            self.alpha,
            self.rho,
            search_direction,
            gradient,
            self.c1,
            self.min_alpha,
        )


class GaussNewtonMethod(NewtonMethod):
    """
    Solve equation equation 10.23
    """

    def __init__(
        self,
        problem,
        alpha=1.0,
        rho=0.9,
        c1=1e-4,
        min_alpha=1e-10,
        FP_PRECISION=1e-7,
        library=np,
        cost_threshold=None,
        batch_linesearch=False,
    ):
        assert alpha > 0, "Alpha must be positive"
        assert 0 < rho < 1, "Constant rho must be in range <0,1> "
        assert 0 < c1 < 1, "Constant c1 must be in range <0,1> "
        assert 0 < min_alpha < alpha, "Constant min alpha must be in range <0,alpha"
        super().__init__(
            problem,
            alpha=alpha,
            rho=rho,
            c1=c1,
            min_alpha=min_alpha,
            FP_PRECISION=FP_PRECISION,
            library=library,
            cost_threshold=cost_threshold,
        )

        self.batch_linesearch = batch_linesearch

        alpha = self.alpha
        self.alpha_vals = [alpha]
        while alpha >= self.min_alpha:
            alpha = self.rho * alpha
            self.alpha_vals.append(alpha)
        if self.math_library == np:
            self.alpha_vals = np.array(self.alpha_vals)
        elif self.math_library == torch:
            self.alpha_vals = torch.tensor(self.alpha_vals)

    def compute_search_direction(self, current_solution, binding_set, gradient):
        r = self.problem.residual(current_solution)
        J = self.problem.jacobian(current_solution)
        start = time.time()
        if not torch.is_tensor(current_solution):
            hessian_approximate = J.T @ J
            hessian_approximate = self.hessian_constraint_modification(
                hessian_approximate, binding_set
            )
            Jr = J.T @ r
            search = self.linear_solve(hessian_approximate, -Jr)
        else:
            search = hessian_search_direction_fast(J, r, binding_set)
        self._hessTime += time.time() - start
        return search

    def compute_step_length(self, current_solution, search_direction, gradient):
        start = time.time()
        if self.batch_linesearch:
            start = time.time()
            step_length = batch_step_length(
                self.problem.tensor_cost,
                self.project_onto_box_constraints,
                current_solution,
                self.alpha_vals,
                search_direction,
                self.problem.cost_gradient(current_solution),
                self.c1,
                self.math_library,
            )
            self._batchtrackTime += time.time() - start
        else:
            step_length = backtracking_step_length(
                self.problem.cost,
                self.project_onto_box_constraints,
                current_solution,
                self.alpha,
                self.rho,
                search_direction,
                gradient,
                self.c1,
                self.min_alpha,
            )
        self._avg_step_length += step_length
        if torch.is_tensor(current_solution):
            self._avg_step_calls += torch.argmin(
                (self.alpha_vals[self.alpha_vals <= self.alpha] - step_length).abs()
            ).item()
        self._avg_step_idx += 1
        self._backtrackTime += time.time() - start
        return step_length


class GeneralizedGaussNewton(GaussNewtonMethod):
    """
    Generalized Gauss Newton formulation from Diehl:
    https://cdn.syscop.de/publications/Messerer2021a.pdf
    """

    def __init__(
        self,
        problem,
        alpha=1.0,
        rho=0.9,
        c1=1e-4,
        min_alpha=1e-10,
        FP_PRECISION=1e-7,
        cost_threshold=None,
    ):
        super().__init__(
            problem,
            alpha,
            rho,
            c1,
            min_alpha,
            FP_PRECISION=FP_PRECISION,
            cost_threshold=cost_threshold,
        )

    def compute_search_direction(self, current_solution, binding_set, gradient):
        # TODO: Should be able to speed up the computation of df and J through better
        # decomposition or reuse
        start = time.time()
        df = gradient
        J = self.problem.jacobian(current_solution)
        r = self.problem.residual(current_solution)
        B = self.problem.outer_convex_hessian(r)
        hessian_approximate = J.T @ B @ J
        hessian_approximate = self.hessian_constraint_modification(hessian_approximate, binding_set)
        search = self.linear_solve(hessian_approximate, -df, library=self.math_library)
        self._hessTime += time.time() - start
        return search


@torch.jit.script
def hessian_search_direction_fast(J, r, binding_set):
    # type: (Tensor, Tensor, Tensor) -> Tensor  # noqa: F821
    hessian_approximate = J.T @ J
    if len(binding_set) > 0:
        hessian_approximate[binding_set, :] = 0
        hessian_approximate[:, binding_set] = 0
        hessian_approximate[binding_set, binding_set] = 1
    Jr = J.T @ r
    search = linear_solve_cholesky_adaptive_fast(hessian_approximate, -Jr, 1e-3)
    return search


# Different algorithms for searching for optimal step length
def batch_step_length(
    f,
    project_func,
    current_solution,
    alpha_vals,
    search_direction,
    gradient,
    c1=1e-4,
    library=np,
):
    satisfied = armijo_condition_batch(
        f, project_func, current_solution, alpha_vals, gradient, search_direction, c1, library
    )
    return alpha_vals[satisfied][0]


def backtracking_step_length(
    f,
    project_func,
    current_solution,
    alpha,
    rho,
    search_direction,
    gradient,
    c1=1e-4,
    min_alpha=1e-10,
):
    """
    Algorithm 3.1
    """
    x = current_solution
    p = search_direction
    if alpha <= min_alpha:
        return alpha
    while not armijo_condition_satisfied(f, project_func, x, alpha, gradient, p, c1):
        alpha = rho * alpha
        if alpha <= min_alpha:
            return alpha
    return alpha


# TODO update to use projection like backtracking
def scipy_exact_line_search(
    cost_func,
    current_solution,
    alpha,
    search_direction,
    gradient_func,
    gradient,
    c1=1e-4,
    c2=0.9,
    max_iters=100,
):
    def g_t(x):
        return gradient_func(x).T

    alpha, _, _, _, _, _ = scipy.optimize.line_search(
        cost_func,
        gradient_func,
        current_solution,
        search_direction,
        amax=alpha,
        gfk=gradient,
        c1=c1,
        c2=c2,
        maxiter=max_iters,
    )
    return alpha


# TODO update to use projection like backtracking
def wolfe_line_search(phi, phi_d, alpha_max, c1=1e-4, c2=0.9, max_iters=100):
    """
    Algorithm 3.5
    A line search algorithm for the wolfe conditions
    """

    def choose_alpha(x, y):
        return x + (y - x) / 2

    alpha_previous = 1
    alpha_current = choose_alpha(alpha_previous, alpha_max)
    alpha = alpha_current
    first_iteration = True

    phi_alpha_previous = phi(alpha_previous)
    phi_alpha_current = phi(alpha_current)
    phi_zero = phi(0)
    phi_d_zero = phi_d(0)

    iter_idx = 0
    while iter_idx < max_iters:
        iter_idx += 1

        if (phi_alpha_current > phi_zero + c1 * alpha_current * phi_d_zero) or (
            phi_alpha_current >= phi_alpha_previous and not first_iteration
        ):
            return zoom(alpha_previous, alpha_current, phi, phi_d, c1, c2, choose_alpha)

        phi_d_alpha_current = phi_d(alpha_current)

        if abs(phi_d_alpha_current) <= -c2 * phi_d_zero:
            return alpha_current

        # (ADAM) I added this not first iter check since alpha is undefined there
        if phi_d_alpha_current >= 0 and not first_iteration:
            return zoom(alpha, alpha_previous, phi, phi_d, c1, c2, choose_alpha)

        # save previous iteration
        phi_alpha_previous = phi_alpha_current
        alpha_previous = alpha_current

        alpha = choose_alpha(alpha_current, alpha_max)

        first_iteration = False


def zoom(alpha_lo, alpha_hi, phi, phi_d, c1, c2, choose_alpha, max_iters=100):
    """
    Algorithm 3.6
    """
    phi_d_zero = phi_d(0)

    iter_idx = 0
    while iter_idx < max_iters:
        iter_idx += 1

        alpha_j = choose_alpha(alpha_lo, alpha_hi)
        phi_alpha_j = phi(alpha_j)

        if (phi_alpha_j > phi(0) + c1 * alpha_j * phi_d_zero) or phi(alpha_j) >= phi(alpha_lo):
            alpha_hi = alpha_j
            continue

        phi_d_alpha_j = phi_d(alpha_j)

        if abs(phi_d_alpha_j) <= -c2 * phi_d_zero:
            return alpha_j

        if phi_d_alpha_j * (alpha_hi - alpha_lo) >= 0:
            alpha_hi = alpha_lo

        alpha_lo = alpha_j

    return alpha_lo  # TODO I don't know what you want to return if it timed out
