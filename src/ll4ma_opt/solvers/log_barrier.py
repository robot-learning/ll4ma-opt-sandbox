#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file implements a simplified version of the log barrier interior point algorithm and phase I
infeasible solver from Boyd and Vandenberghe's Convex Optimization chapter 11.

The main difference is it does not currently support linear equality constrains, but does support
bound constraints.

@author: Tucker Hermans <tucker.hermans@utah.edu>
"""
# Standard Library
import time

# Third Party
import numpy as np
import torch

# ll4ma
from ll4ma_opt.problems.problem import Problem, Constraint
from ll4ma_opt.solvers import GaussNewtonMethod, GradientDescent, NewtonMethod
from ll4ma_opt.solvers.cem import CEM
from ll4ma_opt.solvers.mppi import MPPI
from ll4ma_opt.solvers.optimization_solver import OptimizationSolver, SolverReturn
from ll4ma_opt.solvers.quasi_newton import BFGSMethod, SR1Method
from ll4ma_opt.solvers.opt_conv_criteria import (
    _LOG_CONSTRAINT_THRESH,
    _PHASE_I_FEASIBLE,
    _PHASE_I_COST_NEG,
    _NO_FEASIBLE_FOUND,
    _COST_THRESH,
)


_OPTIMIZERS_DICT = {
    "newton": NewtonMethod,
    "gradient descent": GradientDescent,
    "BFGS": BFGSMethod,
    "SR1": SR1Method,
    "gauss newton": GaussNewtonMethod,
    "cem": CEM,
    "mppi": MPPI,
}


class LogBarrierProblemWrapper(Problem):
    """
    Takes an inequality constrained problem and generates a bound-constrained problem with
    log barrier costs for all inequality constraints

    Used by the LogBarrierSolver class
    """

    def __init__(self, problem, barrier_multiplier):
        self.initial_solution = problem.initial_solution.copy()
        self.size = problem.size
        self.problem = problem
        # copy data over before init
        super().__init__()
        self.min_bounds = problem.min_bounds
        self.max_bounds = problem.max_bounds

        self.barrier_multiplier = barrier_multiplier

        # NOTE: Ignores negative values in np.log computation
        np.seterr(invalid="ignore")

    def cost(self, x):
        cost = self.problem.cost(x)
        # cost = original_cost + \sum_{i} mu * ln(c_i(x))

        cost = self.barrier_multiplier * cost + np.sum(
            [-np.log(-constraint.error(x)) for constraint in self.problem.inequality_constraints]
        )

        return cost

    def cost_gradient(self, x):
        grad = self.barrier_multiplier * self.problem.cost_gradient(x)

        for constraint in self.problem.inequality_constraints:
            error = constraint.error(x)
            error_gradients = constraint.error_gradient(x)
            grad = grad - (error_gradients / error)

        return grad

    def cost_hessian(self, x):
        hessian = self.barrier_multiplier * self.problem.cost_hessian(x)

        for constraint in self.problem.inequality_constraints:
            error = constraint.error(x)
            error_gradient = constraint.error_gradient(x)

            hessian = hessian + 1.0 / (error**2) * (
                error_gradient @ error_gradient.T - error * constraint.error_hessian(x)
            )

        return hessian


class PhaseIConstraintWrapper(Constraint):
    """
    Small wrapper to update the error and gradient functions analytically for constraints converted
    with slack variables for Phase I optimization
    """

    def __init__(self, constraint, original_size, slack_idx):
        super().__init__()
        self.original_constraint = constraint
        self.original_size = original_size
        self.slack_idx = slack_idx

    def error(self, x):
        """
        Constraint g(x) <= 0 converts to the form g(x) - s <= 0
        """
        if np.isnan(x).any():
            print(f"Error, {x} has nan values in cost")
        return self.original_constraint.error(x[: self.original_size]) - x[self.slack_idx]

    def error_gradient(self, x):
        """
        Constraint g(x) <= 0 converts to the form g(x) - s <= 0
        """
        if np.isnan(x).any():
            print(f"Error, {x} has nan values in gradient")
        grad = np.zeros_like(x)
        grad[: self.original_size] = (
            self.original_constraint.error_gradient(x[: self.original_size]) - x[self.slack_idx]
        )
        grad[self.slack_idx] = np.array([[-1.0]])
        return grad

    def error_hessian(self, x):
        hessian = np.zeros((len(x), len(x)))
        hessian[: self.original_size, : self.original_size] = (
            self.original_constraint.error_hessian(x[: self.original_size])
        )
        # hessian[-1,-1] = 0.001
        return hessian


class PhaseIProblemConverter(Problem):
    """
    Converts an inequality and bound constrained problem into a new problem with simple cost and
    slack variable to find a feasible initial solution, following the approach from
    Boyd and Vandenberghe, Chapter 11.4.1 "Basic phase I method"

    If the original problem has inequality constraints g_i(x) <= 0 and cost f(x)
    then the new problem has constraints of the form g_i(x) - s <= 0 and cost s

    Note: there is only a single slack variable s not a separate one for each constraint
    """

    def __init__(self, problem, FP_PRECISION=1e-7, phase_I_slack_init_epsilon=10.0):
        self.original_size = int(problem.size())
        self.size = lambda: self.original_size + 1
        self.slack_idx = self.original_size

        # Initialze x variables to the current values
        self.initial_solution = np.ones((self.size(), 1))
        self.initial_solution[: self.original_size] = problem.initial_solution[:]

        # Initialize slack variable to be equal to max of constraints under the initial x + epsilon
        self.initial_solution[self.slack_idx] = (
            np.max(
                [ineq.error(problem.initial_solution) for ineq in problem.inequality_constraints]
            )
            + phase_I_slack_init_epsilon
        )
        print(f"Initializing slack variable to value {self.initial_solution[self.slack_idx]}")

        self.original_problem = problem
        # copy data over before init
        super().__init__()

        # Copy over original problem bounds
        self.min_bounds[: self.original_size] = problem.min_bounds[:]
        self.max_bounds[: self.original_size] = problem.max_bounds[:]

        # Copy previous inequality constraints
        # Convert inequality constraints to subtract the slack variable
        self.inequality_constraints = [
            PhaseIConstraintWrapper(ineq, self.original_size, self.slack_idx)
            for ineq in problem.inequality_constraints
        ]

    def cost(self, x):
        """
        New cost is simply the cost of the slack variable

        x - decision vector with slack variable
        """
        return x[self.slack_idx][0]

    def cost_gradient(self, x):
        """
        New cost gradient is simply 1 for the slack variable and 0 elsewhere

        x - decision vector with slack variable
        """
        f_grad = np.zeros_like(x)
        f_grad[self.slack_idx] = 1.0
        return f_grad


class PhaseISumProblemConverter(Problem):
    """
    Converts an inequality and bound constrained problem into a new problem with simple cost and
    slack variables to find a feasible initial solution, following the approach from
    Boyd and Vandenberghe, Chapter 11.4.1 "Sum of infeasibilities"

    If the original problem has inequality constraints g_i(x) <= 0 and cost f(x)
    then the new problem has constraints of the form g_i(x) - s_i <= 0 and cost S^T 1

    Note: there is a vector of slack variables s, one for each constraint
    """

    def __init__(
        self,
        problem,
        FP_PRECISION=1e-7,
    ):
        self.original_size = int(problem.size())
        num_constraints = len(problem.inequality_constraints)
        self.size = lambda: self.original_size + num_constraints

        # Initialze x variables to the current values
        self.initial_solution = np.ones((self.size(), 1))
        self.initial_solution[: self.original_size] = problem.initial_solution[:]

        # Initialize slack variable to be equal to max of constraints under the initial x + epsilon
        self.initial_solution[self.original_size :] = (
            np.array(
                [ineq.error(problem.initial_solution) for ineq in problem.inequality_constraints]
            ).reshape((num_constraints, 1))
            + 1.0
        )

        print(
            f"Initializing slack variables to values {self.initial_solution[self.original_size :]}"
        )

        self.original_problem = problem
        # copy data over before init
        super().__init__()

        # Copy over original problem bounds
        self.min_bounds[: self.original_size] = problem.min_bounds[:]
        self.max_bounds[: self.original_size] = problem.max_bounds[:]
        self.min_bounds[self.original_size :] = np.zeros_like(
            self.initial_solution[self.original_size :]
        )

        # Copy previous inequality constraints
        # Convert inequality constraints to subtract the slack variable
        self.inequality_constraints = [
            Constraint(
                lambda x: ineq.tensor_error(x[: self.original_size]) - x[self.original_size + i]
            )
            for i, ineq in enumerate(problem.inequality_constraints)
        ]

    def cost(self, x):
        """
        New cost is simply the cost of the slack variable

        x - decision vector with slack variable
        """
        return np.sum(x[self.original_size :])

    def cost_gradient(self, x):
        """
        New cost gradient is simply 1 for all slack variables and 0 elsewhere

        x - decision vector with slack variable
        """
        f_grad = np.zeros_like(x)
        f_grad[self.original_size :] = 1.0
        return f_grad


class LogBarrierSolver(OptimizationSolver):
    """
    Interior point method / log barrier solver from Boyd and Vandenberghe chapter 11.

    Can solve problems of the form min f(x) s.t. g(x) <= 0; x_min <= x <= x_max
    """

    def __init__(
        self,
        problem,
        internal_optimizer="newton",
        optimizer_args={"alpha": 1, "rho": 0.9},
        FP_PRECISION=1e-7,
        init_barrier_multiplier=1.0,
        update_ratio=20.0,
        phase_I_update_ratio=2.0,
        use_phase_I_sum=True,
        cost_threshold=None,
    ):
        assert update_ratio > 1, "Update ratio must be larger than one"
        super().__init__(problem, FP_PRECISION, cost_threshold=cost_threshold)
        self.optimizer_args = optimizer_args
        self.internal_optimizer = internal_optimizer
        self.init_barrier_multiplier = init_barrier_multiplier
        self.update_ratio = update_ratio
        assert phase_I_update_ratio > 1, "phase I Update ratio must be larger than one"
        self.phase_I_update_ratio = phase_I_update_ratio
        self.use_phase_I_sum = use_phase_I_sum

        # Create wrapped problem
        self.log_problem = LogBarrierProblemWrapper(self.problem, self.init_barrier_multiplier)

        # Create internal optimizer
        self.optimizer = _OPTIMIZERS_DICT[self.internal_optimizer](
            problem=self.log_problem, FP_PRECISION=self.FP_PRECISION, **self.optimizer_args
        )

    def optimize(self, x0=None, max_iterations=100, solving_phase_I=False):
        """
        Creates a wrapper problem at each iteration that decreases the barrier multiplier until
        convergence
        """
        assert max_iterations >= 1, "max_iterates must be ≥ 1"
        assert isinstance(max_iterations, int), "max_iterates must be an int"
        m = len(self.problem.inequality_constraints)

        start = time.time()

        internal_iterates = []
        converged = False
        criteria = {}

        if x0 is None:
            x_iter = self.problem.initial_solution[:]
        else:
            x_iter = x0
        internal_iterates = []

        if not solving_phase_I and not self.test_feasibility(x_iter):
            x_iter, feasible_found = self.find_feasible_solution(x0)

            if not feasible_found:
                # Unable to find feasible solution
                cost = self.problem.cost(x_iter)
                if torch.is_tensor(cost):
                    cost = cost.item()

                duration = time.time() - start

                return SolverReturn(
                    solution=x_iter,
                    cost=cost,
                    converged=converged,
                    convergence_criteria={_NO_FEASIBLE_FOUND: ""},
                    time=duration,
                    iterates=np.array([x_iter]),
                )

        for i in range(max_iterations):
            res = self.optimizer.optimize(x_iter, max_iterations=max_iterations)

            # Update current iterate and internal iterates
            x_iter = res.solution
            internal_iterates.append(res.iterates)

            # Convergence criteria from Boyd and Vandenberghe's Convex Optimization chapter 11
            # Stop when number of constraints divided by the multiplier gets under convergence
            # precision (epsilon)
            log_constraint_score = m / self.log_problem.barrier_multiplier
            if log_constraint_score < self.FP_PRECISION:
                converged = True
                criteria = {_LOG_CONSTRAINT_THRESH: log_constraint_score}
                break

            # Test using the cost_threshold
            cur_cost = float(self.problem.cost(x_iter))
            if (
                not solving_phase_I
                and self.cost_threshold is not None
                and cur_cost < self.cost_threshold
            ):
                converged = True
                criteria[_COST_THRESH] = (self.cost_threshold, cur_cost)
                break

            # Stop solving a phase I (feasibility) problem once the slack variables becomes negative
            if solving_phase_I and self.test_feasibility(x_iter):
                if self.use_phase_I_sum and x_iter[self.problem.original_size :].all() <= 0:
                    converged = True
                    criteria = {_PHASE_I_FEASIBLE: x_iter[self.problem.original_size :]}
                    break
                elif not self.use_phase_I_sum and x_iter[self.problem.original_size] < 0:
                    converged = True
                    criteria = {_PHASE_I_COST_NEG: x_iter[self.problem.original_size]}
                    break

            # Decrease log barrier multiplier for next iteration
            self.log_problem.barrier_multiplier *= self.update_ratio
            print(
                f"Outer iteration {i} converged, setting log-barrier multiplier to"
                f" {self.log_problem.barrier_multiplier}"
            )

            # Output slack variable value for debugging
            if solving_phase_I:
                print(f"Slack variables have value: {x_iter[self.problem.original_size :]}")

        cost = self.problem.cost(x_iter)
        if torch.is_tensor(cost):
            cost = cost.item()
        duration = time.time() - start
        return SolverReturn(
            solution=x_iter,
            cost=cost,
            converged=converged,
            convergence_criteria=criteria,
            time=duration,
            iterates=np.vstack(internal_iterates),
        )

    def test_feasibility(self, x_test):
        """
        Finds out if a solution is striclty feasible
        """
        return (
            len(
                [
                    1
                    for constraint in self.problem.inequality_constraints
                    if constraint.error(x_test) > 0
                ]
            )
            < 1
        )

    def find_feasible_solution(self, x0):
        """
        Solve a simple constrained problem to generate a feasible solution
        """

        # Create a phase_I problem to solve
        if self.use_phase_I_sum:
            phase_I_problem = PhaseISumProblemConverter(self.problem)
        else:
            phase_I_problem = PhaseIProblemConverter(self.problem)
        phase_I_solver = LogBarrierSolver(
            phase_I_problem,
            "newton",
            update_ratio=self.phase_I_update_ratio,
            use_phase_I_sum=self.use_phase_I_sum,
        )

        phase_I_res = phase_I_solver.optimize(
            phase_I_problem.initial_solution, solving_phase_I=True
        )

        feasible_x0 = phase_I_res.solution[: phase_I_problem.original_size]
        print(f"Found new initial solution {feasible_x0}")

        return feasible_x0, self.test_feasibility(feasible_x0)
