# Third Party
import numpy as np
import torch
from torch import Tensor  # noqa: F401
import scipy


def adaptive_cholesky_factorization_fast(A, beta=1e-3, max_iters=1000):
    retval = _adaptive_cholesky_factorization_fast(A, beta, max_iters)
    if retval.shape != A.shape:
        return None
    return retval


@torch.jit.script
def _adaptive_cholesky_factorization_fast(A, beta, max_iters):
    # type: (Tensor, float, int) -> Tensor
    n = A.shape[0]
    min_diag = 0.1
    min_diag = min(min_diag, A[torch.arange(n), torch.arange(n)].min())

    tau = 0.0 if min_diag > 0 else -min_diag + beta
    eyemat = torch.eye(n, device=A.device)
    for i in range(max_iters):
        L, info = torch.linalg.cholesky_ex(A + tau * eyemat)
        if info == 0:
            return L
        tau = max(2.0 * tau, beta)
    return torch.zeros((1,), device=A.device)


def adaptive_cholesky_factorization(A, beta=1e-3, max_iters=1000, library=np):
    """
    Nocedal and Wright Algorithm 3.3: Cholesky with Added Multiple of the Identity

    A - matrix to decompose
    beta - regularization offset for the diagonal elements
    max_iters - maximum number of iterations to attempt to make A positive definite

    returns - Lower triangular matrix, L, from the Cholesky decomp of A with Tikinov regularization
              or None if the decompistion fails to create a positive definite version of A within
              max_iters iterations
    """
    min_diag = 0.1
    n = A.shape[0]
    for i in range(n):
        if A[i, i] < min_diag:
            min_diag = A[i, i]

    if min_diag > 0:
        tau = 0
    else:
        tau = -min_diag + beta

    for i in range(max_iters):
        try:
            if library is np:
                L, low = scipy.linalg.cho_factor(A + tau * library.identity(n))
            else:
                L = library.linalg.cholesky(
                    A + tau * library.eye(n, dtype=A.dtype, device=A.device)
                )
                low = None
            return L, low
        except library.linalg.LinAlgError:
            tau = max(2 * tau, beta)
    return None


def QR_factorization(A):
    return scipy.linalg.qr(A, mode="full", pivoting="true")


def get_nonsingular_YZ(A):
    m = A.shape[0]
    Q, R, P = QR_factorization(A.T)
    Y = Q[:, :m]
    Z = Q[:, m:]
    return Y, Z


def compute_inverse_cholesky(A, beta=0.1, library=np):
    """
    Computes inverse of a square matrix using (adaptive) cholesky decomposition.
    """
    if A.ndim < 2:
        print(A)
        print(A.ndim)
    try:
        L = library.linalg.cholesky(A)
        if L.ndim < 2:
            print("A=", A)
            print(A.ndim)
            print("L=", L)
            print(L.ndim)

    except library.linalg.LinAlgError:
        L, low = adaptive_cholesky_factorization(A, beta=beta, library=library)
        if L is None:
            print(f"Matrix A has form {A}")
            raise Exception("Failed to make matrix A positive definite within max_iters")
    L_inv = library.linalg.inv(L)
    return L_inv.T @ L_inv


def compute_inverse_cholesky_fast(A, beta=0.1, library=np):
    # assert A.ndim >= 2

    tensor_A = A if library is torch else torch.tensor(A)
    tensor_retval = _compute_inverse_cholesky_fast(tensor_A, beta, 10000)
    if tensor_retval.shape[0] == 1:
        return None
    retval = tensor_retval if library is torch else tensor_retval.numpy()
    return retval


@torch.jit.script
def _compute_inverse_cholesky_fast(A, beta, max_iters):
    # type: (Tensor, float, int) -> Tensor
    L, info = torch.linalg.cholesky_ex(A)
    if info > 0:
        L = _adaptive_cholesky_factorization_fast(A, beta, max_iters)
        if L.shape[0] == 1:
            return L

    L_inv = torch.linalg.inv(L)
    return L_inv.T @ L_inv


def linear_solve_cholesky_adaptive(A, b, beta=1e-3, library=np):
    """
    Takes a matrix A and vector b and returns the solution x to
    Ax=b using Cholesky decomposition

    A - linear equality matrix
    b - linear equality vector
    beta - damping term for the adaptive Cholesky solver
    library - math library being used (np or torch)

    returns - solution x to Ax=b
    """

    L, low = adaptive_cholesky_factorization(A, beta=beta, library=library)

    if library is np:
        x = scipy.linalg.cho_solve((L, low), b)
    elif library is torch:
        x = library.cholesky_solve(b, L)
    else:
        x = library.linalg.cho_solve(b, L)

    return x


@torch.jit.script
def linear_solve_cholesky_adaptive_fast(A, b, beta):
    # type: (Tensor, Tensor, float) -> Tensor
    """
    Takes a matrix A and vector b and returns the solution x to
    Ax=b using Cholesky decomposition

    A - linear equality matrix
    b - linear equality vector
    beta - damping term for the adaptive Cholesky solver

    returns - solution x to Ax=b
    """

    L = _adaptive_cholesky_factorization_fast(A, beta, 1000)

    x = torch.cholesky_solve(b, L)

    return x


def linear_solve_lu_adaptive(A, b, beta=1e-3, library=np):
    """
    Takes a matrix A and vector b and returns the solution x to
    Ax=b using LU decomposition

    A - linear equality matrix
    b - linear equality vector
    beta - damping term for the adaptive Cholesky solver
    library - math library being used (np or torch)

    returns - solution x to Ax=b
    """

    # scipy and pytorch linalg calls match for these functions
    if library is np:
        lib = scipy
    else:
        lib = library

    lu_fac = lib.linalg.lu_factor(A)
    x = lib.linalg.lu_solve(lu_fac, b)

    return x


def linear_solve_ldl_adaptive(A, b, beta=1e-3, library=np):
    raise NotImplementedError("Placeholder function to be implemented soon")
