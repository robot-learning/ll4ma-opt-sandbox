#!/usr/bin/env python3
"""
Implementation of Model Predictive Path Integral control (MPPI).

Author: Adam Conkey
"""
# Standard Library
import time

# Third Party
import numpy as np
import torch
from ll4ma_util import ui_util
from tqdm import tqdm

# ll4ma
from ll4ma_opt.solvers import OptimizationSolver, SolverReturn
from ll4ma_opt.solvers.opt_conv_criteria import _COST_THRESH


class MPPI(OptimizationSolver):
    """
    Model Predictive Path Integral Control (MPPI). Implements the high-level steps of:

        1. Start with a nominal solution
        2. Generate random perturbations to the nominal solution
        3. Do rollouts with your simulator (dynamics function)
        4. Evaluate the cost of your rollouts
        5. Compute weighted disturbance vectors, where low cost disturbances
           are given a higher weight than high cost disturbances.
        6. Update the nominal solution by summing in the weighted disturbances
        7. Repeat steps 2-6 until a low-cost solution is found

    See [1] for the basic math and a cool application. See [2] for a very clear tutorial
    video on MPPI and [3] for their Python implementation. See [4] for another reference
    implementation.

    ---
    [1] Williams, Grady, et al. "Aggressive driving with model predictive path integral control."
        ICRA 2016. https://ieeexplore.ieee.org/abstract/document/7487277

    [2] https://youtu.be/19QLyMuQ_BE

    [3] https://github.com/SensorsINI/CartPoleSimulation/blob/master/Controllers/controller_mppi.py

    [4] https://github.com/NVlabs/storm/blob/main/storm_kit/mpc/control/mppi.py

    """

    def __init__(
        self,
        problem,
        sampler,
        alpha=0.001,
        n_samples=100,
        cost_epsilon=1e-10,
        FP_PRECISION=1e-7,
        library=np,
    ):
        """
        Args:
            std (float): Standard deviation for sampling distribution
            sampler (Sampler): Sampler class to generate solution samples from
            alpha (float): Temperature constant on exponentiated (negative) costs. Must be in
                           positive. Lower values result in more sensitivity to cost. A
                           reasonable starting range is [0.01-100] in orders of magnitude.
            n_samples (int): Number of samples to generate at each iteration.
            cost_epsilon (float): Optimization is converged if the cost falls below this value
        """
        if n_samples < 2:
            raise ValueError("Number of samples (n_samples) must be at least 2.")

        super().__init__(problem, FP_PRECISION=FP_PRECISION, library=library)
        self.sampler = sampler
        self.alpha = alpha
        self.n_samples = n_samples
        self.cost_epsilon = cost_epsilon

    def optimize(self, x0=None, max_iterations=100, verbose=True):
        """
        Args:
            x0 (ndarray): Initial distribution mean, shape (n_dims,), defaults to all zeros.
            max_iterations (int): Maximum number of iterations to run
            verbose (bool): Print text and progress bar when true
        """
        if x0 is not None and max(x0.shape) != self.problem.size():
            raise ValueError(f"Expected x0 to have {self.problem.size()} dims but got {len(x0)}")
        x0 = np.zeros(self.problem.size()) if x0 is None else x0

        if verbose:
            print(f"\nRunning MPPI optimization for {max_iterations} iterations...")
            pbar = tqdm(total=max_iterations, desc="MPPI")

        with torch.no_grad():
            u = torch.tensor(x0).squeeze().unsqueeze(0)

            iterates = [u.clone().numpy().squeeze()]
            sample_iterates = [(u + self.sampler.sample(self.n_samples)).numpy()]
            converged = False
            criteria = {}
            start = time.time()

            for i in range(max_iterations):
                # Generate samples for action perturbations
                delta_u = self.sampler.sample(self.n_samples)
                samples = u + delta_u  # (B, N)
                costs = self.problem.tensor_batch_cost(samples).flatten()  # (B,)

                iterates.append(u.clone().numpy().squeeze())  # clone is necessary
                sample_iterates.append(samples.numpy())

                # Compute weighted sampled disturbances based on their costs. Note we
                # subtract the min cost from costs for numerical stability, it guarantees
                # the normalization constant will be >= 1 even if all samples have really
                # high costs.
                exp_neg_cost = torch.exp(-1.0 / self.alpha * (costs - torch.min(costs))).unsqueeze(
                    -1
                )
                weighted_delta_u = exp_neg_cost * delta_u / exp_neg_cost.sum()  # (B, N)
                weighted_delta_u = weighted_delta_u.sum(dim=0).unsqueeze(0)  # (1, N)

                if torch.any(torch.isnan(weighted_delta_u)):
                    ui_util.print_error_exit(
                        "Encountered NaN in MPPI update. Check cost "
                        "function values, they're probably too large."
                    )

                # Update the nominal action vector with weighted disturbances
                u += weighted_delta_u.clone()
                u = self.problem.clamp(u)

                cost = self.problem.tensor_batch_cost(u).item()
                if cost < self.cost_epsilon:
                    converged = True
                    criteria = {_COST_THRESH: (self.cost_threshold, cost)}
                    iterates.append(u.clone().numpy().squeeze())
                    sample_iterates.append((u + self.sampler.sample(self.n_samples)).numpy())
                    break
                if verbose:
                    pbar.update(1)
                self.sampler.step()

        if verbose:
            pbar.close()
        duration = time.time() - start
        u = u.T.numpy()
        iterates = np.expand_dims(np.array(iterates), -1)
        return SolverReturn(
            solution=u,
            cost=float(cost),
            converged=bool(converged),
            convergence_criteria=criteria,
            time=duration,
            iterates=iterates,
            sample_iterates=np.array(sample_iterates),
        )


if __name__ == "__main__":
    # Standard Library
    import argparse

    # ll4ma
    from ll4ma_opt.problems import DoubleIntegrator, DubinsCar
    from ll4ma_opt.solvers.samplers import Sampler

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--problem", type=str, choices=["dubins", "di"], default="dubins")
    args = parser.parse_args()

    if args.problem == "dubins":
        problem = DubinsCar()
        x0 = np.tile(
            [0, problem.max_bounds[1].item(), problem.max_bounds[2].item()],
            problem.horizon,
        )
        std = 0.1
    elif args.problem == "di":
        problem = DoubleIntegrator()
        x0 = np.random.randn(problem.horizon * problem.act_size())
        std = 0.2

    sampler = Sampler(
        problem.size(),
        problem.min_bounds,
        problem.max_bounds,
    )
    solver = MPPI(problem, sampler, std, 0.01, 1000)
    result = solver.optimize(x0, max_iterations=100)
    result.display(show_solution=False)
    problem.visualize_optimization(result)
