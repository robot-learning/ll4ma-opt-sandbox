# Standard Library
from dataclasses import dataclass, field

# Third Party
import numpy as np
import torch


class OptimizationSolver(object):
    def __init__(self, problem, FP_PRECISION=1e-7, library=np, cost_threshold=None):
        self.FP_PRECISION = FP_PRECISION
        self.problem = problem
        self.math_library = library
        self.cost_threshold = cost_threshold

        self.reset_time_vars()

    def reset_time_vars(self):
        self._gradTime = 0
        self._searchTime = 0
        self._lengthTime = 0
        self._hessTime = 0
        self._batchtrackTime = 0
        self._backtrackTime = 0
        self._avg_step_length = 0.0
        self._avg_step_calls = 0.0
        self._avg_step_idx = 0

    def optimize(self, x0=None, max_iterations=100):
        """
        x0 - initialization point, if None use origin, requires self.n to be defined
        max_iterations - maximum number of optimization iterations to take
        returns  (argmin, f(argmin), converged_flag, iterates)
        """
        raise NotImplementedError("Abstract class method not implemented")

    def project_decision_variables(self, x):
        raise NotImplementedError(
            "Need to define the correct constraint variables and then define this projection"
        )

    def write_log_to_disk(self, filename):
        raise NotImplementedError("TODO: Implement this function")


@dataclass
class SolverReturn:
    """
    Return structure for solvers.

    Args:
        solution (ndarray): Vector solution for the problem
        cost (float): Cost of the solution
        converged (bool): True if solver converged to a solution, False otherwise
        convergence_criteria (dict): Dictionary of criteria that solver uses to define convergence
        iterates (ndarray): Solutions for each optimization iteration, array of shape
                            (n_iters, problem_size, 1)
        sample_iterates (ndarray): Sampled solutions for each optimization iteration. Only
                                   populated by sampling-based solvers (e.g CEM, MPPI).
                                   Has shape (n_iters, n_samples, problem_size, 1)
    """

    solution: np.ndarray = None
    cost: float = -1.0
    converged: bool = False
    convergence_criteria: dict = field(default_factory=dict)
    time: float = -1.0
    iterates: np.ndarray = None
    sample_iterates: np.ndarray = None

    def __post_init__(self):
        if self.solution is not None and not (
            isinstance(self.solution, np.ndarray) or torch.is_tensor(self.solution)
        ):
            raise ValueError(
                "'solution' should be type numpy array or torch tensor                 but is"
                f" {type(self.solution)}"
            )
        if not isinstance(self.cost, float):
            raise ValueError(f"'cost' should be type float but is {type(self.cost)}")
        if not isinstance(self.converged, bool):
            raise ValueError(f"'converged' should be type bool but is {type(self.converged)}")
        if not isinstance(self.time, float):
            raise ValueError(f"'time' should be type float but is {type(self.time)}")
        if self.iterates is not None and not (
            isinstance(self.iterates, np.ndarray) or torch.is_tensor(self.iterates)
        ):
            raise ValueError(
                "'iterates' should be type numpy array or torch tensor                     but is"
                f" {type(self.iterates)}"
            )
        if self.sample_iterates is not None and not (
            isinstance(self.sample_iterates, np.ndarray) or torch.is_tensor(self.sample_iterates)
        ):
            raise ValueError(
                f"'sample_iterates' should be type numpy array but is {type(self.sample_iterates)}"
            )

    def display(self, show_solution=True):
        print(f"Converged? {self.converged}")
        print(f"Convergence criteria? {self.convergence_criteria}")
        print(f"Cost: {self.cost:.8f}")
        print(f"Number of steps: {len(self.iterates) - 1}")
        print(f"Time: {self.time:.4f} secs")
        if show_solution:
            print("Solution:", self.solution.squeeze())
