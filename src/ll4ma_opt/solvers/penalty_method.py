#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 13:37:27 2021

@author: tabor
"""
# Standard Library
import time

# Third Party
import numpy as np
import torch

# ll4ma
from ll4ma_opt.problems.problem import Problem, TensorConstraint
from ll4ma_opt.solvers import GaussNewtonMethod, GradientDescent, NewtonMethod
from ll4ma_opt.solvers.cem import CEM
from ll4ma_opt.solvers.mppi import MPPI
from ll4ma_opt.solvers.optimization_solver import OptimizationSolver, SolverReturn
from ll4ma_opt.solvers.quasi_newton import BFGSMethod, SR1Method
from ll4ma_opt.solvers.opt_conv_criteria import (
    _STEP_THRESH,
    _EQ_CONST_VIOL,
    _INEQ_CONST_VIOL,
    _COST_THRESH,
)


_OPTIMIZERS_DICT = {
    "newton": NewtonMethod,
    "gradient descent": GradientDescent,
    "BFGS": BFGSMethod,
    "SR1": SR1Method,
    "gauss newton": GaussNewtonMethod,
    "cem": CEM,
    "mppi": MPPI,
}


def vector_similarity(a, b):
    """
    Returns a score between [-1, 1]
    If result:
    -> 0 : The vectors are not similar and are orthogonal to each other
    -> 1 : The vectors are similar or in the same direction
    ->-1 : The vectors are in oppisite directions
    Performs dot produnt on the unit vectors of a,b
    """
    a = a.reshape(1, -1)
    b = b.reshape(-1, 1)
    assert a.shape[1] == b.shape[0], "The vector lengths {} , {} do not match".format(
        a.shape, b.shape
    )
    al = np.linalg.norm(a)
    bl = np.linalg.norm(b)
    if al * bl == 0:
        return 0
    return np.dot(a / np.linalg.norm(a), b / np.linalg.norm(b))


def compare_grads(a, b):
    ac = a.reshape(-1, 1)
    bc = b.reshape(-1, 1)
    # np.set_printoptions(precision=2, floatmode='maxprec_equal')
    print("fd vs a_grads error: \n", np.hstack([ac, bc, ac - bc]))
    vecsim = vector_similarity(ac, bc)
    print("Similarity: ", np.arccos(vecsim) * 180 / np.pi, vecsim)
    print(
        "norms: ", [np.linalg.norm(ac), np.linalg.norm(bc), np.linalg.norm(ac) - np.linalg.norm(bc)]
    )


class PenaltyProblemWrapper(Problem):
    """
    Takes an arbitrary constrained problem and generates a bound-constrained problem with
    quadratic penalty terms for the original constraints.

    Used by the PenaltyMethodSolver class
    """

    def __init__(self, problem, quadratic_coefficients, active_ineq_constraints, library=np):
        self.initial_solution = problem.initial_solution
        self.size = problem.size
        self.problem = problem
        # copy data over before init
        super().__init__()
        self._min_bounds = problem.min_bounds
        self._max_bounds = problem.max_bounds

        self.quadratic_coefficients = quadratic_coefficients
        self.active_ineq_constraints = active_ineq_constraints
        self.torch_zero = torch.tensor(np.zeros((1, 1)))

        self.library = library

    def cost(self, x):
        cost = self.problem.cost(x)
        # cost = original_cost + penalty *(1/2)* constraint^2

        if isinstance(self.problem.equality_constraints, list):
            cost = cost + np.sum(
                [
                    self.quadratic_coefficients * (constraint.error(x) ** 2)
                    for constraint in self.problem.equality_constraints
                ]
            )
        elif isinstance(self.problem.equality_constraints, TensorConstraint):
            errors = self.problem.equality_constraints.error(x) ** 2
            cost = cost + self.quadratic_coefficients @ errors

        if isinstance(self.active_ineq_constraints, list):
            for index, constraint in enumerate(self.active_ineq_constraints):
                error = constraint.error(x)
                if self.inequality_contributes_to_cost(error, index):
                    cost = cost + self.quadratic_coefficients * error**2
        elif isinstance(self.active_ineq_constraints, TensorConstraint):
            errors = self.active_ineq_constraints.error(x)
            contribs = self.inequality_contributes_to_cost(error)
            cost = cost + self.quadratic_coefficients * errors[contribs] ** 2
        return cost

    def inequality_contributes_to_cost(self, error, index):
        """
        Criteria defining if an inequality constraint is added to cost
        For Penalty method that is if constraint is violated
        """
        return error > 0

    def cost_gradient(self, x):
        grad = self.problem.cost_gradient(x)
        # gradient = original_gradient + penalty * (constraint_gradient*constraint)
        if isinstance(self.problem.equality_constraints, list):
            for constraint in self.problem.equality_constraints:
                grad = grad + 2 * self.quadratic_coefficients * constraint.error_gradient(
                    x
                ) * constraint.error(x)
        elif isinstance(self.problem.equality_constraints, TensorConstraint):
            errors, egrad = self.problem.equality_constraints(x)
            grad = grad + self.quadratic_coefficients * egrad * errors

        if isinstance(self.active_ineq_constraints, list):
            for index, constraint in enumerate(self.active_ineq_constraints):
                error = constraint.error(x)
                if self.inequality_contributes_to_cost(error, index):
                    grad = grad + 2 * self.quadratic_coefficients * (
                        constraint.error_gradient(x) * error
                    )
        elif isinstance(self.active_ineq_constraints, TensorConstraint):
            errors, egrad = self.active_ineq_constraints(x)
            contribs = self.inequality_contributes_to_cost(error)
            grad = grad + 2 * self.quadratic_coefficients * egrad[contribs] * errors[contribs]

        return grad

    def cost_hessian(self, x):
        hessian = self.problem.cost_hessian(x)

        # hessian = original_hessian + penalty *
        # (constraint_hessian*constraint + constraint_gradient^2)
        for constraint in self.problem.equality_constraints:
            error = constraint.error(x)
            error_gradient = constraint.error_gradient(x)
            hessian = hessian + 2 * self.quadratic_coefficients * (
                constraint.error_hessian(x) * error + error_gradient @ error_gradient.T
            )
        for index, constraint in enumerate(self.active_ineq_constraints):
            error = constraint.error(x)
            if self.inequality_contributes_to_cost(error, index):
                error_gradient = constraint.error_gradient(x)
                hessian = hessian + 2 * self.quadratic_coefficients * (
                    constraint.error_hessian(x) * error + error_gradient @ error_gradient.T
                )
        return hessian

    def residual(self, x):
        residual = self.problem.residual(x).reshape(-1, 1)
        penalty_constant = np.sqrt(self.quadratic_coefficients)

        concat = (
            self.library.concatenate if hasattr(self.library, "concatenate") else self.library.cat
        )
        if len(self.problem.equality_constraints):
            equality_constraint_terms = penalty_constant * self.library.stack(
                [constraint.error(x) for constraint in self.problem.equality_constraints]
            )
            residual = concat((residual, equality_constraint_terms.reshape((-1, 1))))
        if len(self.active_ineq_constraints):
            terms = [
                self.library.max(constraint.error(x), 0)
                for constraint in self.active_ineq_constraints
            ]
            terms = [term[0] if isinstance(term, tuple) else term for term in terms]
            inequality_constraint_terms = penalty_constant * self.library.stack(terms)
            residual = concat((residual, inequality_constraint_terms.reshape((-1, 1))))
        return residual

    def jacobian(self, x):
        jacobian = self.problem.jacobian(x).reshape(1, -1)
        penalty_constant = np.sqrt(self.quadratic_coefficients)

        concat = (
            self.library.concatenate if hasattr(self.library, "concatenate") else self.library.cat
        )
        if len(self.problem.equality_constraints):
            equality_constraint_terms = penalty_constant * concat(
                [constraint.error_gradient(x) for constraint in self.problem.equality_constraints],
                axis=1,
            )
            jacobian = concat((jacobian, equality_constraint_terms.T))
        if len(self.active_ineq_constraints):
            inequality_constraint_terms = []
            for constraint in self.active_ineq_constraints:
                error = constraint.error(x)
                if error > 0:
                    inequality_constraint_terms.append(constraint.error_gradient(x))
                else:
                    inequality_constraint_terms.append(x * 0)
            inequality_jacobian = penalty_constant * concat(inequality_constraint_terms, axis=1)
            jacobian = concat((jacobian, inequality_jacobian.T))
        return jacobian


class AugLagrangProblemWrapper(PenaltyProblemWrapper):
    """
    Takes an arbitrary constrained problem and generates a bound-constrained problem with
    the augmented Lagrangian penalty terms for the original constraints.
    """

    def __init__(
        self,
        problem,
        quadratic_coefficients,
        active_ineq_constraints,
        ineq_l_mults,
        eq_l_mults,
    ):
        """
        Extends PenaltyMethodWrapper by adding a Lagrangian cost term that creates
        the augmented Lagrangian problem
        """
        super().__init__(problem, quadratic_coefficients, active_ineq_constraints)

        # Keep track of current Lagrangian coefficients
        self.ineq_l_mults = ineq_l_mults
        self.eq_l_mults = eq_l_mults

    def inequality_contributes_to_cost(self, error, index):
        """
        Criteria defining if an inequality constraint is added to cost
        For Augmented Lagrangian method that is if constraint is violated or multiplier is positive
        This makes the function smoother as inequality constraints slowly decay over outer iteration
        loops instead of dropping off to 0 immediately

        https://arxiv.org/pdf/1412.4329.pdf
        Equation 6 and 7
        """
        return error > 0 or self.ineq_l_mults[index] > 0

    def cost(self, x):
        """
        Get augmented Lagrangian penalized cost for decision variable x
        """
        # Get initial problem cost and quadratic penalty from parent class
        penalty_cost = super().cost(x)

        # Add the Lagrangian penalty for equality constraints
        al_cost = penalty_cost + np.sum(
            [
                l_mult * c.error(x)
                for l_mult, c in zip(self.eq_l_mults, self.problem.equality_constraints)
            ]
        )

        # Add the Lagrangian penalty for inequality constraints

        for index, (l_mult, constraint) in enumerate(
            zip(self.ineq_l_mults, self.active_ineq_constraints)
        ):
            error = constraint.error(x)
            if self.inequality_contributes_to_cost(error, index):
                al_cost = al_cost + l_mult * constraint.error(x)

        return al_cost

    def cost_gradient(self, x):
        """
        Get the gradient of the augmented Lagrangian cost for decision variable x
        """
        # Get initial problem and quadratic penalty gradient parent class
        grad = super().cost_gradient(x)

        # Add Lagrangian gradient for the constraints
        for l_mult, constraint in zip(self.eq_l_mults, self.problem.equality_constraints):
            grad = grad + l_mult * constraint.error_gradient(x)

        for index, (l_mult, constraint) in enumerate(
            zip(self.ineq_l_mults, self.active_ineq_constraints)
        ):
            error = constraint.error(x)
            if self.inequality_contributes_to_cost(error, index):
                grad = grad + l_mult * constraint.error_gradient(x)

        return grad

    def cost_hessian(self, x):
        """
        Get the Hessian of the augmented Lagrangian cost for decision variable x
        """
        # Get initial problem and quadratic penalty gradient parent class
        hessian = super().cost_hessian(x)

        # Add Lagrangian Hessian for the constraints
        for l_mult, constraint in zip(self.eq_l_mults, self.problem.equality_constraints):
            hessian = hessian + l_mult * constraint.error_hessian(x)
        for index, (l_mult, constraint) in enumerate(
            zip(self.ineq_l_mults, self.active_ineq_constraints)
        ):
            error = constraint.error(x)
            if self.inequality_contributes_to_cost(error, index):
                hessian = hessian + l_mult * constraint.error_hessian(x)
        return hessian


class PenaltyMethod(OptimizationSolver):
    """
    Solves arbitrary smooth contsrained optimization problems by transforming constraints into
    quadratic penalty costs and adding them to the original cost function
    """

    def __init__(
        self,
        problem,
        internal_optimizer="gradient descent",
        optimizer_args={"alpha": 1, "rho": 0.9},
        FP_PRECISION=1e-7,
        quadratic_update_multiplier=10.0,
        cost_threshold=None,
        library=np,
        print_info=False,
    ):
        super().__init__(problem, FP_PRECISION, library=library, cost_threshold=cost_threshold)

        self.optimizer_args = optimizer_args
        self.internal_optimizer = internal_optimizer
        # How much we update the penalty parameter each round
        self.quadratic_update_mul = quadratic_update_multiplier
        self.print_info = print_info

    def reset_time_vars(self):
        self._gradTime = 0
        self._searchTime = 0
        self._lengthTime = 0
        self._hessTime = 0
        self._batchtrackTime = 0
        self._backtrackTime = 0
        self._avg_step_length = 0.0
        self._avg_step_calls = 0.0
        self._avg_step_idx = 0

    def optimize(self, x0=None, max_iterations=100):
        self.reset_time_vars()
        assert max_iterations >= 1, "max_iterates must be ≥ 1"
        assert isinstance(max_iterations, int), "max_iterates must be an int"
        start = time.time()

        quadratic_coefficients = 1e1
        internal_iterates = []
        active_ineq_constraints = []
        converged = False

        x_iter = x0
        for i in range(max_iterations):
            new_problem = PenaltyProblemWrapper(
                self.problem, quadratic_coefficients, active_ineq_constraints, self.math_library
            )

            optimizer = _OPTIMIZERS_DICT[self.internal_optimizer](
                problem=new_problem,
                FP_PRECISION=self.FP_PRECISION,
                library=self.math_library,
                **self.optimizer_args
            )
            x_old = x_iter

            res = optimizer.optimize(x_iter, max_iterations=max_iterations)
            self._hessTime += optimizer._hessTime
            self._avg_step_length += optimizer._avg_step_length
            self._avg_step_calls += optimizer._avg_step_calls
            self._avg_step_idx += optimizer._avg_step_idx
            x_iter = res.solution
            internal_iterates.append(res.iterates)
            g = self.evaluate_constraints(x_iter, self.problem.inequality_constraints)
            h = self.evaluate_constraints(x_iter, self.problem.equality_constraints)
            g_max = self.math_library.max(g)
            h_violation_max = self.math_library.max(self.math_library.abs(h))
            iterate_step_max = self.math_library.abs(x_iter - x_old).max()

            criteria = {
                _STEP_THRESH: iterate_step_max,
                _INEQ_CONST_VIOL: g_max,
                _EQ_CONST_VIOL: h_violation_max,
            }

            if g_max < self.FP_PRECISION and h_violation_max < self.FP_PRECISION:
                if iterate_step_max < self.FP_PRECISION:
                    if self.print_info:
                        print(
                            "Outer iteration ",
                            i,
                            " converged in ",
                            res.iterates.shape[0] - 1,
                            " inner iterations",
                        )
                    converged = True
                    break
                if self.print_info:
                    print(
                        "Outer iteration ",
                        i,
                        " violates no constraints after ",
                        res.iterates.shape[0] - 1,
                        " inner iterations, continuing",
                    )
                    converged = True
                    break

                # Test using the cost_threshold
                cur_cost = float(self.problem.cost(x_iter))
                if self.cost_threshold is not None and cur_cost < self.cost_threshold:
                    converged = True
                    criteria[_COST_THRESH] = (self.cost_threshold, cur_cost)
                    break

                print(
                    "Outer iteration ",
                    i,
                    " violates no constraints after ",
                    res.iterates.shape[0] - 1,
                    " inner iterations, continuing",
                )
                continue
            else:
                quadratic_coefficients *= self.quadratic_update_mul
                violated_ineq_constraints = [
                    self.problem.inequality_constraints[j]
                    for j in self.math_library.argwhere(g > 0).flatten().tolist()
                ]
                # Combine without duplicates
                active_ineq_constraints = list(
                    set(active_ineq_constraints + violated_ineq_constraints)
                )
            if self.print_info:
                print(
                    "Outer iteration ",
                    i,
                    " needed ",
                    res.iterates.shape[0] - 1,
                    " inner iterations",
                )

        duration = time.time() - start
        return SolverReturn(
            solution=x_iter,
            cost=float(self.problem.cost(x_iter)),
            converged=bool(converged),
            convergence_criteria=criteria,
            time=duration,
            iterates=self.math_library.vstack(internal_iterates),
        )

    def evaluate_constraints(self, x, constraints):
        """
        Retrun a vector containing error for all constraints for decision variable x
        """
        if len(constraints):
            constraint = [constraint.error(x) for constraint in constraints]
            constraint = self.math_library.stack(constraint)
            return constraint
        if self.math_library == torch:
            return torch.tensor(0.0)
        return np.zeros(1)

    def get_always_active_constraints(self, constraints):
        if len(constraints):
            constraint = [
                hasattr(constraint, "always_active") and constraint.always_active
                for constraint in constraints
            ]
            constraint = np.stack(constraint)
            return constraint
        return np.stack([False])


class AugmentedLagranMethod(PenaltyMethod):
    """
    Solves arbitrary smooth contsrained optimization problems by transforming constraints into
    quadratic penalty costs and a Lagrangian cost adding them to the original cost function
    """

    def optimize(self, x0=None, max_iterations=100):
        assert max_iterations >= 1, "max_iterates must be ≥ 1"
        assert isinstance(max_iterations, int), "max_iterates must be an int"
        start = time.time()

        # Initial guesses of lagrange multiplier, currently start with all on,
        # could be smarter, based on x0 for example
        # Lagrange multipliers update when the internal solver converges and meets
        # the constraint tolerance
        ineq_l_mults = np.array([0.0] * len(self.problem.inequality_constraints))
        eq_l_mults = np.array([1.0] * len(self.problem.equality_constraints))

        active_ineq_l_mults = np.array([])

        # This is the quadratic penalty constraint that gets higher if the constraint tolerances
        # aren't met after the inner problem converges
        quadratic_coefficients = 1e5  # mu0 in N&W Alg 17.4

        # These tolerances adapt to get tighter at each outer iteration
        internal_solver_tol = 1e-1  # omega0 in N&W Alg 17.4
        constraint_tol = 1.0 / (quadratic_coefficients**0.1)  # eta0 in N&W Alg 17.4

        # These are the absolute tolerances for convergence of the full problem
        constraint_tol_final = self.FP_PRECISION
        solver_tol_final = self.FP_PRECISION
        converged = False
        criteria = {}

        internal_iterates = []
        active_ineq_constraints = []
        active_ineq_indices = []

        l_mults = []
        penalties = []

        active_mask = self.get_always_active_constraints(self.problem.inequality_constraints)
        active_ineq_indices = np.argwhere(active_mask).flatten().tolist()
        active_ineq_indices.sort()
        active_ineq_constraints = [
            self.problem.inequality_constraints[index] for index in active_ineq_indices
        ]
        active_ineq_l_mults = ineq_l_mults[active_ineq_indices]

        x_iter = x0
        isstuck = False
        for i in range(max_iterations):
            new_problem = AugLagrangProblemWrapper(
                self.problem,
                quadratic_coefficients,
                active_ineq_constraints,
                active_ineq_l_mults,
                eq_l_mults,
            )

            l_mults.append(np.append(ineq_l_mults, eq_l_mults))
            penalties.append(np.append(quadratic_coefficients, quadratic_coefficients))
            optimizer = _OPTIMIZERS_DICT[self.internal_optimizer](
                problem=new_problem, FP_PRECISION=internal_solver_tol, **self.optimizer_args
            )

            x_old = x_iter
            res = optimizer.optimize(x_iter, max_iterations=max_iterations)
            x_iter = res.solution
            internal_iterates.append(res.iterates)

            g = self.evaluate_constraints(x_iter, self.problem.inequality_constraints)
            g = g.clip(min=0.0)
            h = self.evaluate_constraints(x_iter, self.problem.equality_constraints)
            h = np.abs(h)
            # Check if constraint violation is less than current tolerances

            g_max = np.max(g)
            h_violation_max = np.max(np.abs(h))
            iterate_step_max = np.abs(x_iter - x_old).max()

            criteria = {
                _STEP_THRESH: iterate_step_max,
                _INEQ_CONST_VIOL: g_max,
                _EQ_CONST_VIOL: h_violation_max,
            }

            if g_max < constraint_tol and h_violation_max < constraint_tol:
                isstuck = False
                # Test for convergence
                if (
                    iterate_step_max < solver_tol_final
                    and g_max < constraint_tol_final
                    and h_violation_max < constraint_tol_final
                ):
                    if self.print_info:
                        print(
                            "Outer iteration ",
                            i,
                            " converged in ",
                            res.iterates.shape[0] - 1,
                            " inner iterations",
                        )
                    converged = True
                    break
                if self.print_info:
                    print(
                        "Outer iteration ",
                        i,
                        " violates no constraints after ",
                        res.iterates.shape[0] - 1,
                        " inner iterations, continuing",
                    )
                    converged = True
                    break

                # Test using the cost_threshold
                cur_cost = float(self.problem.cost(x_iter))
                if self.cost_threshold is not None and cur_cost < self.cost_threshold:
                    converged = True
                    criteria[_COST_THRESH] = (self.cost_threshold, cur_cost)
                    break

                print(
                    "Outer iteration ",
                    i,
                    " violates no constraints after ",
                    res.iterates.shape[0] - 1,
                    " inner iterations, continuing",
                )

                # Update Lagrange multipliers based on current error
                ineq_l_mults += quadratic_coefficients * g
                ineq_l_mults[ineq_l_mults < 0] = 0  # y = max(0,y + u*c(x))
                eq_l_mults += quadratic_coefficients * h

                # Tighten the constraint and solver tolerances
                constraint_tol = constraint_tol / (quadratic_coefficients**0.9)
                internal_solver_tol = internal_solver_tol / quadratic_coefficients

            else:
                # Inner solver converged, but constraints are violated

                # If the steps didn't change for 2 iterations and penalty has saturated
                # Then return early as infeasible
                if (iterate_step_max < solver_tol_final) and (quadratic_coefficients > 1e12):
                    if isstuck:
                        print(
                            "Outer iteration ",
                            i,
                            " converged with infeasibilities under maximum allowed penalty",
                            " is the problem infeasible?",
                        )
                        break
                    isstuck = True
                else:
                    isstuck = False

                # Increase penalty
                if quadratic_coefficients < 1e12:
                    quadratic_coefficients *= min(self.quadratic_update_mul, (i + 1) * 2)

                # Identify active inequality constraints
                violated_ineq_indices = np.argwhere(g > 0).flatten().tolist()
                # Combine without duplicates
                active_ineq_indices = list(set(violated_ineq_indices + active_ineq_indices))
                active_ineq_indices.sort()
                active_ineq_constraints = [
                    self.problem.inequality_constraints[index] for index in active_ineq_indices
                ]
                active_ineq_l_mults = ineq_l_mults[active_ineq_indices]

                # Tighten the constraint and solver tolerances
                constraint_tol = 1.0 / (quadratic_coefficients**0.1)
                internal_solver_tol = 1.0 / quadratic_coefficients

                if self.print_info:
                    print(
                        "Outer iteration ",
                        i,
                        " needed ",
                        res.iterates.shape[0] - 1,
                        " inner iterations",
                    )

        duration = time.time() - start
        return SolverReturn(
            solution=x_iter,
            cost=float(self.problem.cost(x_iter)),
            converged=bool(converged),
            convergence_criteria=criteria,
            time=duration,
            iterates=np.vstack(internal_iterates),
        )
