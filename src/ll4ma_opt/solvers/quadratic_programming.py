# Third Party
import numpy as np

# ll4ma
from ll4ma_opt.solvers.matrix_utils import compute_inverse_cholesky, get_nonsingular_YZ


class QP:
    def __init__(self, max_iterations=1000):
        self.max_iters = max_iterations
        self.n = 0
        self.me = 0
        self.mi = 0
        self.G = None
        self.c = None
        self.Ae = None
        self.Ai = None
        self.be = None
        self.bi = None
        self.x0 = None

    def setQP(self, G, c, Ae, be, Ai, bi, x0=None):
        # TODO: Add auto feasible X0
        me = 0 if Ae is None else Ae.shape[0]
        mi = 0 if Ai is None else Ai.shape[0]
        n = G.shape[0]
        if me > 0:
            assert Ae.shape == (me, n), "Check Shape of Ae"
            assert be.shape == (
                me,
                1,
            ), "Shape of Equality Constraint RHS mismatched, needs to be a column vector"
        if mi > 0:
            assert Ai.shape == (mi, n), "Check Shape of Ai"
            assert bi.shape == (
                mi,
                1,
            ), "Shape of inEquality Constraint RHS mismatched, needs to be a column vector"
        assert G.shape == (n, n), "Shape of Objective Matrix and constraint mismatched"
        assert c.shape == (
            n,
            1,
        ), "Shape of Objective Vector and constraint mismatched, needs to be a column vector"
        if x0 is None:
            x0 = np.zeros([n, 1])
        assert x0.shape == (n, 1), "Shape of x0 mismatched, needs to be a column vector"
        self.n = n
        self.me = me
        self.mi = mi
        self.G = G
        self.c = c
        self.Ae = Ae
        self.Ai = Ai
        self.be = be
        self.bi = bi
        self.x0 = x0
        assert self.checkFeasible(x0), (
            "x0 not feasible. Feasible initial point has to be provided. (You can also create an"
            " issue or nudge for the auto feasible point to be implemented)"
        )

    def direct_EQP_nullspace(self, G, c, A, b, x0=None, lagrangians=False):
        m = A.shape[0]
        n = A.shape[1]
        assert A.shape == (m, n), "Check Shape of A"
        assert G.shape == (n, n), "Shape of Objective Matrix and constraint mismatched"
        assert c.shape == (
            n,
            1,
        ), "Shape of Objective Vector and constraint mismatched, needs to be a column vector"
        assert b.shape == (
            m,
            1,
        ), "Shape of Constraint RHS mismatched, needs to be a column vector"
        x = x0
        if x is None:
            x = np.zeros([n, 1])
        assert x.shape == (n, 1), "Shape of x0 mismatched, needs to be a column vector"
        g = c + G @ x  # 16.6
        h = A @ x - b  # 16.6
        Y, Z = get_nonsingular_YZ(A)

        # TODO: Replace inverse with solve
        AY_inv = np.linalg.inv(A @ Y)  # AY is nonsingular so this should work always
        p_y = -AY_inv @ h  # 16.18
        # Calculations for p_z
        ZtG = Z.T @ G  # ZTrans*G
        # TODO: Replace inverse with solve
        ZtGZ_inv = compute_inverse_cholesky(ZtG @ Z)
        ZtGY = ZtG @ Y
        ZtGZp_z = -ZtGY @ p_y - Z.T @ g
        p_z = ZtGZ_inv @ ZtGZp_z  # 16.19
        p = Y @ p_y + Z @ p_z

        if not lagrangians:
            return x + p
        lag_muls = AY_inv.T @ Y.T @ g + G @ p  # 16.20
        return x + p, lag_muls

    def checkFeasible(self, x):
        Ae = self.Ae
        Ai = self.Ai
        be = self.be
        bi = self.bi
        if Ae is not None:
            Efit = Ae @ x - be
            if not self.checkZero(Efit):
                return False
        if Ai is not None:
            Ifit = Ai @ x - bi
            if min(Ifit) < 0:
                return False
        return True

    def checkZero(self, x):
        if (min(x) <= -1e-12) or (max(x) >= 1e-12):
            return False
        return True

    def get_active_inEqs(self, x):
        Ai = self.Ai
        bi = self.bi
        act_ids = []
        if Ai is not None:
            Ifit = Ai @ x - bi
            for i, fit in enumerate(Ifit):
                if fit == 0:
                    act_ids.append(i)
        return act_ids

    def get_workset(self, ids):
        A = self.Ae
        b = self.be
        for i in ids:
            A = np.vstack((A, self.Ai[i]))
            b = np.vstack((b, self.bi[i]))
        assert A.shape == (
            self.me + len(ids),
            self.n,
        ), "Stacking misaligned, check get_workset"
        assert b.shape == (
            self.me + len(ids),
            1,
        ), "Stacking misaligned, check get_workset"
        return A, b

    def get_alpha(self, x, p, w_k):
        alpha = 1
        for i in range(self.mi):
            if (i not in w_k) and (self.Ai[i].T @ p < 0):
                alpha = min(
                    alpha,
                    (self.bi[i] - self.Ai[i].T @ x) / self.Ai[i].T @ p,
                )
        return alpha

    def add_blocking_id(self, x, w_k):
        for i in range(self.mi):
            if (i not in w_k) and self.Ai[i].T @ x - self.bi[i] < 0:
                w_k.append(i)
                return w_k
        return w_k

    def active_set_QP(self):
        Ae = self.Ae
        Ai = self.Ai
        be = self.be
        bi = self.bi
        me = self.me
        mi = self.mi
        n = self.n
        G = self.G
        c = self.c
        x_k = self.x0
        w_k = self.get_active_inEqs(x_k)
        wA_k, wb_k = self.get_workset(w_k)
        iterates = []
        for k in range(self.max_iters):
            iterates.append(x_k)
            g_k = G @ x_k + c
            p = np.zeros([n, 1])
            p_k, lag_muls = self.direct_EQP_nullspace(G, g_k, wA_k, wb_k * 0, p, True)
            if self.checkZero(p_k):
                if np.all(lag_muls[me:] >= 0):
                    iterates.append(x_k)
                    break
                else:
                    j = np.argmin(lag_muls[me:])
                    w_k = np.delete(w_k, np.argwhere(w_k == j))
                    wA_k, wb_k = self.get_workset(w_k)
            else:
                alpha = self.get_alpha(x_k, p_k, w_k)
                x_k = x_k + alpha * p_k
                if not self.checkFeasible(x_k):
                    w_k = self.add_blocking_id(x_k, w_k)
                    wA_k, wb_k = self.get_workset(w_k)
        return x_k, 0, np.array(iterates)
