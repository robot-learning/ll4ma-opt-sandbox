# Third Party
import numpy as np

# ll4ma
from ll4ma_opt.solvers.line_search import NewtonMethod
from ll4ma_opt.solvers.trust_region import IterativeTRSolver, is_pos_def, scale_vector


class BFGSMethod(NewtonMethod):
    """
    Algorithm 6.1.

    Instead of computing the Hessian at every time step we calculate an
    approximation and edit that every time step.
    """

    def __init__(
        self,
        problem,
        alpha=1.0,
        rho=0.9,
        c1=1e-4,
        sigma=0.05,
        FP_PRECISION=1e-7,
        min_alpha=1e-10,
        library=np,
        cost_threshold=None,
    ):
        super().__init__(
            problem,
            alpha,
            rho,
            c1=c1,
            FP_PRECISION=FP_PRECISION,
            min_alpha=min_alpha,
            library=library,
            cost_threshold=cost_threshold,
        )
        self.sigma = sigma
        self.x_km1 = None
        self.g_km1 = None
        self.H = None

    def compute_search_direction(self, current_solution, binding_set, gradient=None):
        x_k = current_solution
        g_k = self.problem.cost_gradient(x_k) if gradient is None else gradient
        if self.H is None:
            self.H = self.math_library.eye(len(x_k), dtype=x_k.dtype) * self.sigma
        # if both solution and gradient changed update Hessian
        elif self.math_library.any(x_k != self.x_km1) and self.math_library.any(g_k != self.g_km1):
            s_k = x_k - self.x_km1
            y_k = g_k - self.g_km1
            ro_k = 1.0 / (y_k.T @ s_k).item()
            Ident = self.math_library.eye(len(x_k))
            if (self.H == Ident * self.sigma).all():
                self.H = Ident * ((y_k.T @ s_k) / (y_k.T @ y_k)).item()
            a_k = Ident - ro_k * s_k @ y_k.T
            b_k = Ident - ro_k * y_k @ s_k.T
            c_k = ro_k * s_k @ s_k.T
            self.H = a_k @ self.H @ b_k + c_k
        self.x_km1 = x_k
        self.g_km1 = g_k
        return -self.hessian_constraint_modification(self.H, binding_set) @ g_k


class SR1Method(IterativeTRSolver):
    """
    Based on Theorem 4.1 and Algorithm 4.3 (Trust Region Subproblem)
    """

    def __init__(
        self,
        problem,
        trust_region_size=2,
        sigma=1.0,
        r=1e-8,
        FP_PRECISION=1e-7,
        cost_threshold=None,
    ):
        super().__init__(
            problem,
            trust_region_size,
            FP_PRECISION=FP_PRECISION,
            cost_threshold=cost_threshold,
        )
        self.sigma = sigma
        self.r = r

        self.g_km1 = None
        self.s_km1 = None
        self.Bmat = None

    def hessian(self, x):
        # local approximation of hessian
        return self.Bmat

    def solve_trust_region_problem(self, current_solution, trust_region_size):
        x_k = current_solution
        delta = trust_region_size
        g_k = self.problem.cost_gradient(x_k)

        if self.Bmat is None:
            self.Bmat = np.eye(len(x_k)) * self.sigma
        else:
            # update self.Bmat
            y_k = g_k - self.g_km1
            s_k = self.s_km1
            a_k = y_k - np.matmul(self.Bmat, s_k)
            a_k_norm = np.linalg.norm(a_k, keepdims=True)
            s_k_norm = np.linalg.norm(s_k, keepdims=True)
            b_k = np.matmul(s_k_norm.T, a_k_norm)
            if np.abs(np.matmul(s_k.T, a_k)).item() >= self.r * b_k:
                n_k = np.matmul(a_k, a_k.T)
                d_k = np.matmul(a_k.T, s_k).item()
                if d_k == 0:
                    d_k += 1e-8
                change = n_k / d_k
                self.Bmat = self.Bmat + change

        self.g_km1 = g_k
        B = self.Bmat
        gradient = g_k

        # case when lambda is 0
        if is_pos_def(B):
            p = self.linear_solve(B, -gradient)
            if np.linalg.norm(p) > delta:
                p = scale_vector(p, delta)
            self.s_km1 = p
            return p

        # we need to find lambda
        lmbd = self.trust_region_subproblem(1, delta, x_k)
        dim = np.shape(B)[0]
        p = self.linear_solve(B + lmbd * np.identity(dim), -gradient)

        if np.linalg.norm(p) > delta:
            p = scale_vector(p, delta)

        self.s_km1 = p
        assert np.linalg.norm(p) - delta <= self.FP_PRECISION
        return p

    # def trust_region_subproblem(self, lmbd_0, delta, x):
    #     Bmat = self.B
    #     self.B = lambda x : Bmat
    #     retval = super().trust_region_subproblem(lmbd_0, delta, x)
    #     self.B = Bmat
    #     return retval
