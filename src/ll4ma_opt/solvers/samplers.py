# Third Party
import numpy as np
import torch
from torch.distributions import MultivariateNormal as MVN
from torch.distributions import kl_divergence


class Sampler:
    def __init__(self, sample_size, min_bounds, max_bounds, n_steps=100):
        self.sample_size = sample_size
        if not torch.is_tensor(min_bounds):
            min_bounds = torch.tensor(min_bounds).flatten()
        if not torch.is_tensor(max_bounds):
            max_bounds = torch.tensor(max_bounds).flatten()
        self.min_bounds = min_bounds
        self.max_bounds = max_bounds
        self.distribution = None
        self.prev_distribution = None
        self.n_steps = n_steps
        self.step_idx = 0
        self.uniform_distr = torch.distributions.Uniform(self.min_bounds, self.max_bounds)

    def set_params(self, *args, **kwargs):
        raise NotImplementedError()

    def sample(self, n_samples, *args, **kwargs):
        if self.distribution is None:
            raise ValueError("Distribution was not set on sampler")
        samples = self.distribution.sample((n_samples,))
        samples = torch.clamp(samples, min=self.min_bounds, max=self.max_bounds)
        return samples

    def fit(self, samples, *args, **kwargs):
        raise NotImplementedError()

    def get_mean(self):
        raise NotImplementedError()

    def step(self):
        """
        This function will typically be called each iteration in the optimization
        and is a hook to enable customizing the sampler as iterations progress,
        e.g. sampling with less noise in later iterations to refine solution.
        """
        self.step_idx += 1

    def kl(self, other):
        raise NotImplementedError()

    def random_uniform_sampling(self, n_samples, sample_size=None):
        if sample_size is None:
            sample_size = self.sample_size
        samples = torch.rand((n_samples, sample_size), dtype=torch.double)
        samples = samples * (self.max_bounds - self.min_bounds) + self.min_bounds
        return samples

    def uniform_sampling(self, n_samples):
        samples = self.uniform_distr.sample(sample_shape=torch.Size((n_samples,)))
        return samples


class GaussianSampler(Sampler):
    def __init__(
        self,
        sample_size,
        min_bounds,
        max_bounds,
        mu=None,
        sigma=None,
        sigma_reg=None,
        start_iso_var=None,
        end_iso_var=None,
        n_steps=100,
    ):
        super().__init__(sample_size, min_bounds, max_bounds, n_steps)

        if (sigma is not None and start_iso_var is not None) or (
            sigma is None and start_iso_var is None
        ):
            raise ValueError("Must specify exactly one of sigma or start_iso_var")

        N = self.sample_size
        mu = torch.zeros(N).double() if mu is None else mu
        if start_iso_var is not None:
            sigma = start_iso_var * torch.eye(N).double()
        elif sigma is None:
            sigma = torch.eye(N).double()
        sigma_reg = 1e-5 * torch.eye(N).double() if sigma_reg is None else sigma_reg
        self.set_params(mu, sigma, sigma_reg)

        # These are isotropic Gaussian variances that can optionally be linearly
        # increased/decreased over iterations when the sampler is stepped
        self.iso_vars = None
        if start_iso_var is not None:
            if end_iso_var is not None:
                self.iso_vars = np.linspace(start_iso_var, end_iso_var, n_steps)
            else:
                self.iso_vars = np.full(n_steps, start_iso_var)

    def set_params(self, mu=None, sigma=None, sigma_reg=None):
        # TODO add error checking on sizes, PSD
        if mu is not None:
            if not isinstance(mu, torch.Tensor):
                mu = torch.tensor(mu).double()
            self.mu = mu.squeeze()
        if sigma is not None:
            if not isinstance(sigma, torch.Tensor):
                sigma = torch.tensor(sigma).double()
            self.sigma = sigma
        if sigma_reg is not None:
            if not isinstance(sigma_reg, torch.Tensor):
                sigma_reg = torch.tensor(sigma_reg).double()
            self.sigma_reg = sigma_reg

        sigma = self.sigma + self.sigma_reg
        self.distribution = MVN(self.mu, sigma)

    def fit(self, samples):
        self.prev_distribution = self.distribution
        self.set_params(samples.mean(dim=0), samples.T.cov())

    def get_mean(self):
        return self.mu

    def step(self):
        super().step()
        if self.iso_vars is not None:
            idx = min(self.n_steps - 1, self.step_idx)
            sigma = self.iso_vars[idx] * torch.eye(self.sample_size).double()
            self.set_params(sigma=sigma)

    def kl_to_prev(self):
        """
        Compute the kl divergence between distributions at subsequent timesteps.

        Returns "inf" if either is not defined.
        """
        if self.distribution is None or self.prev_distribution is None:
            return float("inf")
        return kl_divergence(self.distribution, self.prev_distribution)

    def covariance_determinant(self):
        return self.distribution.covariance_matrix.det()
