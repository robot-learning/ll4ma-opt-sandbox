#!/usr/bin/env python
# Standard Library
import time

# Third Party
import numpy as np
from scipy.optimize import Bounds, minimize

# ll4ma
# from ll4ma_opt.solvers.line_search import GradientDescent, NewtonMethod, Momentum
# from ll4ma_opt.solvers.penalty_method import PenaltyMethod
# from ll4ma_opt.problems import DubinsCar, DoubleIntegrator, TwoLinkIK, PandaIK
from ll4ma_opt.problems import DoubleIntegrator
from ll4ma_opt.solvers.optimization_solver import SolverReturn

# from torchmin import minimize, minimize_constr


if __name__ == "__main__":
    # problem = DubinsCar()
    problem = DoubleIntegrator(horizon=30)
    # problem = F0()
    # problem = TwoLinkIK()
    # problem = PandaIK()

    init = problem.initial_solution

    bounds = Bounds(problem.min_bounds, problem.max_bounds)
    # bounds = {'lb': torch.tensor(problem.min_bounds),
    #           'ub': torch.tensor(problem.max_bounds)}

    eq_constraints = [{"type": "eq", "fun": c.tensor_error} for c in problem.equality_constraints]
    ineq_constraints = [
        {"type": "ineq", "fun": c.tensor_error} for c in problem.inequality_constraints
    ]
    constraints = eq_constraints + ineq_constraints

    print("Solving...")
    start = time.time()

    soln = minimize(
        problem.cost,
        init,
        # method='bfgs',
        method="SLSQP",
        # bounds=bounds,
        constraints=constraints,
        options={"disp": True, "eps": 0.5, "maxiter": 50},
    )

    # soln = minimize_constr(
    #     problem.tensor_cost,
    #     torch.tensor(init),
    #     bounds=bounds,

    #     # options={'disp': True, 'eps': 2}
    # )

    print(soln)

    print("POS", soln.x[problem.pos_idxs].reshape(problem.horizon, -1))
    print("VEL", soln.x[problem.vel_idxs].reshape(problem.horizon, -1))
    print("ACC", soln.x[problem.acc_idxs].reshape(problem.horizon, -1))

    print("TIME", time.time() - start)

    result = SolverReturn()
    result.iterates = np.expand_dims(soln.x, 0)  # soln.allvecs

    # solver = GradientDescent(problem, alpha=1, rho=0.2)
    # solver = NewtonMethod(problem)
    # solver = Momentum(problem, rho=0.25)
    # solver = PenaltyMethod(problem)

    # print("Solving...")
    # result = solver.optimize(problem.initial_solution, max_iterations=5)
    # result.display(False)
    # print("\nGRAD TIME", problem.gradientTime)
    # print("HESS TIME", problem.hessianTime)
    # problem.visualize_optimization(result, client='matplotlib')
    problem.visualize_optimization(result)
