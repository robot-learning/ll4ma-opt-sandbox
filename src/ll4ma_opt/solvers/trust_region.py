# Standard Library
import time

# Third Party
import numpy as np

# ll4ma
from ll4ma_opt.solvers.matrix_utils import adaptive_cholesky_factorization, linear_solve_lu_adaptive
from ll4ma_opt.solvers.optimization_solver import OptimizationSolver, SolverReturn
from ll4ma_opt.solvers.opt_conv_criteria import _GRAD_THRESH, _GRAD_THRESH_L2, _COST_THRESH


class TrustRegionSolver(OptimizationSolver):
    """
    Check the book Numerical Optimization by Nocedal & Wright, Chapter 4.
    Equation references, e.g. (4.4), are the references to the equations in that book.
    Names of the variables are according to the book as well.
    """

    def __init__(
        self,
        problem,
        trust_region_size=2,
        eta=1.0 / 8,
        FP_PRECISION=1e-6,
        cost_threshold=None,
    ):
        assert trust_region_size > 0, "Trust region size must be positive"
        assert 0 <= eta < 1 / 4, "Eta must be in range [0, 1/4>"

        super().__init__(problem, FP_PRECISION=FP_PRECISION, cost_threshold=cost_threshold)
        self.trust_region_size = trust_region_size
        self.eta = eta
        self.linear_solve = linear_solve_lu_adaptive

    def trust_model(self, x, p):
        # TODO clean up this function with intermediate steps now that it isnt lambda
        return (
            self.problem.cost(p)
            + np.dot(self.problem.cost_gradient(x).T, p)
            + 0.5 * np.dot(np.dot(p.T, self.hessian(x)), p)
        )

    def hessian(self, x):
        return self.problem.cost_hessian(x)

    def optimize(self, x0=None, max_iterations=100):
        """
        Algorithm 4.1.
        """
        start = time.time()
        x_k = x0
        delta_hat = self.trust_region_size
        delta_k = delta_hat / 2  # should be in interval <0, delta_hat>

        iterates = []
        iterates.append(x_k)
        k = 0
        converged = False
        criteria = {}  # Store convergence criteria as dictionary

        while k < max_iterations and not converged:
            p_k = self.solve_trust_region_problem(x_k, delta_k)
            ro_k = self.compute_reduction_ro(x_k, p_k)

            if ro_k <= 1 / 4:
                delta_k = (1 / 4) * delta_k
            elif ro_k >= 3 / 4 and abs(np.linalg.norm(p_k) - delta_k) < self.FP_PRECISION:
                delta_k = min(2 * delta_k, delta_hat)

            if ro_k > self.eta:
                x_k = x_k + p_k

            # converged if gradient is close to zero (L2 or L inf)
            gradient = self.problem.cost_gradient(x_k)

            gradient_l2_norm = np.linalg.norm(gradient)
            gradient_infinity_norm = np.abs(gradient).max()

            converged = (
                gradient_l2_norm < self.FP_PRECISION or gradient_infinity_norm < self.FP_PRECISION
            )

            cur_cost = float(self.problem.cost(x_k))
            if self.cost_threshold is not None and cur_cost < self.cost_threshold:
                converged = True
                criteria[_COST_THRESH] = (self.cost_threshold, cur_cost)

            k += 1
            iterates.append(x_k)  # for visualization purposes

            # trust region too small
            if delta_k < self.FP_PRECISION:
                print(f"Trust region too small after iteration {k}")
                break
        criteria[_GRAD_THRESH_L2] = gradient_l2_norm
        criteria[_GRAD_THRESH] = gradient_infinity_norm

        duration = time.time() - start
        return SolverReturn(
            solution=x_k,
            cost=float(self.problem.cost(x_k)),
            converged=bool(converged),
            convergence_criteria=criteria,
            time=duration,
            iterates=np.array(iterates),
        )

    def compute_reduction_ro(self, x, p):
        """
        As defined in (4.4)
        """
        actual_reduction = self.problem.cost(x) - self.problem.cost(x + p)

        zero = np.zeros((x.shape[0], 1))  # 0 as column vector
        predicted_reduction = self.trust_model(x, zero) - self.trust_model(x, p)

        if predicted_reduction == 0:
            return np.inf

        ro = (actual_reduction * 1.0) / predicted_reduction
        return ro

    def solve_trust_region_problem(self, current_solution, trust_region_size):
        raise NotImplementedError("Implement to instantiate different algorithms. :)")


class CauchyPointSolver(TrustRegionSolver):
    def solve_trust_region_problem(self, current_solution, trust_region_size):
        """
        Cauchy Point implementation as in 4.11 and 4.12
        """
        x = current_solution
        delta = trust_region_size

        gradient = self.problem.cost_gradient(x)
        grad_norm = np.linalg.norm(gradient)

        # (4.12)
        gtBg = np.dot(np.dot(gradient.T, self.hessian(x)), gradient).item()
        if gtBg <= 0:
            tau = 1
        else:
            tau = min((grad_norm**3) / (delta * gtBg), 1)

        # (4.11)
        scalar = tau * delta / grad_norm
        p = -scalar * gradient

        return p


class DoglegMethod(TrustRegionSolver):
    """
    Most appropriate when the objective function is convex
    """

    def solve_trust_region_problem(self, current_solution, trust_region_size):
        """
        Starting on page 73
        """
        x = current_solution
        delta = trust_region_size

        gradient = self.problem.cost_gradient(x)
        B = self.hessian(x)

        pb = self.linear_solve(B, -gradient)

        # condition (4.13)
        if np.linalg.norm(pb) <= trust_region_size:
            return pb

        scalar = ((np.dot(gradient.T, gradient)) / np.dot(np.dot(gradient.T, B), gradient)).item()
        pu = -scalar * gradient

        # TODO: Something is broken here
        raise NotImplementedError("Dogleg method is still under development")
        # if np.array_equal(pb,pu):
        #    pu = scale_vector(pu, delta)
        #    return pu

        # looking for solution of equation  (end of page 75)
        # ||pu + (tau-1)(pb - pu)||^2 = delta ^ 2
        #
        # let lambda = tau-1, u = pu, v = pb-pu
        # using theorem ||u+lambda*v||^2 = ||u||^2 + lambda^2 * ||v||^2 + 2 * lambda * dot(u.T,v)
        # solving for lambda: lambda^2 * ||v||^2 + lambda * (2 * dot(u.T,v)) + ||u||^2 - delta^2 = 0
        u = pu
        v = pb - pu
        a = np.linalg.norm(v) ** 2
        b = 2 * np.dot(u.T, v).item()
        c = np.linalg.norm(u) ** 2 - delta**2
        # solutions for lambda
        solutions = np.roots([a, b, c])
        print(solutions)
        # we set previoslu lambda = tau - 1, so tau = 1 + lambda
        tau = max(1 + solutions[0], 1 + solutions[1])

        # make sure solution of the equation we were solving is correct
        assert np.linalg.norm(pu + (tau - 1) * (pb - pu)) ** 2 - delta * delta < self.FP_PRECISION
        assert 0 <= tau <= 2

        if 0 <= tau <= 1:
            return tau * pu
        if 1 <= tau <= 2:
            return pu + (tau - 1) * (pb - pu)


class IterativeTRSolver(TrustRegionSolver):
    """
    Based on Theorem 4.1 and Algorithm 4.3 (Trust Region Subproblem)
    """

    def solve_trust_region_problem(self, current_solution, trust_region_size):
        x = current_solution
        delta = trust_region_size

        B = self.hessian(x)
        gradient = self.problem.cost_gradient(x)

        # case when lambda is 0

        if is_pos_def(B):
            p = self.linear_solve(B, -gradient)
            if np.linalg.norm(p) > delta:
                p = scale_vector(p, delta)
            return p

        # we need to find lambda
        lmbd = self.trust_region_subproblem(1, delta, x)
        dim = np.shape(B)[0]

        p = self.linear_solve(B + lmbd * np.identity(dim), -gradient)

        if np.linalg.norm(p) > delta:
            p = scale_vector(p, delta)

        assert np.linalg.norm(p) - delta <= self.FP_PRECISION
        return p

    def trust_region_subproblem(self, lmbd_0, delta, x):
        B = self.hessian(x)
        lmbd = lmbd_0
        gradient = self.problem.cost_gradient(x)

        # 3 iterations because of practical concerns,
        # as described in the paragraph after the algorithm 4.3
        # TODO: Update this to use adpative cholesky or other tools from matrix_utils more directly
        for i in range(3):
            try:
                R = np.linalg.cholesky(B + lmbd * np.identity(np.shape(B)[0]))
            # the matrix is not positive definite
            except np.linalg.LinAlgError:
                try:
                    R, low = adaptive_cholesky_factorization(B, 1e-3)
                except Exception as err2:
                    print("Error with adaptive cholesky factorization: ", err2)
                    return lmbd

            pl = self.linear_solve(R.T * R, -gradient)
            ql = self.linear_solve(R.T, pl)

            pl_norm = np.linalg.norm(pl)
            ql_norm = np.linalg.norm(ql)

            lmbd = lmbd + ((pl_norm / ql_norm) ** 2) * ((pl_norm - delta) / delta)
        return lmbd


# check if matrix is positive definite


def is_pos_def(x):
    try:
        return np.all(np.linalg.eigvals(x) > 0)
    except Exception:
        # Standard Library
        import pdb

        pdb.set_trace()


def scale_vector(vector, scale):
    unit = vector / np.linalg.norm(vector)
    return unit * scale
