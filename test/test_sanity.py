# Third Party
import numpy as np
import pytest

# ll4ma
from ll4ma_opt.problems.basic_tests import F0, F1, F2, Func2DLeastSquares, Quadratic2D, Rosenbrock
from ll4ma_opt.problems.constrained_circle_problem import CircleEquality, CircleInequality
from ll4ma_opt.problems.find_sphere_center import OrthonormalProblem
from ll4ma_opt.problems.ggn_test_problems import DiehlGGNProblem
from ll4ma_opt.problems.ik_problem import PandaIK, TwoLinkIK
from ll4ma_opt.problems.linear_eq_constraints import TuckersQP, EasyQP
from ll4ma_opt.problems.resources.finite_differences import fd, approximate_hessian
from ll4ma_opt.problems import GMM2D, SteinWrapper
from ll4ma_opt.solvers.cem import CEM
from ll4ma_opt.solvers.conjugate_gradient_methods import ConjugateGradient
from ll4ma_opt.solvers.line_search import (
    GaussNewtonMethod,
    GeneralizedGaussNewton,
    GradientDescent,
    Momentum,
    NewtonMethod,
    NewtonMethodWolfeLine,
)
from ll4ma_opt.solvers.mppi import MPPI
from ll4ma_opt.solvers.opt_conv_criteria import _COST_THRESH
from ll4ma_opt.solvers.penalty_method import AugmentedLagranMethod, PenaltyMethod
from ll4ma_opt.solvers.log_barrier import LogBarrierSolver
from ll4ma_opt.solvers.quasi_newton import BFGSMethod, SR1Method
from ll4ma_opt.solvers.samplers import GaussianSampler
from ll4ma_opt.solvers.trust_region import CauchyPointSolver, DoglegMethod, IterativeTRSolver

# TODO (adam) this orthonormal problem is causing weird errors on my system
BASIC_TEST_PROBLEMS = ["f0", "f1", "f2", "quad_2D", "rosenbrock", "orthonormal"]
IK_PROBLEMS = ["two_link_ik", "panda_ik"]
INEQ_CONSTRAINT_PROBLEMS = ["circle_inequality"]
EQ_CONSTRAINT_PROBLEMS = ["circle_inequality"]
LINEAR_EQ_CONSTRAINT_PROBLEMS = ["TuckersQP", "EasyQP"]
COST_THRESH_PROBLEMS = ["diehl"]

LINE_SEARCH_SOLVERS = ["newton", "gradient_descent", "momentum", "newton_wolfe", "bfgs"]
GAUSS_NEWTON_SOLVERS = ["gauss_newton", "generalized_gauss_newton"]
# TODO add Dogleg method to trust region, raises NotImplementedError currently, see issue:
#     https://bitbucket.org/robot-learning/ll4ma-opt-sandbox/issues/26
TRUST_REGION_SOLVERS = ["cauchy_point", "iterative_tr", "sr1"]
EQ_CONSTRAINT_SOLVERS = ["penalty", "augmented_lagrangian"]
INEQ_CONSTRAINT_SOLVERS = ["penalty", "augmented_lagrangian", "log_barrier"]
LINEAR_EQ_CONSTRAINT_SOLVERS = ["newton"]
CONJUGATE_GRADIENT_SOLVERS = ["conjugate_gradient"]
SAMPLING_SOLVERS = ["cem", "mppi", "cem_uniform_start"]


MAX_ITERATIONS = 2
N_SAMPLES = 100


def validate_result(problem, result, cost_threshold=False):
    assert isinstance(result.solution, np.ndarray)
    assert isinstance(result.cost, float)
    assert isinstance(result.converged, bool)
    assert isinstance(result.iterates, np.ndarray)
    if result.sample_iterates is not None:
        assert isinstance(result.sample_iterates, np.ndarray)

    assert result.solution.shape == (problem.size(), 1)
    # TODO it's hard to check this in a unified way, e.g. penalty methods have inner
    # loop that magnifies the number of iterates returned
    # if not converged:
    #     assert result.iterates.shape[0] == MAX_ITERATIONS + 1, f"Shape: {result.iterates.shape}"
    assert result.iterates.shape[1:] == (problem.size(), 1)
    if result.sample_iterates is not None:
        if not result.converged:
            assert result.sample_iterates.shape[0] == MAX_ITERATIONS + 1
        assert result.sample_iterates.shape[1:] == (N_SAMPLES, problem.size())

    if cost_threshold:
        assert _COST_THRESH in result.convergence_criteria


def validate_fd(grad, fd_grad):
    """Validate that the fd gradient matches the analytic form."""
    assert isinstance(fd_grad, np.ndarray)
    assert np.allclose(grad.ravel(), fd_grad.ravel(), 0.1)


def validate_fd_hessian(hessian, fd_hessian):
    """Validate that the fd hessian  matches the analytic form."""
    assert isinstance(fd_hessian, np.ndarray)
    assert fd_hessian.shape == hessian.shape


class BrokenException(Exception):
    pass


# FIXTURES -------------------------------------------------------------------


@pytest.fixture
def problem(request):
    problem = request.param
    if problem == "f0":
        return F0()
    elif problem == "f1":
        return F1()
    elif problem == "f2":
        return F2()
    elif problem == "quad_2D":
        return Quadratic2D()
    elif problem == "rosenbrock":
        return Rosenbrock()
    elif problem == "two_link_ik":
        return TwoLinkIK()
    elif problem == "panda_ik":
        return PandaIK()
    elif problem == "least_squares":
        return Func2DLeastSquares()
    elif problem == "circle_equality":
        return CircleEquality()
    elif problem == "circle_inequality":
        return CircleInequality()
    elif problem == "orthonormal":
        return OrthonormalProblem()
    elif problem == "diehl":
        return DiehlGGNProblem()
    elif problem == "TuckersQP":
        return TuckersQP()
    elif problem == "EasyQP":
        return EasyQP()
    else:
        raise ValueError(f"Unknown problem: {problem}")


@pytest.fixture
def solver(request, problem):
    solver = request.param
    if solver == "newton":
        return NewtonMethod(problem)
    elif solver == "gradient_descent":
        return GradientDescent(problem)
    elif solver == "newton_wolfe":
        return NewtonMethodWolfeLine(problem)
    elif solver == "momentum":
        return Momentum(problem)
    elif solver == "gauss_newton":
        return GaussNewtonMethod(problem)
    elif solver == "generalized_gauss_newton":
        return GeneralizedGaussNewton(problem)
    elif solver == "cem":
        sampler = GaussianSampler(
            problem.size(),
            problem.min_bounds,
            problem.max_bounds,
            start_iso_var=0.1,
        )
        return CEM(problem, sampler, n_samples=N_SAMPLES, mean_solve_only=True)
    elif solver == "cem_uniform_start":
        sampler = GaussianSampler(
            problem.size(),
            problem.min_bounds,
            problem.max_bounds,
            start_iso_var=0.1,
        )
        return CEM(
            problem,
            sampler,
            n_samples=N_SAMPLES,
            mean_solve_only=True,
            uniform_initial_sampling=True,
        )
    elif solver == "mppi":
        sampler = GaussianSampler(
            problem.size(), problem.min_bounds, problem.max_bounds, start_iso_var=0.1
        )
        return MPPI(problem, sampler, n_samples=N_SAMPLES)
    elif solver == "cauchy_point":
        return CauchyPointSolver(problem)
    elif solver == "dogleg":
        raise BrokenException(
            "DoglegMethod raises NotImplementedError, see issue: "
            "https://bitbucket.org/robot-learning/ll4ma-opt-sandbox/issues/26"
        )
        return DoglegMethod(problem)
    elif solver == "iterative_tr":
        return IterativeTRSolver(problem)
    elif solver == "bfgs":
        return BFGSMethod(problem)
    elif solver == "sr1":
        return SR1Method(problem)
    elif solver == "penalty":
        return PenaltyMethod(problem)
    elif solver == "augmented_lagrangian":
        return AugmentedLagranMethod(problem)
    elif solver == "conjugate_gradient":
        return ConjugateGradient(problem)
    elif solver == "log_barrier":
        return LogBarrierSolver(problem)
    else:
        raise ValueError(f"Unknown solver: {solver}")


# TESTS ----------------------------------------------------------------------


@pytest.mark.parametrize("problem", BASIC_TEST_PROBLEMS + IK_PROBLEMS, indirect=True)
@pytest.mark.parametrize("solver", LINE_SEARCH_SOLVERS, indirect=True)
def test_line_search(problem, solver):
    result = solver.optimize(problem.initial_solution, MAX_ITERATIONS)
    validate_result(problem, result)


@pytest.mark.parametrize("problem", ["least_squares"], indirect=True)
@pytest.mark.parametrize("solver", ["gauss_newton"], indirect=True)
def test_gauss_newton(problem, solver):
    result = solver.optimize(problem.initial_solution, MAX_ITERATIONS)
    validate_result(problem, result)


@pytest.mark.parametrize("problem", BASIC_TEST_PROBLEMS, indirect=True)
@pytest.mark.parametrize("solver", TRUST_REGION_SOLVERS, indirect=True)
def test_trust_region(problem, solver):
    result = solver.optimize(problem.initial_solution, MAX_ITERATIONS)
    validate_result(problem, result)


@pytest.mark.parametrize("problem", EQ_CONSTRAINT_PROBLEMS, indirect=True)
@pytest.mark.parametrize("solver", EQ_CONSTRAINT_SOLVERS, indirect=True)
def test_eq_constraint_method(problem, solver):
    result = solver.optimize(problem.initial_solution, MAX_ITERATIONS)
    validate_result(problem, result)


@pytest.mark.parametrize("problem", INEQ_CONSTRAINT_PROBLEMS + IK_PROBLEMS, indirect=True)
@pytest.mark.parametrize("solver", INEQ_CONSTRAINT_SOLVERS, indirect=True)
def test_ineq_constraint_method(problem, solver):
    result = solver.optimize(problem.initial_solution, MAX_ITERATIONS)
    validate_result(problem, result)


@pytest.mark.parametrize("problem", LINEAR_EQ_CONSTRAINT_PROBLEMS, indirect=True)
@pytest.mark.parametrize("solver", LINEAR_EQ_CONSTRAINT_SOLVERS, indirect=True)
def test_linear_eq_constraint_method(problem, solver):
    result = solver.optimize(problem.initial_solution, MAX_ITERATIONS)
    validate_result(problem, result)


@pytest.mark.parametrize("problem", BASIC_TEST_PROBLEMS, indirect=True)
@pytest.mark.parametrize("solver", CONJUGATE_GRADIENT_SOLVERS, indirect=True)
def test_conjugate_gradient_method(problem, solver):
    result = solver.optimize(problem.initial_solution, max_iterations=MAX_ITERATIONS)
    validate_result(problem, result)


@pytest.mark.parametrize("problem", IK_PROBLEMS, indirect=True)
@pytest.mark.parametrize("solver", SAMPLING_SOLVERS, indirect=True)
def test_sampling_solvers(problem, solver):
    result = solver.optimize(problem.initial_solution, MAX_ITERATIONS)
    validate_result(problem, result)


@pytest.mark.parametrize("problem", COST_THRESH_PROBLEMS, indirect=True)
@pytest.mark.parametrize(
    "solver",
    LINE_SEARCH_SOLVERS + GAUSS_NEWTON_SOLVERS + TRUST_REGION_SOLVERS + CONJUGATE_GRADIENT_SOLVERS,
    indirect=True,
)
def test_cost_threshold_conv(problem, solver):
    solver.cost_threshold = 1
    result = solver.optimize(problem.initial_solution, MAX_ITERATIONS)
    validate_result(problem, result)


@pytest.mark.parametrize("problem", BASIC_TEST_PROBLEMS + IK_PROBLEMS, indirect=True)
def test_fd(problem):
    test_delta = 1.0e-08
    c, g = problem.cost, problem.cost_gradient
    x0 = problem.initial_solution
    g_hat_x0 = fd(c, x0, test_delta)
    g_x0 = g(x0)

    validate_fd(g_x0, g_hat_x0)


@pytest.mark.parametrize("problem", BASIC_TEST_PROBLEMS + IK_PROBLEMS, indirect=True)
def test_fd_hessian(problem):
    test_delta = 1.0e-08
    c = problem.cost
    x0 = problem.initial_solution
    h = problem.cost_hessian
    H_x0 = h(x0)
    H_hat_x0 = approximate_hessian(c, x0, test_delta)

    validate_fd_hessian(H_x0, H_hat_x0)


# Issue 138, make sure that solvers bounce off bound constraint corner and proceed to minimum
@pytest.mark.parametrize(
    "solver",
    [
        "newton",
        "gradient_descent",
    ],
)
def test_bound_constraint_convergence(solver):
    prob = Quadratic2D()
    prob.b *= 0
    prob.Q = np.eye(2)
    prob.min_bounds = -0.5 * np.ones((2, 1))
    if solver == "gradient_descent":
        optimizer = GradientDescent(prob, alpha=1.5)
    if solver == "newton":
        optimizer = NewtonMethod(prob, alpha=1.5)

    initial_solution = np.ones((2, 1))

    result = optimizer.optimize(initial_solution)
    assert result.cost < 1e-5


@pytest.mark.parametrize(
    "seed",
    list(range(5)),
)
def test_stein_result(seed):
    np.random.seed(seed)
    original_problem = GMM2D(1)

    problem = SteinWrapper(original_problem, 100, 1)
    step = 4e-1
    params = {"FP_PRECISION": 1e-10, "alpha": step, "min_alpha": step}

    optimizer = GradientDescent(problem, **params)
    result = optimizer.optimize(problem.initial_solution, 50)

    particles = result.solution.reshape(problem.num_particles, problem.original_size)

    estimated_cov = np.cov(particles.T)
    true_cov = np.linalg.inv(original_problem.covariance_inverses[0])
    error = estimated_cov - true_cov
    error_norm = np.linalg.norm(error)
    assert error_norm < 1e-1
