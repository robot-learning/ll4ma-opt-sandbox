# Third-party
import torch
import argparse

# ll4ma
from ll4ma_opt.solvers import PenaltyMethod, AugmentedLagranMethod


params = {"alpha": 1, "rho": 0.5}
FP_PRECISION = 1e-6


def pointmass(method="newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems import PointMassPositionFormulation

    problem = PointMassPositionFormulation(timesteps=15, obstacles=50, obstacle_radius=0.1)
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display(show_solution=False)
    problem.visualize_optimization(result)


def pillbot(method="newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems import PillbotPositionFormulation

    problem = PillbotPositionFormulation(timesteps=15, obstacles=10, obstacle_radius=0.1)
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display(show_solution=False)
    problem.visualize_optimization(result)


def twolinkIK(method="newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems import TwoLinkIK

    problem = TwoLinkIK(
        use_collision=obstacles,
        collision_env=(1, 0.1, (torch.tensor([1.4, 0.0, 0.45]), torch.tensor([1.4, 0.0, 0.45]))),
        soft_limits=softlimits,
    )
    problem.min_bounds *= 1000
    problem.max_bounds *= 1000
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution + 2.5, max_iterations=max_iters)
    print("result:", result.solution)
    result.display()
    problem.visualize_optimization(result)


def pandaIK(method="newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems import PandaIK

    problem = PandaIK(
        use_collision=obstacles,
        collision_env=(1, 0.1, (torch.tensor([0.25, 0.0, 0.6]), torch.tensor([0.25, 0.0, 0.6]))),
        soft_limits=softlimits,
    )
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display()
    problem.visualize_optimization(result)


def iiwaIK(method="gauss newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems import IiwaIK

    params = {"alpha": 1, "rho": 0.5}
    FP_PRECISION = 1e-5
    target = torch.tensor(
        [
            [0.0, 1.0, 0.0, 7.2559e-01],
            [0.0, 0.0, -1.0, -2.6365e-01],
            [-1.0, 0, 0, 7.3588e-01],
            [0.0, 0.0, 0.0, 1.0],
        ]
    )
    problem = IiwaIK(
        desired_pose=target,
        use_collision=obstacles,
        collision_env=(1, 0.1, (torch.tensor([0.3, 0.0, 0.99]), torch.tensor([0.3, 0.0, 0.99]))),
        soft_limits=softlimits,
    )
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display()
    problem.visualize_optimization(result)


def twolinkTraj(method="newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems import TwoLinkTraj

    problem = TwoLinkTraj(
        timesteps=40,
        use_collision=obstacles,
        collision_env=(1, 0.1, (torch.tensor([1.4, 0.0, 0.45]), torch.tensor([1.4, 0.0, 0.45]))),
        soft_limits=softlimits,
    )
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display(show_solution=True)
    problem.visualize_optimization(result)


def twolinkKDL(
    method="gauss newton", max_iters=50, auglag=False, obstacles=False, softlimits=False
):
    from ll4ma_opt.problems import TwoLinkKDLTraj

    problem = TwoLinkKDLTraj(
        timesteps=40,
        use_collision=obstacles,
        collision_env=(1, 0.1, (torch.tensor([1.4, 0.0, 0.45]), torch.tensor([1.4, 0.0, 0.45]))),
        soft_limits=softlimits,
    )
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display(show_solution=True)
    problem.visualize_optimization(result)


def pandaTraj(method="newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems import PandaTraj

    problem = PandaTraj(
        timesteps=30,
        use_collision=obstacles,
        collision_env=(1, 0.1, (torch.tensor([0.25, 0.0, 0.6]), torch.tensor([0.25, 0.0, 0.6]))),
        soft_limits=softlimits,
        pose_constraint=False,
    )
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display(show_solution=False)
    problem.visualize_optimization(result)


def pandaKDLTraj(
    method="gauss newton", max_iters=50, auglag=False, obstacles=False, softlimits=False
):
    from ll4ma_opt.problems import PandaKDLTraj

    problem = PandaKDLTraj()
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display(show_solution=False)
    problem.visualize_optimization(result)


def iiwaDHTraj(method="newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems import IiwaTraj

    problem = IiwaTraj()
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display(show_solution=True)
    problem.visualize_optimization(result)


def iiwaTraj(method="gauss newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems import IiwaKDLTraj

    params = {"alpha": 1, "rho": 0.5}
    FP_PRECISION = 1e-5
    # target = torch.tensor(
    #    [[0.,  1., 0.,  7.2559e-01],
    #     [ 0., 0., -1., -2.6365e-01],
    #     [-1., 0, 0,  7.3588e-01],
    #     [ 0.,  0.,  0.,  1.]]
    # )
    target = torch.tensor(
        [0.07605195, 1.7792633, -1.27016297, -0.65090574, -0.33412373, 1.02281634, 1.97996214]
    )
    problem = IiwaKDLTraj(
        desired_pose=target,
        timesteps=10,
        # error_type='pose',
        error_type="position",
        use_collision=obstacles,
        collision_env=(1, 0.1, (torch.tensor([0.3, 0.0, 0.99]), torch.tensor([0.3, 0.0, 0.99]))),
        soft_limits=softlimits,
    )
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display(show_solution=True)
    problem.visualize_optimization(result)


def ori(method="newton", max_iters=50, auglag=False, obstacles=False, softlimits=False):
    from ll4ma_opt.problems.orientation_problem import OrientationProblem

    desired_pose = torch.DoubleTensor(
        [
            [1, 0, 0],
            [0, -1, 0],
            [0, 0, -1],
        ]
    )
    problem = OrientationProblem(desired_pose)
    if auglag:
        solver = AugmentedLagranMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    else:
        solver = PenaltyMethod(problem, method, params, FP_PRECISION=FP_PRECISION)
    result = solver.optimize(problem.initial_solution, max_iterations=max_iters)
    result.display(show_solution=True)
    problem.visualize_optimization(result)


def main():
    import warnings

    warnings.filterwarnings("ignore", category=DeprecationWarning)

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--test", type=str, default="twolinkIK", help="which test to run")
    parser.add_argument(
        "-m", "--method", type=str, default="gauss newton", help="which method to run"
    )
    parser.add_argument("-i", "--iterations", type=int, default=50, help="max iterations")
    parser.add_argument("-a", "--auglag", action="store_true", help="if using augmented lagrangian")
    parser.add_argument(
        "-o", "--obstacles", action="store_true", help="if using obstacle environment"
    )
    parser.add_argument(
        "-l", "--softlimits", action="store_true", help="if using soft limit constraints"
    )
    opt = parser.parse_args()

    tests = {
        "twolinkIK": twolinkIK,
        "pandaIK": pandaIK,
        "iiwaIK": iiwaIK,
        "pointmass": pointmass,
        "twolinkTraj": twolinkTraj,
        "pandaTraj": pandaTraj,
        "pandaKDL": pandaKDLTraj,
        "pillbot": pillbot,
        "twolinkKDL": twolinkKDL,
        "iiwa": iiwaTraj,
        "iiwaTraj": iiwaTraj,
        "iiwaDH": iiwaDHTraj,
        "iiwaDHTraj": iiwaDHTraj,
        "ori": ori,
    }
    tests[opt.test](opt.method, opt.iterations, opt.auglag, opt.obstacles, opt.softlimits)


if __name__ == "__main__":
    main()
